<?php
class SH_Shortcodes
{
    function featured($atts, $contents = null)
    {
        global $wpsc_product, $wpsb, $post, $wpsc_variations;
        $t = $GLOBALS['_sh_base']; 
        extract( shortcode_atts( array(
            'title' => __('Featured', SH_NAME),
            'num' => 6,
            'sort' => 'date',
            'view' => 'span4'
        ), $atts ) );
        
        wp_enqueue_script( array( 'jquery-tooltipster','jquery-prettyPhoto', ) );
        
        wp_reset_query() ;
        $include = get_option('sticky_products');
        query_posts(array('post_type'=>'wpsc-product', 'post__in'=>$include, 'showposts'=>$num, 'orderby'=>$sort));
        $output = '';
        ob_start(); ?>
        
        <?php $compare_template = sh_page_template('tpl-compare.php');
                
                
            if($compare_template) $compare_page_id = $compare_template->ID; 
            else $compare_page_id = '';
            
            $permalink = strpos(get_permalink($compare_page_id), '?');
            
            if($permalink == true){
                $seperator = "&";
            }else{
                $seperator = "?";
            }
        ?>
        <div class="product_wrap">
            <div class="container">
                
                <div class="row heading-wrap">
                    <div class="span12 heading">
                        <h2><?php echo $title; ?><span></span></h2>
                    </div>
                </div>
                
                <div class="row">
                    <?php $count = 0;
                    while(have_posts()): the_post();
                     
                        global $wpsc_variations;
                        $classes = '';
                        $count++;
                        if ( 0 == ( $count - 1 ) % 4 )
                            $classes = ' first';
                        if ( 0 == $count % 4 )
                            $classes = ' last';
                        
                        $wpsc_variations = new wpsc_variations( get_the_id() );?>
                        <div class="span3 product<?php echo $classes; ?>">
        
                            <div>
                                <figure>
                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('pro-single-slide'); ?></a>
                                    <div class="overlay">
                                        <a href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="zoom prettyPhoto"></a>
                                        <a href="<?php the_permalink(); ?>" class="link"></a>
                                    </div>
                                </figure>
                                <div class="detail">
                                    <span><?php wpsc_the_product_price_display( array( 'price_text'=>'%s', 'output_old_price' => false, 'output_you_save'  => false, ) )?></span>
                                    <h4><?php the_title(); ?></h4>
                                    <div class="icon">
            
                                      <?php 
                                        if(wpsc_product_external_link(wpsc_the_product_id()) != '') $action =  wpsc_product_external_link(wpsc_the_product_id()); 
                                        else $action = wpsc_this_page_url(); ?>
                                         <form class="product_form"  enctype="multipart/form-data" action="<?php echo $action; ?>" method="post" name="product_<?php echo wpsc_the_product_id(); ?>" id="product_<?php echo wpsc_the_product_id(); ?>" >
                                          <?php if(wpsc_product_has_stock() && $t->alloption('hide_addtocart_button') == 0 &&  get_option('addtocart_or_buynow') !='1' && !wpsc_have_variation_groups() ) : ?>
                                           
                                        <input type="hidden" value="add_to_cart" name="wpsc_ajax_action"/>
                                        <input type="hidden" value="<?php echo wpsc_the_product_id(); ?>" name="product_id"/>
                                        <button type="submit" name="Buy" class="wpsc_buy_button" id="product_<?php echo wpsc_the_product_id(); ?>_submit_button">
                                            <a href="<?php the_permalink(); ?>" class="one tooltip" title="<?php _e('Add to Cart', SH_NAME)?>"></a></button>
                                       
                                          <?php elseif( wpsc_have_variation_groups() ): ?>
                                            <a href="<?php the_permalink(); ?>" class="one tooltip" title="<?php _e('Add to Cart', SH_NAME)?>"></a> 
                                        <?php else: ?>
                                            <a href="<?php the_permalink(); ?>" class="one tooltip" title="<?php _e('Buy Now', SH_NAME); ?>"><?php _e('Buy Now', SH_NAME);?></a>
                                        <?php endif; ?>
                                        <div class="wpsc_loading_animation">
                                            <img title="" alt="<?php esc_attr_e( 'Loading', SH_NAME ); ?>" src="<?php echo wpsc_loading_animation_url(); ?>" />
                                        </div><!--close wpsc_loading_animation-->
                                    </form>
                                       
                                    </div>
                                </div>
                            </div>
        
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
        
            
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        
        wp_reset_query() ;
        return $output;
    }
	?>
        
        
        <!-- SLIDER this code should be replace into the parent theme because it is not do automaticaly -->
        <div class="slider clearfix">
            <div class="slider-slides">
            	<?php while(have_posts()): the_post();?>
                	<?php $meta_settings = get_post_meta($post->ID, 'sh_slide_settings', true);?>
                    <div class="slides">
                        <a href="<?php echo sh_set($meta_settings, 'link');?>"><?php the_post_thumbnail('large-slide'); ?></a>
                        <div class="overlay">
                            <h1><?php the_title();?></h1>
                            <p><?php echo (sh_set($meta_settings, 'discount')) ? '<span>'.sh_set($meta_settings, 'discount').'</span> OFF<br/>' : ''; ?><?php echo sh_set($meta_settings, 'subtitle'); ?></p>
                        </div>
                    </div>
                <?php endwhile;?>
                
            </div>
            <a href="#" class="next"></a>
            <a href="#" class="prev"></a>
            <div class="slider-btn"></div>
        </div>
        <!-- SLIDER -->
    
    	<!-- featured -->
        <div class="product_wrap">
        	<div class="container">
                
                <div class="row heading-wrap">
                    <div class="span12 heading">
                        <h2><?php echo $title; ?><span></span></h2>
                    </div>
                </div>
                
                <div class="row">
                    <?php $count = 0;
					while(have_posts()): the_post();
					 
						global $wpsc_variations;
						$classes = '';
						$count++;
						if ( 0 == ( $count - 1 ) % 4 )
							$classes = ' first';
						if ( 0 == $count % 4 )
							$classes = ' last';
                    	
						$wpsc_variations = new wpsc_variations( get_the_id() );?>
                        <div class="span3 product<?php echo $classes; ?>">
        
                            <div>
                                <figure>
                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('pro-single-slide'); ?></a>
                                    <div class="overlay">
                                        <a href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="zoom prettyPhoto"></a>
                                        <a href="<?php the_permalink(); ?>" class="link"></a>
                                    </div>
                                </figure>
                                <div class="detail">
                                    <span><?php wpsc_the_product_price_display( array( 'price_text'=>'%s', 'output_old_price' => false, 'output_you_save'  => false, ) )?></span>
                                    <h4><?php the_title(); ?></h4>
                                    <div class="icon">
            
									  <?php 
                                        if(wpsc_product_external_link(wpsc_the_product_id()) != '') $action =  wpsc_product_external_link(wpsc_the_product_id()); 
                                        else $action = wpsc_this_page_url(); ?>
                                         <form class="product_form"  enctype="multipart/form-data" action="<?php echo $action; ?>" method="post" name="product_<?php echo wpsc_the_product_id(); ?>" id="product_<?php echo wpsc_the_product_id(); ?>" >
                                          <?php if(wpsc_product_has_stock() && $t->alloption('hide_addtocart_button') == 0 &&  get_option('addtocart_or_buynow') !='1' && !wpsc_have_variation_groups() ) : ?>
                                           
                                        <input type="hidden" value="add_to_cart" name="wpsc_ajax_action"/>
                                        <input type="hidden" value="<?php echo wpsc_the_product_id(); ?>" name="product_id"/>
                                        <button type="submit" name="Buy" class="wpsc_buy_button" id="product_<?php echo wpsc_the_product_id(); ?>_submit_button"><a class="one tooltip"></a></button>
                                       
                                          <?php elseif( wpsc_have_variation_groups() ): ?>
                                            <a href="<?php the_permalink(); ?>" class="one tooltip" title="<?php _e('Add to Cart', SH_NAME)?>"></a> 
                                        <?php else: ?>
                                            <a href="<?php the_permalink(); ?>" class="one tooltip" title="<?php _e('Buy Now', SH_NAME); ?>"><?php _e('Buy Now', SH_NAME);?></a>
                                        <?php endif; ?>
                                         <a href="<?php echo get_permalink($compare_page_id).$seperator;?>product_id=<?php echo get_the_id();?>" class="three tooltip" title="Add to compare"></a>
                                          <div class="wpsc_loading_animation">
                                            <img title="" alt="<?php esc_attr_e( 'Loading', SH_NAME ); ?>" src="<?php echo wpsc_loading_animation_url(); ?>" />
                                        </div><!--close wpsc_loading_animation-->
                                    </form>
                                       
                                    </div>
                                </div>
                            </div>
        
                        </div>
                    <?php endwhile; ?>
                </div>
			</div>
		</div>
	
		<?php
		
}
?>