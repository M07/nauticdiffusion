<?php
global $wp_query;
$image_width = get_option('product_image_width');
/*
 * Most functions called in this page can be found in the wpsc_query.php file
 */
$_SESSION['bistro_pro_view'] = isset($_SESSION['bistro_pro_view']) ? $_SESSION['bistro_pro_view'] : 'list';
 if( sh_set( $_GET, 'pro_view' ) ){
	 $_SESSION['bistro_pro_view'] = (sh_set( $_GET, 'pro_view' )) == 'grid' ? 'grid' : 'list';
 }
 $default_view = $_SESSION['bistro_pro_view'];
?>
<div>

<?php wpsc_output_breadcrumbs(); ?>

	<?php do_action('wpsc_top_of_products_page'); // Plugin hook for adding things to the top of the products page, like the live search ?>
	<?php /*if(wpsc_display_categories()): ?>
	  <?php if(wpsc_category_grid_view()) :?>
			<div class="wpsc_categories wpsc_category_grid group">
				<?php wpsc_start_category_query(array('category_group'=> get_option('wpsc_default_category'), 'show_thumbnails'=> 1)); ?>
					<a href="<?php wpsc_print_category_url();?>" class="wpsc_category_grid_item  <?php wpsc_print_category_classes_section(); ?>" title="<?php wpsc_print_category_name(); ?>">
						<?php wpsc_print_category_image(); ?>
					</a>
					<?php wpsc_print_subcategory("", ""); ?>
				<?php wpsc_end_category_query(); ?>

			</div><!--close wpsc_categories-->
	  <?php else:?>
			<ul class="wpsc_categories">

				<?php wpsc_start_category_query(array('category_group'=>get_option('wpsc_default_category'), 'show_thumbnails'=> get_option('show_category_thumbnails'))); ?>
						<li>
							<?php wpsc_print_category_image(); ?>

							<a href="<?php wpsc_print_category_url();?>" class="wpsc_category_link <?php wpsc_print_category_classes_section(); ?>" title="<?php wpsc_print_category_name(); ?>"><?php wpsc_print_category_name(); ?></a>
							<?php if(wpsc_show_category_description()) :?>
								<?php wpsc_print_category_description("<div class='wpsc_subcategory'>", "</div>"); ?>
							<?php endif;?>

							<?php wpsc_print_subcategory("<ul>", "</ul>"); ?>
						</li>
				<?php wpsc_end_category_query(); ?>
			</ul>
		<?php endif; ?>
	<?php endif; ?>
<?php // */ ?>

	<?php if(wpsc_display_products()): ?>

		
		<?php if(wpsc_is_in_category()) : ?>
			<div class="wpsc_category_details">
				<?php if(wpsc_show_category_thumbnails()) : ?>
					<img src="<?php echo wpsc_category_image(); ?>" alt="<?php echo wpsc_category_name(); ?>" />
				<?php endif; ?>

				<?php if(wpsc_show_category_description() &&  wpsc_category_description()) : ?>
					<?php echo wpsc_category_description(); ?>
				<?php endif; ?>
			</div><!--close wpsc_category_details-->
		<?php endif; ?>
		
		
		
		<?php if(wpsc_has_pages_top()) : ?>
				<?php _the_pagination(); ?>
		<?php endif; ?>

		<?php if( $default_view == 'grid' ):?>
        	<?php /** start the product loop here */ ?>
           
                <div class="clearfix row">
                    <?php while(wpsc_have_products()) :  wpsc_the_product(); ?>
                        
                            <?php sh_pro_grid_view(); ?>
                        <!--close default_product_display-->
                    <?php endwhile; ?>
                  
                </div>
            
		<?php /** end the product loop here */?>
        <?php else:?>
        	<?php /** start the product loop here */ ?>
            <div class=" product-list clearfix">
				<?php while(wpsc_have_products()) :  wpsc_the_product(); ?>
                    
                        <?php sh_pro_list_view(); ?>
                        <!--close default_product_display-->
                    
                <?php endwhile; ?>
            </div>
            <?php /** end the product loop here */?>
        <?php endif;?>
		
		
        
		<?php if(wpsc_product_count() == 0):?>
			<h3><?php  _e('There are no products in this group.', SH_NAME); ?></h3>
		<?php endif ; ?>
        
		<?php do_action( 'wpsc_theme_footer' ); ?>

		<?php //if(wpsc_has_pages_bottom()) : ?>
				<?php _the_pagination(); ?>
		<?php //endif; ?>
	<?php endif; ?>
</div><!--close default_products_page_container-->
