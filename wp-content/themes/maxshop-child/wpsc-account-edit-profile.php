<?php
/**
 * The Account > Edit Profile template.
 *
 * Displays the user account page.
 *
 * @package WPSC
 * @since WPSC 3.8.10
 */

require_once( 'functions/wpsc-user_log_functions.php' );
?>

<?php echo validate_form_data(); ?>

<div class="checkout_forms_container">
    <div class="checkout_forms_container_head">
      <h3><?php _e('Billing and Shipping', SH_NAME);?><h3>
    </div>
    <div class="checkout_forms_container_body">
		<form method="post">
			<table>
				<?php wpsc_display_form_fields_child(); ?>
			</table>
			<tr>
				<td>
					<input type="hidden" value="true" name="submitwpcheckout_profile" />
					<input type="submit" value="<?php _e( 'Save Profile', SH_NAME ); ?>" name="submit" />
				</td>
			</tr>
		</form>
	</div>
</div>