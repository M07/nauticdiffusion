<?php
/**
 * The User Account template wrapper.
 *
 * Displays the user account page.
 *
 * @package WPSC
 * @since WPSC 3.8
 */

require_once( 'functions/wpsc-user_log_functions.php' );
global $current_tab; ?>
<div class="wrap">
	<?php if ( is_user_logged_in() ) : ?>
		<div class="user-profile-links">

			<?php $default_profile_tab = apply_filters( 'wpsc_default_user_profile_tab', 'purchase_history' ); ?>
			<?php $current_tab = isset( $_REQUEST['tab'] ) ? $_REQUEST['tab'] : $default_profile_tab; ?>

			<?php wpsc_user_profile_links_child(); ?>

			<?php do_action( 'wpsc_additional_user_profile_links', '|' ); ?>

		</div>

		<?php do_action( 'wpsc_user_profile_section_' . $current_tab ); ?>

	<?php else : ?>
		<div class="loginform-container">
			<p class="information"><?php _e( 'You must be logged in to use this page. Please use the form below to login to your account.', SH_NAME ); ?></p>

			<form name="loginform" id="loginform" action="<?php echo esc_url( wp_login_url() ); ?>" method="post">
				<p>
					<label><?php _e( 'Username:', SH_NAME ); ?><br /><input type="text" name="log" id="log" value="" size="20" tabindex="1" /></label>
				</p>

				<p>
					<label><?php _e( 'Password:', SH_NAME ); ?><br /><input type="password" name="pwd" id="pwd" value="" size="20" tabindex="2" /></label>
				</p>

				<p>
					<label class="rememberme">
						<input name="rememberme" type="checkbox" id="rememberme" value="forever" tabindex="3" />
						<?php _e( 'Remember me', SH_NAME ); ?>
					</label>
				</p>
				<p class="registerArea">
					<span><?php _e( 'New customer?', SH_NAME ); ?></span> <a href="<?php echo site_url() . "/wp-login.php?action=register" ?>"><?php _e( 'subcribe here', SH_NAME ); ?></a>		
				</p>

				<p class="submit">
					<input type="submit" name="submit" id="submit" class="btn btn-primary" value="<?php _e( 'Login &raquo;', SH_NAME ); ?>" tabindex="4" />
					<input type="hidden" name="redirect_to" value="<?php echo esc_url( get_option( 'user_account_url' ) ); ?>" />
				</p>
			</form>
		</div>
	<?php endif; ?>

</div>
