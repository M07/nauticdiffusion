<?php $settings = _WSH()->option(); //printr($settings);?>	
	<?php if(kvalue($settings, 'footer_banner_strip')):?>	
    	<!-- FOOTER -->
        <div class="shipping-wrap">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="shipping">
                            <p <?php echo (kvalue($settings, 'footer_banner_image')) ? 'style="background-image:url('.kvalue($settings, 'footer_banner_image').');"' : '';?>><?php echo kvalue($settings, 'bannerstrip_text');?></p>
                            <a href="<?php echo kvalue($settings, 'banner_url');?>" class="button"><?php echo kvalue($settings, 'banner_label');?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <?php endif;?> 
     	<div class="footer-wrap" <?php echo (kvalue($settings, 'footer_background_color')) ? 'style="background:'.kvalue($settings, 'footer_background_color').';"' : '';?>>
            <div class="container">
                <div class="row">

                    <div class="footer clearfix">
                    
                    	<?php dynamic_sidebar('footer-sidebar');?>

                    </div>
                </div>

                <div class="row">
                    <footer class="clearfix">
                    	<?php if(kvalue($settings, 'copyright_text')):?>
                            <div class="span5">
                                <p><?php echo kvalue($settings, 'copyright_text');?></p>
                            </div>
                        <?php endif;?>
                        <div class="span2 back-top">
                            <a href="#"> <img src="<?php echo get_template_directory_uri(); ?>/images/back.png" alt=""></a>
                        </div>
                        <?php if(kvalue($settings, 'use_pb') == true):?>
                       		<div class="span5">
                                 <?php get_social_icons();?>
                            </div>
                        <?php endif;?>
                    </footer>
                </div>
            </div>
        </div>  

    </div>
    <?php wp_enqueue_script( array( 'jquery-tooltipster','jquery-prettyPhoto' ) );?>
    <?php wp_footer();?>
</body>

</html>