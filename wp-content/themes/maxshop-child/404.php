<?php get_header();?>

<!-- BAR -->
<div class="bar-wrap">
    <div class="container">
        <div class="row">
            <div class="span12">
                <img src='<?php echo get_template_directory_uri(); ?>/../maxshop-child/image/404.jpg'/>
                    
            </div>
        </div>
    </div>
</div>
<!-- PRODUCT-OFFER -->
<div class="blog_wrap">
    
    <div class="container">
        <div class="row">
            <div class="span9">
                <h1>
                    <?php wp_title(''); ?>
                </h1>
            </div>
        	<div class="span9 blog">
				<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', SH_NAME ); ?></p>
                <div style="margin:10px 0px">
                    <?php get_search_form(); ?>
                </div>
        	</div>    
		</div>
    </div>    
</div>

<?php get_footer();?>