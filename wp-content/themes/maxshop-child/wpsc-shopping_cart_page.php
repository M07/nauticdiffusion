<?php
global $wpsc_cart, $wpdb, $wpsc_checkout, $wpsc_gateway, $wpsc_coupons, $wpsc_registration_error_messages;
$wpsc_checkout = new wpsc_checkout();
$alt = 0;
$coupon_num = wpsc_get_customer_meta( 'coupon' );
if( $coupon_num )
   $wpsc_coupons = new wpsc_coupons( $coupon_num );

if(wpsc_cart_item_count() < 1) :?>
   <h3><?php _e('Oops, there is nothing in your cart.', SH_NAME)?></h3>
   <p><?php echo __('Help empty cart item', SH_NAME)?></p>
   <p><a href="<?php echo site_url()?>"><?php echo __('Please visit our shop', SH_NAME)?></a></p>
   <?php
   return;
endif;
?>
<div id="checkout_page_container">
  <h3><?php _e('Please review your order', SH_NAME);?></h3>
  <div class="shopping-cart">
    <ul class="title clearfix">
        <li class="hidden-xs col-sm-2"><?php _e('Image', SH_NAME); ?></li>
        <li class="col-xs-4 col-sm-3"><?php _e('Product Name', SH_NAME); ?></li>
        <li class="col-xs-3 col-sm-2"><?php _e('Quantity', SH_NAME); ?></li>
        <li class="hidden-xs col-sm-1"><?php _e('Price', SH_NAME); ?></li>
        <li class="col-xs-2 col-sm-1"><?php _e('Total', SH_NAME); ?></li>
        <li class="col-xs-3"><?php _e('Action', SH_NAME); ?></li>
    </ul>
    <?php while (wpsc_have_cart_items()) : wpsc_the_cart_item(); ?>
        <ul class=" clearfix product_row_<?php echo wpsc_the_cart_item_key(); ?>">
          <li class="hidden-xs col-sm-2 wpsc_product_image_<?php echo wpsc_the_cart_item_key(); ?>">
            <?php if('' != wpsc_cart_item_image()): ?>
              <?php do_action ( "wpsc_before_checkout_cart_item_image" ); ?>
                <img src="<?php echo wpsc_cart_item_image(); ?>" alt="<?php echo wpsc_cart_item_name(); ?>" title="<?php echo wpsc_cart_item_name(); ?>" class="product_image" />
              <?php do_action ( "wpsc_after_checkout_cart_item_image" ); ?>
            <?php else:?>
              <div class="item_no_image">
                <?php do_action ( "wpsc_before_checkout_cart_item_image" ); ?>
                <a href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>">
                  <span><?php _e('No Image',SH_NAME); ?></span>
                </a>
                <?php do_action ( "wpsc_after_checkout_cart_item_image" ); ?>
              </div>
            <?php endif; ?>
          </li>
          <li class="col-xs-4 col-sm-3 item-name">
            <div class="title-item">
              <a href="<?php echo esc_url( wpsc_cart_item_url() ); ?>">
                <span><?php echo wpsc_cart_item_name(); ?></span>
              </a>
            </div>
          </li>
          <li class="col-xs-3 col-sm-2">
            <input type="number" class="product-quantity" name="quantity" size="2" value="<?php echo wpsc_cart_item_quantity(); ?>" class="input-text" />
          </li>
          <li class="hidden-xs col-sm-1"><?php echo wpsc_cart_single_item_price(); ?></li>
          <li class="col-xs-2 col-sm-1"><?php echo wpsc_cart_item_price(); ?></li>
          <li class="col-xs-3">
            <button class="hidden-xs btn btn-primary update-product" data-key="<?php echo wpsc_the_cart_item_key(); ?>" type="button"><?php _e('Update', SH_NAME); ?></button>
            <button class="hidden-xs btn btn-danger delete-product" data-key="<?php echo wpsc_the_cart_item_key(); ?>" type="button"><?php _e('Delete', SH_NAME);?></button>
            
            <button class="visible-xs btn btn-primary update-product" data-key="<?php echo wpsc_the_cart_item_key(); ?>" type="button" aria-label="Left Align">
              <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
            </button>
            <button class="visible-xs btn btn-danger delete-product" data-key="<?php echo wpsc_the_cart_item_key(); ?>" type="button" aria-label="Left Align">
              <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
            </button>
          </li>
        </ul>
        <?php endwhile;?>
        <a href="<?php echo site_url() ?>" class="btn btn-primary continue-shopping"><?php _e('Continue Shopping', SH_NAME);?> </a>
    </div>
  
   <!-- cart contents table close -->
  <?php if(wpsc_uses_shipping()): ?>
     <p class="wpsc_cost_before"></p>
   <?php endif; ?>
   <?php  //this HTML dispalys the calculate your order HTML   ?>

   <?php if(wpsc_has_category_and_country_conflict()): ?>
      <p class='validation-error'><?php echo esc_html( wpsc_get_customer_meta( 'category_shipping_conflict' ) ); ?></p>
   <?php endif; ?>

   <?php if(isset($_SESSION['WpscGatewayErrorMessage']) && $_SESSION['WpscGatewayErrorMessage'] != '') :?>
      <p class="validation-error"><?php echo $_SESSION['WpscGatewayErrorMessage']; ?></p>
   <?php
   endif;
   ?>
  <?php 
  $disableSellingForm = false;
  if($disableSellingForm):
    ?>
    <div class="information alert alert-info" role="alert">
      <p>Le système de commande en ligne n'est pas encore complètement opérationnel, veuillez contacter <strong>Nautic Diffusion</strong> pour confirmer votre commande :</p>
      <p><a href="tel:0660535921">06 60 53 59 21</a> - <a href="tel:0493739524">04 93 73 95 24</a> - <a href="mailto:nauticdiffusions@gmail.com">nauticdiffusions@gmail.com</a></p>
    </div>
  <?php
    else:
  ?>
    <div class="cart-calculator clearfix">
      <?php if(wpsc_uses_shipping()) : ?>
        <div class="span4">
          <h6><?php _e('Estimate Shipping & Taxes', SH_NAME); ?></h6>
          <div class="estimate clearfix">
            <?php if ( ! wpsc_have_shipping_quote() ) : // No valid shipping quotes ?>
              <?php if ( ! wpsc_have_valid_shipping_zipcode() ) : ?>
                <p class="wpsc_update_location"><span class="shipping_error"><?php _e('Please provide a Zipcode and click Calculate in order to continue.', SH_NAME); ?></span></p>
              <?php else: ?>
                <p class="wpsc_update_location_error"><span class="shipping_error"><?php _e('Sorry, online ordering is unavailable to this destination and/or weight. Please double check your destination details.', SH_NAME); ?></span></p>
              <?php endif; ?>
            <?php endif; ?>
            <form name='change_country' id='change_country' action='' method='post'>
              <?php echo wpsc_shipping_country_list();?>
              <input type='hidden' name='wpsc_update_location' value='true' />
              <input class="red-button" type='submit' value="<?php esc_attr_e( 'Calculate', SH_NAME ); ?>" name='wpsc_submit_zipcode' />
            </form>
          </div>
        </div>
      <?php endif;?>
      <?php if(wpsc_uses_coupons()): ?>
        <div class="span4">
          <h6><?php _e('Enter coupon code :', SHNAME); ?></h6>
          <div class="estimate clearfix">
            <?php if(wpsc_coupons_error()): ?>
              <p><?php _e('Coupon is not valid.', SH_NAME); ?></p>
            <?php endif;?>
            <form  method="post" action="<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>">
              <input type="text" name="coupon_num" placeholder="<?php _e('Your Coupon Number', SH_NAME);?>" id="coupon_num" value="<?php echo $wpsc_cart->coupons_name; ?>" />
              <input type="submit" class="red-button" value="<?php _e('Update', SH_NAME) ?>" />
            </form>
          </div>
        </div>
      <?php endif; ?>
      
      <div class="price-container clearfix">
        <?php 
        $wpec_taxes_controller = new wpec_taxes_controller();
        if($wpec_taxes_controller->wpec_taxes_isenabled()): ?>
          <div class="total clearfix">
            <span class="price-label minimize"><?php echo wpsc_display_tax_label(true); ?> : </span><span id="checkout_tax" class="price minimize"><?php echo wpsc_cart_tax(); ?></span>
          </div>
        <?php endif; ?> 
      </div>
      <div class="price-container clearfix">        
        <div class="total clearfix">
          <span class="price-label"><?php _e('Total :', SH_NAME)?></span><span class="price"><?php echo wpsc_cart_total(); ?></span>
        </div>
      </div>

    <?php do_action('wpsc_before_shipping_of_shopping_cart'); ?>
    <div id="wpsc_shopping_cart_container">
    <?php do_action('wpsc_before_form_of_shopping_cart'); ?>
    <?php if( ! empty( $wpsc_registration_error_messages ) ): ?>
      <p class="validation-error">
        <?php
        foreach( $wpsc_registration_error_messages as $user_error )
         echo $user_error."<br />\n";
        ?>
      </p>
    <?php endif; ?>
    <?php if ( wpsc_show_user_login_form() && !is_user_logged_in() ): ?>
      <p><?php _e('You must sign in or register with us to continue with your purchase', SH_NAME);?></p>
      <div class="wpsc_registration_form">
        <fieldset class='wpsc_registration_form'>
          <h2><?php _e( 'Sign in', SH_NAME ); ?></h2>
          <?php 
            $args = array( 'remember' => false, 'redirect' => home_url( $_SERVER['REQUEST_URI'] ) );
            wp_login_form( $args );
          ?>
          <div class="wpsc_signup_text"><?php _e('If you have bought from us before please sign in here to purchase', SH_NAME);?></div>
        </fieldset>
      </div>
    <?php endif; ?>
    <div class="checkout_forms_container">
      <div class="checkout_forms_container_head">
        <h3><?php _e('Billing and Shipping', SH_NAME);?><h3>
      </div>
      <div class="checkout_forms_container_body">
        <form class='wpsc_checkout_forms' action='<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>' method='post' enctype="multipart/form-data">
          <?php
          /**
           * Both the registration forms and the checkout details forms must be in the same form element as they are submitted together, you cannot have two form elements submit together without the use of JavaScript.
          */ 
          if(wpsc_show_user_login_form()):
            global $current_user;
            get_currentuserinfo();?>
            <div class="wpsc_registration_form">
              <fieldset class='wpsc_registration_form wpsc_right_registration'>
                <h2><?php _e('Join up now', SH_NAME);?></h2>
                <label><?php _e('Username:', SH_NAME); ?></label>
                <input type="text" name="log" id="log" value="" size="20"/><br/>

                <label><?php _e('Password:', SH_NAME); ?></label>
                <input type="password" name="pwd" id="pwd" value="" size="20" /><br />

                <label><?php _e('E-mail', SH_NAME); ?>:</label>
                <input type="text" name="user_email" id="user_email" value="" size="20" /><br />
                <div class="wpsc_signup_text"><?php _e('Signing up is free and easy! please fill out your details your registration will happen automatically as you checkout. Don\'t forget to use your details to login with next time!', SH_NAME);?></div>
              </fieldset>
            </div>
            <div class="clear"></div>
          <?php endif; // closes user login form
          $misc_error_messages = wpsc_get_customer_meta( 'checkout_misc_error_messages' );
          if( ! empty( $misc_error_messages ) ): ?>
             <div class='login_error'>
                <?php foreach( $misc_error_messages as $user_error ) { ?>
                  <p class='validation-error'><?php echo $user_error; ?></p>
                <?php } ?>
             </div>
          <?php endif; ?>
          <?php ob_start(); ?>
          <table class='wpsc_checkout_table table-1'>
          <?php 
          $i = 0;
          while (wpsc_have_checkout_items()) : wpsc_the_checkout_item();
            if(wpsc_checkout_form_is_header() == true) {
              $i++;
              //display headers for form fields
              if($i > 1): ?>
                </table>
                <table class='wpsc_checkout_table table-<?php echo $i; ?>'>
              <?php endif; ?>
              <tr <?php echo wpsc_the_checkout_item_error_class();?>>
                <td <?php wpsc_the_checkout_details_class(); ?> colspan='2'>
                  <h4><?php echo wpsc_checkout_form_name();?></h4>
                </td>
              </tr>
              <?php if(wpsc_is_shipping_details()):?>
                <tr class='same_as_shipping_row'>
                  <td colspan ='2'>
                    <?php $checked = '';
                    $shipping_same_as_billing = wpsc_get_customer_meta( 'shipping_same_as_billing' );
                    if(isset($_POST['shippingSameBilling']) && $_POST['shippingSameBilling'])
                       $shipping_same_as_billing = true;
                    elseif(isset($_POST['submit']) && !isset($_POST['shippingSameBilling']))
                      $shipping_same_as_billing = false;
                      wpsc_update_customer_meta( 'shipping_same_as_billing', $shipping_same_as_billing );
                    if( $shipping_same_as_billing )
                      $checked = 'checked="checked"'; ?>
                    <label for='shippingSameBilling'><?php _e('Same as billing address:',SH_NAME); ?></label>
                    <input type='checkbox'  data-wpsc-meta-key="shippingSameBilling" value='true' class= "wpsc-visitor-meta" name='shippingSameBilling' id='shippingSameBilling' <?php echo $checked; ?> />
                    <br/>
                    <span id="shippingsameasbillingmessage"><?php _e('Your order will be shipped to the billing address', SH_NAME); ?></span>
                  </td>
                </tr>
              <?php endif;
              // Not a header so start display form fields
              } elseif( $wpsc_checkout->checkout_item->unique_name == 'billingemail') { ?>
                <?php $email_markup =
                "<div class='wpsc_email_address'>
                  <p class='" . wpsc_checkout_form_element_id() . "'>
                    <label class='wpsc_email_address' for='" . wpsc_checkout_form_element_id() . "'>
                      " . __('Enter your email address', SH_NAME) . "
                      </label>
                  <p class='wpsc_email_address_p'>
                    <img src='https://secure.gravatar.com/avatar/empty?s=60&amp;d=mm' id='wpsc_checkout_gravatar' />
                    " . wpsc_checkout_form_field();
                    if(wpsc_the_checkout_item_error() != '')
                      $email_markup .= "<p class='validation-error'>" . wpsc_the_checkout_item_error() . "</p>";
                 $email_markup .= "</div>";
              } else { ?>
                <tr>
                  <td class='<?php echo wpsc_checkout_form_element_id(); ?>'>
                    <label for='<?php echo wpsc_checkout_form_element_id(); ?>'>
                      <?php echo wpsc_checkout_form_name();?>
                    </label>
                  </td>
                  <td>
                    <?php echo wpsc_checkout_form_field();
                    if(wpsc_the_checkout_item_error() != ''): ?>
                      <p class='validation-error'><?php echo wpsc_the_checkout_item_error(); ?></p>
                    <?php endif; ?>
                  </td>
                </tr>
              <?php }//endif; ?>
            <?php endwhile; ?>
            <?php
              $buffer_contents = ob_get_contents();
              ob_end_clean();
              if(isset($email_markup))
                echo $email_markup;
              echo $buffer_contents;
            
            do_action('wpsc_inside_shopping_cart');
            //this HTML displays activated payment gateways
            if(wpsc_gateway_count() > 1): // if we have more than one gateway enabled, offer the user a choice ?>
              <tr>
                <td colspan='2' class='wpsc_gateway_container'>
                  <h3><?php _e('Payment Type', SH_NAME);?></h3>
                  <?php wpsc_gateway_list(); ?>
                </td>
              </tr>
            <?php else: // otherwise, there is no choice, stick in a hidden form ?>
              <tr>
                <td colspan="2">
                  <?php wpsc_gateway_hidden_field(); ?>
                </td>
              </tr>
            <?php endif; ?>
            <?php if(wpsc_has_tnc()) : ?>
              <tr>
                <td colspan='2'>
                  <label for="agree"><input id="agree" type='checkbox' value='yes' name='agree' /> <?php printf(__("I agree to the <a class='thickbox' target='_blank' href='%s' class='termsandconds'>Terms and Conditions</a>", "wpsc"), esc_url( home_url( "?termsandconds=true&amp;width=360&amp;height=400" ) ) ); ?> <span class="asterix">*</span></label>
                </td>
              </tr>
            <?php endif; ?>
          </table>
          <!-- div for make purchase button -->
          <div class='wpsc_make_purchase'>
            <span>
              <?php if(!wpsc_has_tnc()) : ?>
                <input type='hidden' value='yes' name='agree' />
              <?php endif; ?>
              <input type='hidden' value='submit_checkout' name='wpsc_action' />
              <input type='submit' value='<?php _e('Purchase', SH_NAME);?>' class='btn btn-primary' />
             </span>
          </div>
        <div class='clear'></div>
      </form>
    </div>
  <?php
    endif;
  ?>
  </div>
</div>
</div><!--close checkout_page_container-->
<?php
do_action('wpsc_bottom_of_shopping_cart');