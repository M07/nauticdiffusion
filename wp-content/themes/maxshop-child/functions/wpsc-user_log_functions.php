<?php

/**
 * WP eCommerce User Account class
 *
 * This class is responsible for theming the User Account page.
 *
 * @package wp-e-commerce
 * @since 3.8
 */
global $wpdb, $user_ID, $wpsc_purchlog_statuses, $separator;

if ( get_option( 'permalink_structure' ) != '' )
	$separator = "?";
else
	$separator = "&amp;";

function validate_form_data() {

	global $wpdb, $user_ID, $wpsc_purchlog_statuses;

	$any_bad_inputs = false;
	$changes_saved = false;
	$bad_input_message = '';
	$_SESSION['collected_data'] = null;
	$message = '';
			
	if ( ! empty($_POST['collected_data']) ) {
			
		if( ! wp_verify_nonce( $_POST['_wpsc_user_profile'], 'wpsc_user_profile') ) {
			$bad_input_message = __( 'It would appear either you are trying to hack into this account, or your session has expired.  Hoping for the latter.', SH_NAME );
		} else {
			
			foreach ( (array)$_POST['collected_data'] as $value_id => $value ) {
				$form_sql = $wpdb->prepare( "SELECT * FROM `" . WPSC_TABLE_CHECKOUT_FORMS . "` WHERE `id` = %d LIMIT 1", $value_id );
				$form_data = $wpdb->get_row( $form_sql, ARRAY_A );
				$bad_input = false;
				if ( $form_data['mandatory'] == 1 ) {
					switch ( $form_data['type'] ) {
						case "email":
							if ( !preg_match( "/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-.]+\.[a-zA-Z]{2,5}$/", $value ) ) {
								$any_bad_inputs = true;
								$bad_input = true;
							}
							break;

						case "delivery_country":
							if ( ($value != null ) ) {
								wpsc_update_customer_meta( 'shipping_country', $value );
							}
							break;

						default:
							if ( empty( $value ) )
								$bad_input = true;
							break;
					}
					if ( $bad_input === true ) {

						switch ( $form_data['name'] ) {
							case __( 'First Name', SH_NAME ):
								$bad_input_message .= __( 'Please enter a valid name', SH_NAME ) . "";
								break;

							case __( 'Last Name', SH_NAME ):
								$bad_input_message .= __( 'Please enter a valid surname', SH_NAME ) . "";
								break;

							case __( 'Email', SH_NAME ):
								$bad_input_message .= __( 'Please enter a valid email address', SH_NAME ) . "";
								break;

							case __( 'Address 1', SH_NAME ):
							case __( 'Address 2', SH_NAME ):
								$bad_input_message .= __( 'Please enter a valid address', SH_NAME ) . "";
								break;

							case __( 'City', SH_NAME ):
								$bad_input_message .= __( 'Please enter your town or city.', SH_NAME ) . "";
								break;

							case __( 'Phone', SH_NAME ):
								$bad_input_message .= __( 'Please enter a valid phone number', SH_NAME ) . "";
								break;

							case __( 'Country', SH_NAME ):
								$bad_input_message .= __( 'Please select your country from the list.', SH_NAME ) . "";
								break;

							default:
								$bad_input_message .= sprintf(__( 'Please enter a valid <span class="wpsc_error_msg_field_name">%s</span>.', SH_NAME ), esc_html($form_data['name']) );
								break;
						}
						$bad_input_message .= "<br />";
					} else {
						$meta_data[$value_id] = $value;
					}
				} else {
					$meta_data[$value_id] = $value;
				}
			}				
			$meta_data = apply_filters( 'wpsc_user_log_update', $meta_data, $user_ID );
			wpsc_update_customer_meta( 'checkout_details', $meta_data );
		}
		if (!isset($bad_input_message) || empty($bad_input_message)) {
			$message = '<p class="alert-success alert">'.__( 'Thanks, your changes have been saved.', SH_NAME ).'</p>';
		} else {
			$message = '<p class="alert alert-danger">'.$bad_input_message.'</p>';
		}
	}

	return apply_filters( 'wpsc_profile_message', $message );
}

/**
 * wpsc_display_form_fields_child()
 *
 * This function displays each of the form fields.  Each of them are filterable via 'wpsc_account_form_field_$tag' where tag is permalink-styled name or uniquename.
 * i.e. First Name under Shipping would be 'wpsc_account_form_field_shippingfirstname' - while Your Billing Details would be filtered
 * via 'wpsc_account_form_field_your-billing-details'.
 *
 * @global <type> $wpdb
 * @global <type> $user_ID
 * @global <type> $wpsc_purchlog_statuses
 * @global <type> $gateway_checkout_form_fields
 */
function wpsc_display_form_fields_child() {
// Field display and Data saving function


	do_action( 'wpsc_start_display_user_log_form_fields' );
	$wpsc_checkout = wpsc_core_get_checkout();

	$i = 0;
	while ( wpsc_have_checkout_items() ) {
		wpsc_the_checkout_item();

		if ( wpsc_checkout_form_is_header() ) {
			$i ++;

			// display headers for form fields
			?>

            <?php if ( $i > 1 ) {?>
            	</table>
            	<table>
            <?php } ?>

               <tr class="checkout-heading-row <?php echo esc_attr( wpsc_the_checkout_item_error_class( false ) );?>">
	               <td <?php wpsc_the_checkout_details_class(); ?> colspan='2'>
	               		<h4><?php echo esc_html( wpsc_checkout_form_name() );?></h4>
	               </td>
               </tr>
               <?php if ( wpsc_is_shipping_details() ) {?>
               <tr class='same_as_shipping_row'>
		            <td colspan='2'>
		               	<?php
		               	$checked = '';
		               	$shipping_same_as_billing = wpsc_get_customer_meta( 'shippingSameBilling' );
		               	if ( $shipping_same_as_billing ) {
							$checked = 'checked="checked"';
						}
						?>
						<label for='shippingSameBilling'>
							<input type='checkbox'
								value='true'
								data-wpsc-meta-key="shippingSameBilling"
								class="wpsc-visitor-meta"
								name='shippingSameBilling'
								id='shippingSameBilling' <?php echo $checked; ?> />
							<?php _e( 'Same as billing address:', SH_NAME ); ?>
						</label>
						<br>
						<span id="shippingsameasbillingmessage"><?php _e('Your orders will be shipped to the billing address', SH_NAME); ?></span>
					</td>
			</tr>
			<?php }

			// Not a header so start display form fields
		} elseif ( $wpsc_checkout->checkout_item->unique_name == 'billingemail' ) {
			?>
               <?php

               	$email_markup = "<div class='wpsc_email_address'>
                  <p class='" . wpsc_checkout_form_element_id() . "'>
                     <label class='wpsc_email_address' for='" . wpsc_checkout_form_element_id() . "'>
                     " . __( 'Enter your email address', SH_NAME ) . "
                     </label>
                  <p class='wpsc_email_address_p'>
                  <img src='https://secure.gravatar.com/avatar/empty?s=60&amp;d=mm' id='wpsc_checkout_gravatar' />
                  " . wpsc_checkout_form_field();

			if ( wpsc_the_checkout_item_error() != '' )
				$email_markup .= "<p class='validation-error'>" . wpsc_the_checkout_item_error() . "</p>";
			$email_markup .= "</div>";
		} else {
			?>
			<tr>
				<td class='<?php echo wpsc_checkout_form_element_id(); ?>'>
					<label
						for='<?php echo esc_attr( wpsc_checkout_form_element_id() ); ?>'>
		                <?php echo wpsc_checkout_form_name();?>
		            </label>
                </td>
                <td>
                  <?php echo wpsc_checkout_form_field();?>
                   <?php if ( wpsc_the_checkout_item_error() != '' ) { ?>
                          <p class='validation-error'><?php echo wpsc_the_checkout_item_error(); ?></p>
                  <?php } ?>
               </td>
            </tr>
         <?php }//endif; ?>
      <?php } // end while ?>
    <?php
	echo wp_nonce_field( 'wpsc_user_profile', '_wpsc_user_profile' );		
}
/**
 * Displays the Account Page tabs
 *
 * @access public
 * @since 3.8.10
 *
 */
function wpsc_user_profile_links_child( $args = array() ) {
	global $current_tab, $separator;

	$defaults = array (
 		'before_link_list' => '<ul class="nav nav-tabs" role="tablist">',
 		'after_link_list'  => '</ul>',
 		'before_link_item' => '<li',
 		'after_link_item'  => '<li>',
 		'link_separator'   => ''
	);

	$args = wp_parse_args( $args, $defaults );

	$profile_tabs = apply_filters( 'wpsc_user_profile_tabs', array(
		'purchase_history' => __( 'Purchase History', SH_NAME ),
		'edit_profile'     => __( 'Your Details', SH_NAME )
	) );

	echo $args['before_link_list'];

	$i = 0;
	$user_account_url = get_option( 'user_account_url' );
	$links = array();
	foreach ( $profile_tabs as $tab_id => $tab_title ) :
		$tab_link = $args['before_link_item'].($current_tab == $tab_id ? ' class="active">' : '>');
		$tab_url = add_query_arg( 'tab', $tab_id, $user_account_url );
		$tab_link .= sprintf(
			'<a href="%1$s" class="%2$s">%3$s</a>',
			esc_url( $current_tab == $tab_id ? '#' : $tab_url),
			esc_attr( $current_tab == $tab_id ? 'current' : '' ),
			$tab_title
		);
		$tab_link .= $args['after_link_item'];
		$links[] = $tab_link;
	endforeach;

	echo implode( $args['link_separator'], $links );

	echo $args['after_link_list'];
}
