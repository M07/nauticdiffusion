<?php if( ! defined('ABSPATH')) exit('restricted access'); /** Do not delete these lines */?>
<?php

/** if post is password protected then password is required */
if ( post_password_required() ):?>

<p class="alert">
    <?php _e( 'This product is password protected. Enter the password to view reivews.', SH_NAME ); ?>
</p>
<?php return;
endif;?>


<div class="commnts-wrap" id="comments">

    <?php
	/** Let's Seperate the comments from Trackbacks */
	if(have_comments()):?>
    	
            <h3><?php _e('Comments', SH_NAME);?></h3>
            <ul class="comments">
                <?php wp_list_comments( 'format=xhtml&callback=sh_list_comments&style=list' ); /** Callback to Comments */ ?>
            </ul>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
        <div class="pagination">
            <?php paginate_comments_links(); ?>
        </div>
        <?php endif; ?>
    <?php else: 
            /** If comments are open, but there are no comments. */
            if( ! comments_open()) : ?>
            <h3>
                <?php _e( 'Sorry reivews are closed for this Post.', SH_NAME ); ?>
            </h3>
    		<?php endif;
	endif; ?>
    
</div>


    <?php if( comments_open()) : ?>
    	<?php sh_comment_form(); ?>
    <?php endif; ?>

<!-- comment-area ends --> 

