<?php
global $wpsc_cart, $wpdb, $wpsc_checkout, $wpsc_gateway, $wpsc_coupons, $wpsc_registration_error_messages;
$wpsc_checkout = new wpsc_checkout();
$alt = 0;
$coupon_num = wpsc_get_customer_meta( 'coupon' );
if( $coupon_num )
   $wpsc_coupons = new wpsc_coupons( $coupon_num );

if(wpsc_cart_item_count() < 1) :
   _e('Oops, there is nothing in your cart.', 'wpsc') . "<a href=" . esc_url( get_option( "product_list_url" ) ) . ">" . __('Please visit our shop', 'wpsc') . "</a>";
   return;
endif;
?>
<div id="checkout_page_container">
  <h3><?php _e('Please review your order', SH_NAME); ?></h3>
  <div class="shopping-cart">

        <ul class="title clearfix">
            <li><?php _e('Image', SH_NAME); ?></li>
            <li class="second"><?php _e('Product Name', SH_NAME); ?></li>
            <li><?php _e('Quantity', SH_NAME); ?></li>
            <li><?php _e('Price', SH_NAME); ?></li>
            <li><?php _e('Total', SH_NAME); ?></li>
            <li class="last"><?php _e('Action', SH_NAME); ?></li>
        </ul>
    <?php while (wpsc_have_cart_items()) : wpsc_the_cart_item(); ?>
        <ul class=" clearfix product_row_<?php echo wpsc_the_cart_item_key(); ?>">
            <li class="wpsc_product_image_<?php echo wpsc_the_cart_item_key(); ?>">
               <?php if('' != wpsc_cart_item_image()): ?>
          <figure>
            <?php do_action ( "wpsc_before_checkout_cart_item_image" ); ?>
                        <img src="<?php echo wpsc_cart_item_image(97, 64); ?>" alt="<?php echo wpsc_cart_item_name(); ?>" title="<?php echo wpsc_cart_item_name(); ?>" class="product_image" />
                        <?php do_action ( "wpsc_after_checkout_cart_item_image" ); ?>
                    </figure>
                 <?php else:?>
                    <div class="item_no_image">
                        <?php do_action ( "wpsc_before_checkout_cart_item_image" ); ?>
                       <a href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>">
                       <span><?php _e('No Image',SH_NAME); ?></span>
        
                       </a>
                        <?php do_action ( "wpsc_after_checkout_cart_item_image" ); ?>
                    </div>
                 <?php endif; ?>
            </li>
            <li class="second">
                <h4>
          <?php do_action ( "wpsc_before_checkout_cart_item_name" ); ?>
                    <?php echo wpsc_cart_item_name(); ?>
                    <?php do_action ( "wpsc_after_checkout_cart_item_name" ); ?>
              </h4>
            </li>
            <li>
                <form action="<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>" method="post" class="adjustform qty">
                   <input type="number" name="quantity" size="2" value="<?php echo wpsc_cart_item_quantity(); ?>" class="input-text" />
                   <input type="hidden" name="key" value="<?php echo wpsc_the_cart_item_key(); ?>" />
                   <input type="hidden" name="wpsc_update_quantity" value="true" />
                   <button class="button" type="submit"><span class="red-button black"><?php _e('Update', SH_NAME); ?></span></button>
              </form>
            </li>
            <li><?php echo wpsc_cart_single_item_price(); ?></li>
            <li><?php echo wpsc_cart_item_price(); ?></li>
            <li class="last"> 
            <form action="<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>" method="post" class="adjustform remove">
               <input type="hidden" name="quantity" value="0" />
               <input type="hidden" name="key" value="<?php echo wpsc_the_cart_item_key(); ?>" />
               <input type="hidden" name="wpsc_update_quantity" value="true" />
               <input type="submit" class="checkout_button checkout_red" value="<?php _e('Delete', SH_NAME);?>" />
            </form></li>
        </ul>
        <?php endwhile;?>

        <a href="<?php home_url()?>" class="continue-shopping checkout_button checkout_blue"><?php _e('Continue Shopping', SH_NAME);?> </a>
    </div>
  
   <!-- cart contents table close -->
  <?php if(wpsc_uses_shipping()): ?>
     <p class="wpsc_cost_before"></p>
   <?php endif; ?>
   <?php  //this HTML dispalys the calculate your order HTML   ?>

   <?php if(wpsc_has_category_and_country_conflict()): ?>
      <p class='validation-error'><?php echo esc_html( wpsc_get_customer_meta( 'category_shipping_conflict' ) ); ?></p>
   <?php endif; ?>

   <?php if(isset($_SESSION['WpscGatewayErrorMessage']) && $_SESSION['WpscGatewayErrorMessage'] != '') :?>
      <p class="validation-error"><?php echo $_SESSION['WpscGatewayErrorMessage']; ?></p>
   <?php
   endif;
   ?>
   
  <div class="row cart-calculator clearfix">
    <?php if(wpsc_uses_shipping()) : ?>
        <div class="span4">
            <h6><?php _e('Estimate Shipping & Taxes', SH_NAME); ?></h6>
            <div class="estimate clearfix">
        <?php if ( ! wpsc_have_shipping_quote() ) : // No valid shipping quotes ?>
                    <?php if ( ! wpsc_have_valid_shipping_zipcode() ) : ?>
                    <p class="wpsc_update_location"><span class="shipping_error"><?php _e('Please provide a Zipcode and click Calculate in order to continue.', SH_NAME); ?></span></p>
                    
                    <?php else: ?>
                      <p class="wpsc_update_location_error"><span class="shipping_error"><?php _e('Sorry, online ordering is unavailable to this destination and/or weight. Please double check your destination details.', SH_NAME); ?></span></p>
                    <?php endif; ?>
                 <?php endif; ?>
                 <form name='change_country' id='change_country' action='' method='post'>
          <?php echo wpsc_shipping_country_list();?>
                  <input type='hidden' name='wpsc_update_location' value='true' />
                  
                  <input class="red-button" type='submit' value="<?php esc_attr_e( 'Calculate', SH_NAME ); ?>" name='wpsc_submit_zipcode' />
                
                  </form>
          
            </div>
        </div>
        <?php endif;?>
        
    <?php if(wpsc_uses_coupons()): ?>

            <div class="span4">
                <h6><?php _e('Enter coupon code :', SHNAME); ?></h6>
                <div class="estimate clearfix">
                    <?php if(wpsc_coupons_error()): ?>
                        <p><?php _e('Coupon is not valid.', SH_NAME); ?></p>
                    <?php endif;?>
                     <form  method="post" action="<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>">
                        <input type="text" name="coupon_num" placeholder="<?php _e('Your Coupon Number', SH_NAME);?>" id="coupon_num" value="<?php echo $wpsc_cart->coupons_name; ?>" />
                        <input type="submit" class="red-button" value="<?php _e('Update', SH_NAME) ?>" />
                    </form>
                </div>
            </div>
        
    <?php endif;?>
      <div class="price-container">
        <div class="total clearfix">
            <span class="price-label"><?php _e('Total:', SH_NAME)?></span><span class="price"><?php echo wpsc_cart_total(); ?></span>
        </div>
      </div>
    </div>

   <?php do_action('wpsc_before_shipping_of_shopping_cart'); ?>

   <div id="wpsc_shopping_cart_container">

   <?php
      $wpec_taxes_controller = new wpec_taxes_controller();
      if($wpec_taxes_controller->wpec_taxes_isenabled()):
   ?>
      <table class="productcart">
         <tr class="total_price total_tax">
            <td colspan="3">
               <?php echo wpsc_display_tax_label(true); ?>
            </td>
            <td colspan="2">
               <span id="checkout_tax" class="pricedisplay checkout-tax"><?php echo wpsc_cart_tax(); ?></span>
            </td>
         </tr>
      </table>
   <?php endif; ?>
   <?php do_action('wpsc_before_form_of_shopping_cart'); ?>

  <?php if( ! empty( $wpsc_registration_error_messages ) ): ?>
    <p class="validation-error">
    <?php
    foreach( $wpsc_registration_error_messages as $user_error )
     echo $user_error."<br />\n";
    ?>
  <?php endif; ?>

  <?php if ( wpsc_show_user_login_form() && !is_user_logged_in() ): ?>
      <p><?php _e('You must sign in or register with us to continue with your purchase', 'wpsc');?></p>
      <div class="wpsc_registration_form">

        <fieldset class='wpsc_registration_form'>
          <h2><?php _e( 'Sign in', 'wpsc' ); ?></h2>
          <?php
          $args = array(
            'remember' => false,
                      'redirect' => home_url( $_SERVER['REQUEST_URI'] )
          );
          wp_login_form( $args );
          ?>
          <div class="wpsc_signup_text"><?php _e('If you have bought from us before please sign in here to purchase', 'wpsc');?></div>
        </fieldset>
      </div>
  <?php endif; ?>
  <div class="checkout_forms_container">
    <div class="checkout_forms_container_head">
      <h3><?php _e('Billing and Shipping', 'wpsc');?><h3>
    </div>
    <div class="checkout_forms_container_body">
      <form class='wpsc_checkout_forms' action='<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>' method='post' enctype="multipart/form-data">
          <?php
          /**
           * Both the registration forms and the checkout details forms must be in the same form element as they are submitted together, you cannot have two form elements submit together without the use of JavaScript.
          */
          ?>

        <?php if(wpsc_show_user_login_form()):
              global $current_user;
              get_currentuserinfo();   ?>

        <div class="wpsc_registration_form">

              <fieldset class='wpsc_registration_form wpsc_right_registration'>
                <h2><?php _e('Join up now', 'wpsc');?></h2>

            <label><?php _e('Username:', 'wpsc'); ?></label>
            <input type="text" name="log" id="log" value="" size="20"/><br/>

            <label><?php _e('Password:', 'wpsc'); ?></label>
            <input type="password" name="pwd" id="pwd" value="" size="20" /><br />

            <label><?php _e('E-mail', 'wpsc'); ?>:</label>
                  <input type="text" name="user_email" id="user_email" value="" size="20" /><br />
                  <div class="wpsc_signup_text"><?php _e('Signing up is free and easy! please fill out your details your registration will happen automatically as you checkout. Don\'t forget to use your details to login with next time!', 'wpsc');?></div>
              </fieldset>

            </div>
            <div class="clear"></div>
       <?php endif; // closes user login form
          $misc_error_messages = wpsc_get_customer_meta( 'checkout_misc_error_messages' );
          if( ! empty( $misc_error_messages ) ): ?>
             <div class='login_error'>
                <?php foreach( $misc_error_messages as $user_error ){?>
                   <p class='validation-error'><?php echo $user_error; ?></p>
                   <?php } ?>
             </div>

          <?php
          endif;
          ?>
    <?php ob_start(); ?>
       <table class='wpsc_checkout_table table-1'>
          <?php $i = 0;
          while (wpsc_have_checkout_items()) : wpsc_the_checkout_item(); ?>

            <?php if(wpsc_checkout_form_is_header() == true){
                   $i++;
                   //display headers for form fields ?>
                   <?php if($i > 1):?>
                      </table>
                      <table class='wpsc_checkout_table table-<?php echo $i; ?>'>
                   <?php endif; ?>

                   <tr <?php echo wpsc_the_checkout_item_error_class();?>>
                      <td <?php wpsc_the_checkout_details_class(); ?> colspan='2'>
                         <h4><?php echo wpsc_checkout_form_name();?></h4>
                      </td>
                   </tr>
                   <?php if(wpsc_is_shipping_details()):?>
                   <tr class='same_as_shipping_row'>
                      <td colspan ='2'>
                      <?php $checked = '';
                      $shipping_same_as_billing = wpsc_get_customer_meta( 'shipping_same_as_billing' );
                      if(isset($_POST['shippingSameBilling']) && $_POST['shippingSameBilling'])
                         $shipping_same_as_billing = true;
                      elseif(isset($_POST['submit']) && !isset($_POST['shippingSameBilling']))
                        $shipping_same_as_billing = false;
                      wpsc_update_customer_meta( 'shipping_same_as_billing', $shipping_same_as_billing );
                        if( $shipping_same_as_billing )
                          $checked = 'checked="checked"';
                       ?>
              <label for='shippingSameBilling'><?php _e('Same as billing address:','wpsc'); ?></label>
              <input type='checkbox'  data-wpsc-meta-key="shippingSameBilling" value='true' class= "wpsc-visitor-meta" name='shippingSameBilling' id='shippingSameBilling' <?php echo $checked; ?> />
              <br/><span id="shippingsameasbillingmessage"><?php _e('Your order will be shipped to the billing address', 'wpsc'); ?></span>
                      </td>
                   </tr>
                   <?php endif;

                // Not a header so start display form fields
                }elseif( $wpsc_checkout->checkout_item->unique_name == 'billingemail'){ ?>
                   <?php $email_markup =
                   "<div class='wpsc_email_address'>
                      <p class='" . wpsc_checkout_form_element_id() . "'>
                         <label class='wpsc_email_address' for='" . wpsc_checkout_form_element_id() . "'>
                         " . __('Enter your email address', 'wpsc') . "
                         </label>
                      <p class='wpsc_email_address_p'>
                      <img src='https://secure.gravatar.com/avatar/empty?s=60&amp;d=mm' id='wpsc_checkout_gravatar' />
                      " . wpsc_checkout_form_field();

                       if(wpsc_the_checkout_item_error() != '')
                          $email_markup .= "<p class='validation-error'>" . wpsc_the_checkout_item_error() . "</p>";
                   $email_markup .= "</div>";
                 }else{ ?>
          <tr>
                   <td class='<?php echo wpsc_checkout_form_element_id(); ?>'>
                      <label for='<?php echo wpsc_checkout_form_element_id(); ?>'>
                      <?php echo wpsc_checkout_form_name();?>
                      </label>
                   </td>
                   <td>
                      <?php echo wpsc_checkout_form_field();?>
                       <?php if(wpsc_the_checkout_item_error() != ''): ?>
                              <p class='validation-error'><?php echo wpsc_the_checkout_item_error(); ?></p>
                      <?php endif; ?>
                   </td>
                </tr>

             <?php }//endif; ?>

          <?php endwhile; ?>

    <?php
      $buffer_contents = ob_get_contents();
      ob_end_clean();
      if(isset($email_markup))
        echo $email_markup;
      echo $buffer_contents;
    ?>

          <?php if (wpsc_show_find_us()) : ?>
          <tr>
             <td><label for='how_find_us'><?php _e('How did you find us' , 'wpsc'); ?></label></td>
             <td>
                <select name='how_find_us'>
                   <option value='Word of Mouth'><?php _e('Word of mouth' , 'wpsc'); ?></option>
                   <option value='Advertisement'><?php _e('Advertising' , 'wpsc'); ?></option>
                   <option value='Internet'><?php _e('Internet' , 'wpsc'); ?></option>
                   <option value='Customer'><?php _e('Existing Customer' , 'wpsc'); ?></option>
                </select>
             </td>
          </tr>
          <?php endif; ?>
          <?php do_action('wpsc_inside_shopping_cart'); ?>

          <?php  //this HTML displays activated payment gateways   ?>
          <?php if(wpsc_gateway_count() > 1): // if we have more than one gateway enabled, offer the user a choice ?>
             <tr>
                <td colspan='2' class='wpsc_gateway_container'>
                   <h3><?php _e('Payment Type', 'wpsc');?></h3>
                   <?php wpsc_gateway_list(); ?>
                   </td>
             </tr>
          <?php else: // otherwise, there is no choice, stick in a hidden form ?>
             <tr>
                <td colspan="2" class='wpsc_gateway_container'>
                   <?php wpsc_gateway_hidden_field(); ?>
                </td>
             </tr>
          <?php endif; ?>

          <?php if(wpsc_has_tnc()) : ?>
             <tr>
                <td colspan='2'>
                    <label for="agree"><input id="agree" type='checkbox' value='yes' name='agree' /> <?php printf(__("I agree to the <a class='thickbox' target='_blank' href='%s' class='termsandconds'>Terms and Conditions</a>", "wpsc"), esc_url( home_url( "?termsandconds=true&amp;width=360&amp;height=400" ) ) ); ?> <span class="asterix">*</span></label>
                   </td>
             </tr>
          <?php endif; ?>
          </table>

    <!-- div for make purchase button -->
          <div class='wpsc_make_purchase'>
             <span>
                <?php if(!wpsc_has_tnc()) : ?>
                   <input type='hidden' value='yes' name='agree' />
                <?php endif; ?>
                   <input type='hidden' value='submit_checkout' name='wpsc_action' />
                   <input type='submit' value='<?php _e('Purchase', 'wpsc');?>' class='make_purchase wpsc_buy_button checkout_button checkout_blue' />
             </span>
          </div>

        <div class='clear'></div>
      </form>
    </div>
  </div>
</div>
</div><!--close checkout_page_container-->
<?php
do_action('wpsc_bottom_of_shopping_cart');