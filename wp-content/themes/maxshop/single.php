<?php get_header();?>
<?php global $post;
global $more;
$more = 1;
$meta_settings = get_post_meta($post->ID, 'sh_post_meta', true); //printr($meta_setting);
$meta_setting = sh_set( $meta_settings, 'sh_post_options' ); 
$page_banner = sh_set( sh_set($meta_setting, 0), 'page_banner');
$layout = sh_set( sh_set($meta_setting, 0), 'layout');
$sidebar = sh_set( sh_set($meta_setting, 0), 'sidebar');
?>

<!-- BAR -->
<div class="bar-wrap">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="title-bar" <?php echo ($page_banner)? 'style="background-image:url('.$page_banner.');"' : ''; ?>>
                    <h1><?php echo get_the_title($post->ID); ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BAR -->

<!-- PRODUCT-OFFER -->
<div class="blog_wrap">

    <div class="container">
        <div class="row">
        	<?php if($sidebar && $layout == 'left'):?>
            	<div class="span3">
                    <div id="sidebar2">
                        <?php dynamic_sidebar( sh_set(sh_set($meta_setting,0), 'sidebar', 'default-sidebar') ); ?>
                    </div>
            	</div>
            <?php endif;?>
            <div class="<?php echo ($layout == 'full' || $layout == '') ? 'span12' : 'span9';?> blog">
                <?php while(have_posts()): the_post();?>
                <article class="clearfix">
                    
                    <figure><?php the_post_thumbnail( 'blog-listing' ); ?></figure>
                    
                    <h2><?php the_title(); ?></h2>
                    
                    <div class="post-content clearfix">
						<?php the_content();?>
                    </div>
					
					<?php wp_link_pages(); ?>
                    <ul class="post-meta clearfix">
                        <li><a class="comment-icon" href="#comments"><?php echo comments_number(); ?></a></li>
                        <li><span><?php echo get_the_date(get_option('date_formate')); ?></span></li>
                        <li class="tags"><?php the_tags();?></li>
                    </ul>
                </article>
                <?php endwhile;?>
                
               	<?php comments_template(); ?>
    		</div>
    		 <?php if($sidebar && $layout == 'right'):?>
            	<div class="span3">
                    <div id="sidebar2">
                        <?php dynamic_sidebar( sh_set(sh_set($meta_setting,0), 'sidebar', 'default-sidebar') ); ?>
                    </div>
            	</div>
            <?php endif;?>
        </div>
    </div>
</div>    

<?php get_footer();?>
