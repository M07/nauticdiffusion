<?php get_header();?>

<?php $object = get_queried_object();

$sidebar = '';
if( isset( $object->ID ) ) {
	$meta_settings = get_post_meta($object->ID, 'sh_post_meta', true); 
	$meta_setting = sh_set( $meta_settings, 'sh_post_options' ); 
	$sidebar = sh_set(sh_set($meta_setting,0), 'sidebar', 'default-sidebar');
}
$theme_options = _WSH()->option();

if( !$object ) $page_banner = sh_set( $theme_options, 'home_page_header_image', get_template_directory_uri()."/media/page-title-bg.jpg");
else $page_banner = sh_set( sh_set($meta_setting, 0), 'page_banner');?>

<!-- BAR -->
<div class="bar-wrap">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="title-bar" <?php echo ($page_banner)? 'style="background-image:url('.$page_banner.');"' : ''; ?>>
                    <h1><?php echo ( !$object) ? get_bloginfo('name') : get_the_title($object->ID); ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BAR -->

<!-- PRODUCT-OFFER -->
<div class="blog_wrap">
    
    <div class="container">
        <div class="row">
        	<div class="span9 blog">
				<?php get_template_part( 'includes/modules/blog' ); ?>
                <?php _the_pagination(); ?>
        	</div>
        
			<div class="span3">
            	<div id="sidebar2">
        			<?php if( $sidebar ) dynamic_sidebar( $sidebar );
					else dynamic_sidebar( 'default-sidebar' ); ?>
                </div>
            </div>
		</div>
    </div>
    
</div>


<?php get_footer();?>