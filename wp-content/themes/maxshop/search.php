<?php get_header();
$theme_options = get_option('sh_aplus_option');
$header_image = sh_set( $theme_options, 'search_page_header_image', get_template_directory_uri()."/media/page-title-bg.jpg");?>

<!-- BAR -->
<div class="bar-wrap">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="title-bar" <?php echo ($header_image)? 'style="background-image:url('.$header_image.');"' : ''; ?>>
                    <h1>
						<?php printf( __('Search Results for "%s"', SH_NAME ), get_search_query());?>
            		</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- PRODUCT-OFFER -->
<div class="blog_wrap">
    
    <div class="container">
        <div class="row">
        	<div class="span9 blog">
				<?php get_template_part( 'includes/modules/blog' ); ?>
                <?php _the_pagination(); ?>
        	</div>
        
			<div class="span3">
            	<div id="sidebar2">
        			<?php dynamic_sidebar( 'blog' ); ?>
                </div>
            </div>
		</div>
    </div>
    
</div>
<?php get_footer();?>