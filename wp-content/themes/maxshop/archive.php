<?php get_header();?>
<?php $object = get_queried_object();
$theme_options = get_option('sh_aplus_option');
$header_image = sh_set( $theme_options, 'archive_page_header_image', get_template_directory_uri()."/media/page-title-bg.jpg");?>

<!-- BAR -->
<div class="bar-wrap">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="title-bar" <?php echo ($header_image)? 'style="background-image:url('.$header_image.');"' : ''; ?>>
                    <h1>
						<?php if ( is_day() ) : ?>
							<?php printf( __('Daily Archives: %s', SH_NAME) , get_the_date() ); ?>
                        <?php elseif ( is_month() ) : ?>
                                <?php printf( __('Monthly Archives: %s', SH_NAME), get_the_date( 'F Y' ) ); ?>
                        <?php elseif ( is_year() ) : ?>
                                <?php printf( __('Yearly Archives: %s', SH_NAME), get_the_date( 'Y' ) ); ?>
                        <?php else : ?>
                                <?php _e('Blog Archives', SH_NAME); ?>
                        <?php endif; ?> 
            		</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BAR -->  
<!-- PRODUCT-OFFER -->
<div class="blog_wrap">
    
    <div class="container">
        <div class="row">
        	<div class="span9 blog">
				<?php get_template_part( 'includes/modules/blog' ); ?>
                <?php _the_pagination(); ?>
        	</div>
        
			<div class="span3">
            	<div id="sidebar2">
        			<?php dynamic_sidebar( 'blog' ); ?>
                </div>
            </div>
		</div>
    </div>
    
</div>
<?php get_footer();?>