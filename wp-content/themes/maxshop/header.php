<!DOCTYPE html>
<?php $settings = _WSH()->option();?>

<html <?php language_attributes(); ?>>
<head>
    <title><?php if(is_home() || is_front_page())
	 	echo get_bloginfo('name') . ' - ' . get_bloginfo('description');
		else {
			wp_title(''); echo ' - ' . get_bloginfo( 'name' );
		}?>
    </title>
    <?php echo ( sh_set( $settings, 'favicon' ) ) ? '<link rel="icon" type="image/png" href="'.sh_set( $settings, 'favicon' ).'">': '';?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if lt IE 8]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
    <![endif]-->
    <!--[if IE 8 ]> <html class="ie8"> <![endif]-->
    <!--[if IE 9 ]> <html class="ie9"> <![endif]-->
    <!--[if IE 7]>
    <script type="text/javascript">
    location.replace("http://windows.microsoft.com/en-US/internet-explorer/products/ie/home");
    </script>
    <![endif]-->

    <?php wp_head();?>
</head>

<body id="index" <?php body_class(); ?>>
	<?php if( class_exists('WP_eCommerce') ): ?>
        <div class="header-bar">
            <div class="container">
                <div class="row">
                    <div class="span12 right">
                        <div class="social-strip">
                            <ul>
                                <li><a href="<?php echo get_option('user_account_url'); ?>" class="account"><?php _e('My Account', SH_NAME);?></a></li>
                                <?php if( $tpl_wishlist = sh_page_template('tpl-wishlist.php') ): ?>
                                    <li><a href="<?php echo get_permalink($tpl_wishlist->ID); ?>" class="wish"><?php _e('Wishlist', SH_NAME);?></a></li>
                                <?php endif; ?>
                                <?php if( get_option('checkout_url') ): ?>
                                    <li><a href="<?php echo get_option('checkout_url'); ?>" class="check"><?php _e('Checkout', SH_NAME);?></a></li>
                                <?php endif;?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif;?>

<!-- // Top header started -->
<div class="header-top">
    <div class="container">
        <div class="row">

            <div class="span5">
                <div class="logo">
                    
                    
                        <?php if( sh_set($settings, 'logo_image') ): ?>
                            <a title="<?php bloginfo('name'); ?>" href="<?php echo home_url();?>" >
                            	<img alt="logo" width="<?php echo sh_set($settings, 'logo_width');?>" height="<?php echo sh_set($settings, 'logo_height');?>" src="<?php echo sh_set($settings, 'logo_image', get_template_directory_uri().'/images/logo.jpg');?>">
                            </a>
                            <?php if( get_bloginfo( 'description' ) ): ?>
                            	<h1><a href="<?php echo home_url(); ?>" title="<?php bloginfo('description'); ?>"><?php bloginfo('description'); ?></a></h1>
                            <?php endif; ?>
                        <?php else: ?>
                            <h1 class="heading1"><a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1>
                            
                            <?php if( get_bloginfo( 'description' ) ): ?>
                            	<h1><a href="<?php echo home_url(); ?>" title="<?php bloginfo('description'); ?>"><?php bloginfo('description'); ?></a></h1>
                            <?php endif; ?>
                        <?php endif; ?>
                    

                </div>
            </div>

            <div class="span4">
                <form action="<?php echo home_url();?>" method="get">
                    <input name="s" type="text" placeholder="<?php _e('Type and hit enter', SH_NAME);?>">
                    <input type="submit" value="">
                </form>
            </div>
            <?php if( function_exists( 'wpsc_cart_item_count' ) ): ?>
                <div class="span3">
                    <div class="cart shopping-cart-wrapper">
                        <ul>
                            <li class="first"><a href="<?php echo get_option('shopping_cart_url'); ?>"></a><span></span></li>
                            <li><?php echo wpsc_cart_item_count(); ?> <?php _e('item(s)', SH_NAME); ?> - <?php echo strip_tags(wpsc_cart_total(true)); ?></li>
                        </ul>
                    </div>
                </div>
            <?php endif;?>

        </div>
    </div>
</div>
<!-- END top header -->    

<header>
    <div class="container">
        <div class="row">
            <div class="span12">
                <nav class="desktop-nav">
                    <ul class="clearfix">
					 	<?php wp_nav_menu(
                            array(
                                'container' => false, 
                                'items_wrap' => '%3$s',	
                                'theme_location' => 'main_menu', 
                                'depth' => 3, 
                                //'walker' => new maxshop_walker_nav_menu
                            )
                        );?>  
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>

<!-- Start wrapper -->
<div class="wrapper">