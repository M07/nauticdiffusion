
(function($){
	"use strict";
	var onlineshop = {			
			count: 0,
			tweets: function( options, selector ){
				
				options.action = '_ajax_callback';
				options.subaction = 'tweets';

				$.ajax({
					url: ajaxurl,
					type: 'POST',
					data:options,
					//dataType:"json",
					success: function(res){
						
						$(selector).html( res );	
					}
				});
				
			},
			
			wishlist: function(options, selector)
			{
				options.action = '_ajax_callback';
				
				if( $(selector).data('_sh_add_to_wishlist') === true ){
					onlineshop.msg( 'You have already done this job', 'error' );
					return;
				}
				
				$(selector).data('_sh_add_to_wishlist', true );

				onlineshop.loading(true);
				
				$.ajax({
					url: ajaxurl,
					type: 'POST',
					data:options,
					dataType:"json",
					success: function(res){

						try{
							var newjason = res;

							if( newjason.code === 'fail'){
								$(selector).data('_sh_add_to_wishlist', false );
								onlineshop.loading(false);
								onlineshop.msg( newjason.msg, 'error' );
							}else if( newjason.code === 'exists'){
								$(selector).data('_sh_add_to_wishlist', true );
								onlineshop.loading(false);
								onlineshop.msg( newjason.msg, 'error' );
							}else if( newjason.code === 'success' ){
								onlineshop.loading(false);
								$(selector).data('_sh_add_to_wishlist', true );
								onlineshop.msg( newjason.msg, 'success' );
							}else if( newjason.code === 'del' ){
								onlineshop.loading(false);
								$(selector).data('_sh_add_to_wishlist', true );
								$(selector).parents('tr').remove();
								onlineshop.msg( newjason.msg, 'success' );
							}
							
							
						}
						catch(e){
							onlineshop.loading(false);
							$(selector).data('_sh_add_to_wishlist', false );
							onlineshop.msg( 'There was an error while adding product to whishlist '+e.message, 'error' );
							
						}
					}
				});
			},
			loading: function( show ){
				if( $('.loading' ).length === 0 ) {
					$('body').append('<div class="loading" style="display:none;"></div>');
				}
				
				if( show === true ){
					$('.loading').show('slow');
				}
				if( show === false ){
					$('.loading').hide('slow');
				}
			},
			
			form_click : function(){
    
				$('input[name="search_query"]').keyup( function(e){
				 
				 var value = $(this).val();
				 
				 $.ajax({
				  url: ajaxurl,
				  method: 'POST',
				  data: 'action=_ajax_callback&subaction=live_serch&query='+value,
				  success: function(res){
				   //console.log(res);
				   $('.product-compare-list').html(res);
				  }
				 });
				 
				});
    
    
				$('.product-compare-list > ul > li').live('click', function(e){
				 e.preventDefault();
				 
				 var data_id = $(this).data('id');
				 
				 $(this).parent('ul').slideUp();
				 
				 $('input[name="search_query"]').val($(this).text());
				 
				 $.ajax({
				  url: ajaxurl,
				  method: 'POST',
				  data: 'action=_ajax_callback&subaction=live_serch_res&data_id='+data_id,
				  success: function(res){
				   //console.log(res);
				   $('#product-compare-with').html(res);
				   $('#product-compare-with #carousel2').carouFredSel({
						responsive: true,
						circular: false,
						auto: false,
						items: {
							visible: 1,
							width: 200,
							height: '56%'
						},
						prev: '.prev',
						next: '.next',
						scroll: {
							fx: 'fade'
						}
					});
				
					$('#product-compare-with #thumbs').carouFredSel({
						responsive: true,
						circular: false,
						infinite: false,
						auto: false,
						prev: '#product-compare-with #prev',
						next: '#product-compare-with #next',
						items: {
							visible: {
								min: 2,
								max: 6
							}
				
						}
					});
				
					$('#product-compare-with #thumbs a').click(function() {
						$('#carousel2').trigger('slideTo', '#' + this.href.split('#').pop() );
						$('#thumbs a').removeClass('selected');
						$(this).addClass('selected');
						return false;
					});
				  }
				 });
				 
				});
			   },
			
			msg: function( msg, type ){
				if( $('#pop' ).length === 0 ) {
					$('body').append('<div style="display: none;" id="pop"><div class="pop"><div class="alert"><p></p></div></div></div>');
				}
				var alert_type = 'alert-' + type;
				
				$('#pop > .pop p').html( msg );
				$('#pop > .pop > .alert').addClass(alert_type);
				
				$('#pop').slideDown('slow').delay(5000).fadeOut('slow', function(){
					$('#pop .pop .alert').removeClass(alert_type);
				});
				
				
			},
			
		};
	
	$.fn.tweets = function( options ){
		
		var settings = {
				screen_name	:	'wordpress',
				count		:	3,
				template	:	'blockquote'
			};
			
			options = $.extend( settings, options );
			
			onlineshop.tweets( options, this );
			
			
	};
	
	onlineshop.form_click();
	
	$(document).ready(function() {
		
		"use strict";
        
		$('.add_to_wishlist, a[rel="product_del_wishlist"]').click(function(e) {
			e.preventDefault();
			
			if( $(this).attr('rel') === 'product_del_wishlist' ){
				if( confirm( 'Are you sure! you want to delete it' ) ){
					var opt = {subaction:'wishlist_del', data_id:$(this).attr('data-id')};
					onlineshop.wishlist( opt, this );
				}
			}else{
				var opt = {subaction:'wishlist', data_id:$(this).attr('data-id')};
				onlineshop.wishlist( opt, this );
			}
			
		});/**wishlist end*/
		
    });/** document.ready end */
		
})(jQuery);






jQuery(document).ready(function($) {

	"use strict";
	
	if( $(".fa-search-btn, prettyPhoto").length ) {
		$(".fa-search-btn").prettyPhoto();
		$(".prettyPhoto").prettyPhoto();
	}
	
	 $(".input-qty-box .input a").click(function () {
		var inputEl = $(this).parent().parent().children().next().children();
		var qty = inputEl.val();
		if ($(this).parent().hasClass("plus"))
			qty++;
		else
			qty--;
		if (qty < 0)
			qty = 0;
		inputEl.val(qty);
      });
	  
	$('.content-li:first').addClass ('active');
	$('.content-li:first').css ('display', 'block');
			
	$('ul.i-tab').delegate('li:not(.active)', 'click', function() {
		$(this).addClass('active').siblings().removeClass('active')
		.parents('.tabs').find('ul.tab-content .content-li').hide()
		.eq($(this).index()).show();
	});
	
	if( $('.bxslider').length ){
		$('.bxslider').bxSlider({ 
			slideWidth: 270,
			minSlides: 4,
			maxSlides: 4,
			slideMargin: 28
		});
	}
	
	if( $('.bxslider2').length )
	{
		$('.bxslider2').bxSlider({ 
			slideWidth: 193,
			minSlides: 7,
			maxSlides: 7
		});
	}
	
	 
	 // Scroll function
	var $self = $('#scroll-top-img');
	var scrollEvent = $(window).scroll(function() {
		
		var scrollDis = $(document).height() - $(window).height() - 300;
		
		
		// Switch animation type
		$( ($(window).scrollTop() > scrollDis) ? $self.fadeIn(300) : $self.fadeOut(300) );
		
		
		
	});

	// To the top
	$self.click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop:0
		}, 500, 'linear');
	});
	
	
	
});



