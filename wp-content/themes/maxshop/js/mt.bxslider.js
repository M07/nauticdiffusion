jQuery(function($) {
	$('.mtbxslider').bxSlider({
	  minSlides: 4,
	  maxSlides: 8,
	  slideWidth: 170,
	  slideMargin: 10,
	  ticker: true,
	  speed: 35000
	});
});