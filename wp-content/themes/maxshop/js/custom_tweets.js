

(function($){
	"use strict";
	var rockband = {			
			count: 0,
			tweets: function( options, selector ){
				
				options.action = '_ajax_callback';
				options.subaction = 'tweets';
				
				$.ajax({
					url: ajaxurl,
					type: 'POST',
					data:options,
					dataType:"json",
					success: function(res){
						console.log(selector);
						alert(res)

						var reply = res;
						
						var html = '';
						if( options.template === 'blockquote' ){
							$.each(reply, function(k, element){
								var date = new Date(element.created_at);
								html += '<li><div class="tweet-detail">';
								html += '<span class="date">'+element.user.name+' ( '+date.toString('dddd, MMMM ,yyyy')+' )</span>';
								html += '<p>'+element.text+'</p>';
								html += '</div></li>';
							});
						}else{
							$.each(reply, function(k, element) {
								html += '<li>'+element.text+'</li>';
							});
						}
						alert(html);
						$(selector).html( html );	
					}
				});
				
			},
		};
	
	$.fn.tweets = function( options ){
		
		var settings = {
				screen_name	:	'wordpress',
				count		:	3,
				template	:	'blockquote'
			};
			
			options = $.extend( settings, options );
			
			
			
			rockband.tweets( options, this );
	};
	
		
})(jQuery);




