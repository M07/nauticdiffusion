jQuery(function($) {
  "use strict";

  var quantityClass = ".product-quantity";

  // Root

  window.Nautic = window.Nautic || {};

  // function: Nautic.updateProduct
  // description: Update the quantity product 
  Nautic.updateProduct = function() {
  	var $ul = $(this).closest("ul");
  	var quantity = Math.max($(quantityClass, $ul).val(), 0);
    Nautic.changeQuantityProduct(quantity, $(this).attr("data-key"));
  };

  // function: Nautic.deleteProduct
  // description: Delete the product 
  Nautic.deleteProduct = function() {
    Nautic.changeQuantityProduct(0, $(this).attr("data-key"));
  };

  Nautic.changeQuantityProduct = function(quantity, key) {
    var ajaxData =  {
                      'quantity': quantity,
                      'key': key,
                      'wpsc_update_quantity': true
                    };
    $.ajax({
      type: "Post",
      url: document.URL,
      data: ajaxData,
      success: function(response){
        window.location.reload();
      },
      error: function(response){
        alert("Error:" + response);
      }
    });
  };
});

jQuery(function($) {
  $(".update-product").click(Nautic.updateProduct);
  $(".delete-product").click(Nautic.deleteProduct);
});
