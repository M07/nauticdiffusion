<?php

return array(
	'title' => __('Aplus Theme Options', SH_NAME),
	'logo' => 'logo.png',
	'menus' => array(
		array(
			'title' => __('General Settings', SH_NAME),
			'name' => 'general_settings',
			'icon' => 'font-awesome:fa-magic',
			'menus' => array(
				array(
					'title' => __('General Settings', SH_NAME),
					'name' => 'general_settings',
					'icon' => 'font-awesome:fa-th-large',
					'controls' => array(
						array(
							'type' => 'section',
							'title' => __('Color Scheme', SH_NAME),
							'name' => 'color_scheme',
							'description' => __('This section is used for theme color settings', SH_NAME),
							'fields' => array(
								array(
									'type' => 'color',
									'name' => 'color_scheme',
									'label' => __('Color Scheme', SH_NAME),
									'description' => __('Choose the default color scheme for the theme.', SH_NAME),
									'default' => '#98ed28',
								),
								array(
									'type' => 'select',
									'name' => 'pre_color_scheme',
									'label' => __('Predefined Color Schemes', SH_NAME),
									'description' => __('Choose the default color scheme from predefined colors.', SH_NAME),
									'items' => array( array('value'=>0, 'label'=>__('No color', SH_NAME) ), array('value'=>'#EC644B', 'label'=>__('Red', SH_NAME) ), ),
									'default' => '#EC644B',
								),
								
							),
						),
						array(
							'type' => 'section',
							'title' => __('Twitter Settings', SH_NAME),
							'name' => 'twitter_settings',
							'description' => __('This section contain the information about twitter api settings', SH_NAME),
							'fields' => array(
								array(
									'type' => 'textbox',
									'name' => 'api',
									'label' => __('API Key', SH_NAME),
									'description' => __('Enter the twitter API key, You can get the api at http://developer.twitter.com', SH_NAME),
									'default' => '',
								),
								array(
									'type' => 'textbox',
									'name' => 'api_secret',
									'label' => __('API Secret', SH_NAME),
									'description' => __('Enter the API secret', SH_NAME),
									'default' => '',
								),
								array(
									'type' => 'textbox',
									'name' => 'token',
									'label' => __('Token', SH_NAME),
									'description' => __('Enter the twitter api token', SH_NAME),
									'default' => '',
								),
								array(
									'type' => 'textbox',
									'name' => 'token_secret',
									'label' => __('Token Secret', SH_NAME),
									'description' => __('Enter the API token secret', SH_NAME),
									'default' => '',
								),
								
							),
						),
						
						array(
							'type' => 'textarea',
							'name' => 'ta_1',
							'label' => __('Textarea', SH_NAME),
							'description' => __('Everytime you need long text..', SH_NAME),
							'default' => 'lorem ipsum',
						),
					),
				
				),
				
				/** Submenu for heading settings */
				array(
					'title' => __('Header Settings', SH_NAME),
					'name' => 'header_settings',
					'icon' => 'font-awesome:fa-th-large',
					'controls' => array(
						array(
							'type' => 'upload',
							'name' => 'favicon',
							'label' => __('Favicon', SH_NAME),
							'description' => __('Upload the favicon, should be 16x16', SH_NAME),
							'default' => '',
						),
						array(
							'type' => 'select',
							'name' => 'logo_text',
							'label' => __('Logo in Text', SH_NAME),
							'description' => __('Enable to show text instead of logo', SH_NAME),
							'items' => array( array('value'=>'text', 'label'=>'Text'), array('value'=>'logo', 'label'=>'Logo'), ),
							'default' => 'logo'
						),
						
						array(
							'type' => 'section',
							'title' => __('Logo with Image', SH_NAME),
							'name' => 'logo_with_image',
							'dependency' => array(
								'field' => 'logo_text',
								'function' => 'vp_dep_is_logo',
							),
							'fields' => array(
								array(
									'type' => 'upload',
									'name' => 'logo_image',
									'label' => __('Logo Image', SH_NAME),
									'description' => __('Inser the logo image', SH_NAME),
									'default' => get_template_directory_uri().'/images/logo.png'
								),
								array(
									'type' => 'slider',
									'name' => 'logo_width',
									'label' => __('Logo Width', SH_NAME),
									'description' => __('choose the logo width', SH_NAME),
									'default' => '144',
									'mix' => 20,
									'max' => 400
								),
								array(
									'type' => 'slider',
									'name' => 'logo_height',
									'label' => __('Logo Height', SH_NAME),
									'description' => __('choose the logo height', SH_NAME),
									'default' => '45',
									'mix' => 20,
									'max' => 400
								),
								
							),
						),
						array(
							'type' => 'section',
							'title' => __('Custom Logo Text', SH_NAME),
							'name' => 'section_custom_logo_text',
							'dependency' => array(
								'field' => 'logo_text',
								'function' => 'vp_dep_is_text',
							),
							'fields' => array(
								array(
									'type' => 'textbox',
									'name' => 'logo_heading',
									'label' => __('Logo Heading', SH_NAME),
									'description' => __('Enter the website heading instead of logo', SH_NAME),
									'default' => 'Aplus'
								),
								array(
									'type' => 'slider',
									'name' => 'logo_font_size',
									'label' => __('Logo Font Size', SH_NAME),
									'description' => __('Choose the logo font size', SH_NAME),
									'default' => 40,
									'min' => 12,
									'max' => 45
								),
								array(
									'type' => 'select',
									'name' => 'logo_font_face',
									'label' => __('Logo Font Face', SH_NAME),
									'description' => __('Select Font', SH_NAME),
									'items' => array(
										'data' => array(
											array(
												'source' => 'function',
												'value' => 'vp_get_gwf_family',
											),
										),
									),
								),
								array(
									'type' => 'radiobutton',
									'name' => 'logo_font_style',
									'label' => __('Logo Font Style', SH_NAME),
									'description' => __('Select Font Style', SH_NAME),
									'items' => array(
										'data' => array(
											array(
												'source' => 'binding',
												'field' => 'logo_font_face',
												'value' => 'vp_get_gwf_style',
											),
										),
									),
									'default' => array(
										'{{first}}',
									),
								),
								array(
									'type' => 'color',
									'name' => 'logo_color',
									'label' => __('Logo Color', SH_NAME),
									'description' => __('Choose the default color for logo.', SH_NAME),
									'default' => '#98ed28',
								),
								array(
									'type' => 'textbox',
									'name' => 'slogan_heading',
									'label' => __('Slogan Heading', SH_NAME),
									'description' => __('Enter the slogan', SH_NAME),
									'default' => 'Multipurpose Wordpress theme'
								),
								array(
									'type' => 'slider',
									'name' => 'slogan_font_size',
									'label' => __('Slogan Font Size', SH_NAME),
									'description' => __('Choose the slogan font size', SH_NAME),
									'default' => 40,
									'min' => 12,
									'max' => 45
								),
								array(
									'type' => 'select',
									'name' => 'slogan_font_face',
									'label' => __('Slogan Font Face', SH_NAME),
									'description' => __('Select Font', SH_NAME),
									'items' => array(
										'data' => array(
											array(
												'source' => 'function',
												'value' => 'vp_get_gwf_family',
											),
										),
									),
								),
								array(
									'type' => 'radiobutton',
									'name' => 'slogan_font_style',
									'label' => __('Slogan Font Style', SH_NAME),
									'description' => __('Select Font Style', SH_NAME),
									'items' => array(
										'data' => array(
											array(
												'source' => 'binding',
												'field' => 'slogan_font_face',
												'value' => 'vp_get_gwf_style',
											),
										),
									),
									'default' => array(
										'{{first}}',
									),
								),
								array(
									'type' => 'color',
									'name' => 'slogan_color',
									'label' => __('Slogan Color', SH_NAME),
									'description' => __('Choose the default color for slogan.', SH_NAME),
									'default' => '#98ed28',
								),
							),
						),
						array(
							'type' => 'codeeditor',
							'name' => 'header_css',
							'label' => __('Header CSS', SH_NAME),
							'description' => __('Write your custom css to include in header.', SH_NAME),
							'theme' => 'github',
							'mode' => 'css',
						),
						array(
							'type' => 'codeeditor',
							'name' => 'header_js',
							'label' => __('Header JS', SH_NAME),
							'description' => __('Write your custom js to include in header.', SH_NAME),
							'theme' => 'twilight',
							'mode' => 'javascript',
						),
					),
				
				),
				
				/** Submenu for footer area */
				array(
					'title' => __('Footer Settings', SH_NAME),
					'name' => 'footer_settings',
					'icon' => 'font-awesome:fa-th-large',
					'controls' => array(
						array(
							'type' => 'upload',
							'name' => 'footer_logo',
							'label' => __('Footer Logo', SH_NAME),
							'description' => __('Upload logo to use in footer area', SH_NAME),
							'default' => get_template_directory_uri().'/images/logo.png',
						),
						array(
							'type' => 'slider',
							'name' => 'footer_logo_width',
							'label' => __('Logo Width', SH_NAME),
							'description' => __('choose the logo width', SH_NAME),
							'default' => '144',
							'mix' => 20,
							'max' => 400
						),
						array(
							'type' => 'slider',
							'name' => 'footer_logo_height',
							'label' => __('Logo Height', SH_NAME),
							'description' => __('choose the logo height', SH_NAME),
							'default' => '45',
							'mix' => 20,
							'max' => 400
						),
						array(
							'type' => 'textarea',
							'name' => 'footer_copyright',
							'label' => __('Footer Copyright Text', SH_NAME),
							'description' => __('Enter the copyright text to show in footer area', SH_NAME),
						),
						array(
							'type' => 'codeeditor',
							'name' => 'footer_js',
							'label' => __('Footer JS', SH_NAME),
							'description' => __('Write your custom js to include in footer.', SH_NAME),
							'theme' => 'twilight',
							'mode' => 'javascript',
						),
					)
				), //End of submenu
			),
			
			
		),
		/** Social Network Settings */
		array(
				'title' => __('Socializing Settings', SH_NAME),
				'name' => 'socializing_settings',
				'icon' => 'font-awesome:fa-th-large',
				'controls' => array(
					array(
						'name'  => 'use_pb',
						'label' => 'Enable Socializing',
						'type'  => 'toggle',
					),
					array(
						'type'      => 'textbox',
						'name'      => 'facebook',
						'label'     => __('Facebook', SH_NAME),
						'description' => '',
						'default'	=> '',
					),
					array(
						'type'      => 'textbox',
						'name'      => 'twitter',
						'label'     => __('Twitter', SH_NAME),
						'description' => '',
						'default'	=> '',
					),
					array(
						'type'      => 'textbox',
						'name'      => 'linkedin',
						'label'     => __('LinkedIn', SH_NAME),
						'description' => '',
						'default'	=> '',
					),
					array(
						'type'      => 'textbox',
						'name'      => 'googleplus',
						'label'     => __('Google+', SH_NAME),
						'description' => '',
						'default'	=> '',
					),
					array(
						'type'      => 'textbox',
						'name'      => 'github',
						'label'     => __('Gibhub', SH_NAME),
						'description' => '',
						'default'	=> '',
					),
					array(
						'type'      => 'textbox',
						'name'      => 'xing',
						'label'     => __('Xing', SH_NAME),
						'description' => '',
						'default'	=> '',
					),
					array(
						'type'      => 'textbox',
						'name'      => 'pinterest',
						'label'     => __('Pinterest', SH_NAME),
						'description' => '',
						'default'	=> '',
					),
				)
			),
			
		/** Portfolio Settings */
		array(
				'title' => __('Portfolio Settings', SH_NAME),
				'name' => 'portfolio_settings',
				'icon' => 'font-awesome:fa-th-large',
				'controls' => array(
					
					array(
							'type' => 'select',
							'name' => 'columns',
							'label' => __('Columns', SH_NAME),
							'description' => __('Choose Number of columns for Portfolio Page', SH_NAME),
							'items' => array( array('value'=>'2column', 'label'=>'2 Columns'), array('value'=>'3column', 'label'=>'3 Column'), array('value'=>'4column', 'label'=>'4 Column'), ),
							'default' => '2column'
						)
				)
		),
			
				/** Under Construction Settings */
		array(
				'title' => __('Under Construction Settings', SH_NAME),
				'name' => 'under_construction_settings',
				'icon' => 'font-awesome:fa-th-large',
				'controls' => array(
					array(
						'name'  => 'under_const_status',
						'label' => 'Enable Under Construction Mode',
						'type'  => 'toggle',
						'default'	=> 0,
						
					),
					array(
								'type'  => 'section',
								'title' => __('Under Construction Page Settings', SH_NAME),
								'name'  => 'h1_settings',
								'description' => __('heading 1 font settings', SH_NAME),
								'dependency' => array(
									'field' => 'under_const_status',
									'function' => 'vp_dep_boolean',
								),
								'fields' => array(
												array(
														'type' => 'date',
														'name' => 'launch_date',
														'label' => __('Date', SH_NAME),
														'description' => __('Choose the Launching Date.', SH_NAME),
														'format' => 'yy-mm-dd',
														'default' => '2013-12-12',
													),
													array(
														'type' => 'textbox',
														'name' => 'under_const_welcome_text',
														'label' => __('Welcome Text', SH_NAME),
														'description' => __('Enter the Welcome Text to show on under construction page.', SH_NAME),
														'default' => 'We are currently working on an awesome new site, won"t be long!',
													)
												)
						)
				)
			),
		
		/** Font settings */	
		array(
				'title' => __('Font Settings', SH_NAME),
				'name' => 'font_settings',
				'icon' => 'font-awesome:fa-th-large',
				'menus' => array(
					
					/** heading font settings */
					array(
						'title' => __('Heading Font', SH_NAME),
						'name' => 'heading_font_settings',
						'icon' => 'font-awesome:fa-th-large',
						
						'controls' => array(
							
							array(
								'type' => 'toggle',
								'name' => 'use_custom_font',
								'label' => __('Use Custom Font', SH_NAME),
								'description' => __('Use custom font or not', SH_NAME),
							),
							array(
								'type'  => 'section',
								'title' => __('H1 Settings', SH_NAME),
								'name'  => 'h1_settings',
								'description' => __('heading 1 font settings', SH_NAME),
								'dependency' => array(
									'field' => 'use_custom_font',
									'function' => 'vp_dep_boolean',
								),
								'fields' => array(
									array(
										'type' => 'slider',
										'label' => __('Font Size', SH_NAME),
										'name' => 'h1_font_size',
										'description' => __('Choose the font size for h1', SH_NAME),
										'default'=>'38.5',
										'min' => 12,
										'max' => 50
									),
									array(
										'type' => 'select',
										'label' => __('Font Family', SH_NAME),
										'name' => 'h1_font_family',
										'description' => __('Select the font family to use for h1', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'function',
													'value' => 'vp_get_gwf_family',
												),
											),
										),
										
									),
									array(
										'type' => 'radiobutton',
										'name' => 'h1_font_style',
										'label' => __('Font Style', SH_NAME),
										'description' => __('Select Font Style', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'h1_font_family',
													'value' => 'vp_get_gwf_style',
												),
											),
										),
										'default' => array(
											'{{first}}',
										),
									),
									array(
										'type' => 'radiobutton',
										'name' => 'h1_font_weight',
										'label' => __('Font Weight', SH_NAME),
										'description' => __('Select Font Weight', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'h1_font_family',
													'value' => 'vp_get_gwf_weight',
												),
											),
										),
									),
									array(
										'type' => 'color',
										'name' => 'h1_font_color',
										'label' => __('Font Color', SH_NAME),
										'description' => __('Choose the font color for heading h1', SH_NAME),
										'default' => '#98ed28',
									),
								),
							),
							array(
								'type'  => 'section',
								'title' => __('H2 Settings', SH_NAME),
								'name'  => 'h2_settings',
								'description' => __('heading h2 font settings', SH_NAME),
								'dependency' => array(
									'field' => 'use_custom_font',
									'function' => 'vp_dep_boolean',
								),
								'fields' => array(
									array(
										'type' => 'slider',
										'label' => __('Font Size', SH_NAME),
										'name' => 'h2_font_size',
										'description' => __('Choose the font size for h2', SH_NAME),
										'default'=>'34.5',
										'min' => 12,
										'max' => 50
									),
									array(
										'type' => 'select',
										'label' => __('Font Family', SH_NAME),
										'name' => 'h2_font_family',
										'description' => __('Select the font family to use for h2', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'function',
													'value' => 'vp_get_gwf_family',
												),
											),
										),
										
									),
									array(
										'type' => 'radiobutton',
										'name' => 'h2_font_style',
										'label' => __('Font Style', SH_NAME),
										'description' => __('Select Font Style', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'h2_font_family',
													'value' => 'vp_get_gwf_style',
												),
											),
										),
										'default' => array(
											'{{first}}',
										),
									),
									array(
										'type' => 'radiobutton',
										'name' => 'h2_font_weight',
										'label' => __('Font Weight', SH_NAME),
										'description' => __('Select Font Weight', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'h2_font_family',
													'value' => 'vp_get_gwf_weight',
												),
											),
										),
									),
									array(
										'type' => 'color',
										'name' => 'h2_font_color',
										'label' => __('Font Color', SH_NAME),
										'description' => __('Choose the font color for heading h1', SH_NAME),
										'default' => '#98ed28',
									),
								),
							),
							array(
								'type'  => 'section',
								'title' => __('H3 Settings', SH_NAME),
								'name'  => 'h3_settings',
								'description' => __('heading h3 font settings', SH_NAME),
								'dependency' => array(
									'field' => 'use_custom_font',
									'function' => 'vp_dep_boolean',
								),
								'fields' => array(
									array(
										'type' => 'slider',
										'label' => __('Font Size', SH_NAME),
										'name' => 'h3_font_size',
										'description' => __('Choose the font size for h3', SH_NAME),
										'default'=>'30.5',
										'min' => 12,
										'max' => 50
									),
									array(
										'type' => 'select',
										'label' => __('Font Family', SH_NAME),
										'name' => 'h3_font_family',
										'description' => __('Select the font family to use for h3', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'function',
													'value' => 'vp_get_gwf_family',
												),
											),
										),
										
									),
									array(
										'type' => 'radiobutton',
										'name' => 'h3_font_style',
										'label' => __('Font Style', SH_NAME),
										'description' => __('Select Font Style', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'h3_font_family',
													'value' => 'vp_get_gwf_style',
												),
											),
										),
										'default' => array(
											'{{first}}',
										),
									),
									array(
										'type' => 'radiobutton',
										'name' => 'h3_font_weight',
										'label' => __('Font Weight', SH_NAME),
										'description' => __('Select Font Weight', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'h3_font_family',
													'value' => 'vp_get_gwf_weight',
												),
											),
										),
									),
									array(
										'type' => 'color',
										'name' => 'h3_font_color',
										'label' => __('Font Color', SH_NAME),
										'description' => __('Choose the font color for heading h3', SH_NAME),
										'default' => '#98ed28',
									),
								),
							),
						)
					),
					
					/** body font settings */
					array(
						'title' => __('Body Font', SH_NAME),
						'name' => 'body_font_settings',
						'icon' => 'font-awesome:fa-th-large',
						'controls' => array(
							array(
								'type' => 'toggle',
								'name' => 'body_custom_font',
								'label' => __('Use Custom Font', SH_NAME),
								'description' => __('Use custom font or not', SH_NAME),
							),
							array(
								'type'  => 'section',
								'title' => __('Body Font Settings', SH_NAME),
								'name'  => 'body_font_settings',
								'description' => __('body font settings', SH_NAME),
								'dependency' => array(
									'field' => 'body_custom_font',
									'function' => 'vp_dep_boolean',
								),
								'fields' => array(
									array(
										'type' => 'slider',
										'label' => __('Font Size', SH_NAME),
										'name' => 'body_font_size',
										'description' => __('Choose the font size for body area', SH_NAME),
										'default'=>'14',
										'min' => 12,
										'max' => 50
									),
									array(
										'type' => 'select',
										'label' => __('Font Family', SH_NAME),
										'name' => 'body_font_family',
										'description' => __('Select the font family to use for body', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'function',
													'value' => 'vp_get_gwf_family',
												),
											),
										),
										
									),
									array(
										'type' => 'radiobutton',
										'name' => 'body_font_style',
										'label' => __('Font Style', SH_NAME),
										'description' => __('Select Font Style', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'body_font_family',
													'value' => 'vp_get_gwf_style',
												),
											),
										),
										'default' => array(
											'{{first}}',
										),
									),
									array(
										'type' => 'radiobutton',
										'name' => 'body_font_weight',
										'label' => __('Font Weight', SH_NAME),
										'description' => __('Select Font Weight', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'body_font_family',
													'value' => 'vp_get_gwf_weight',
												),
											),
										),
									),
									array(
										'type' => 'color',
										'name' => 'body_font_color',
										'label' => __('Font Color', SH_NAME),
										'description' => __('Choose the font color for heading body', SH_NAME),
										'default' => '#98ed28',
									),
								),
							),
						)
					)
					
					
					
					
					
				)
		),
		
		/** Blog Listing Settings */
		array(
				'title' => __('Blog Settings', SH_NAME),
				'name' => 'blog_page_settings',
				'icon' => 'font-awesome:fa-th-large',
				'controls' => array(
					
					array(
						'type' => 'radioimage',
						'name' => 'blog_sidebar_layout',
						'label' => __('Blog Listing Layout', SH_NAME),
						'description' => __('Choose the layout for blog pages', SH_NAME),
						'items' => array(
							array(
								'value' => 'left',
								'label' => __('Blog with Left Sidebar', SH_NAME),
								'img' => get_template_directory_uri().'/images/2cl.png',
							),
							array(
								'value' => 'right',
								'label' => __('Blog with Right Sidebar', SH_NAME),
								'img' => get_template_directory_uri().'/images/2cr.png',
							),
							array(
								'value' => 'full',
								'label' => __('Full Width', SH_NAME),
								'img' => get_template_directory_uri().'/images/1col.png',
							),
							
						),
					),
					array(
						'type' => 'select',
						'name' => 'blog_pages_sidebar',
						'label' => __('Sidebar', SH_NAME),
						'default' => '',
						'items' => sh_get_sidebars(true),
						'dependency' => array(
							'field' => 'use_custom_font',
							'function' => 'vp_dep_sidebar_boolean',
						),
					),
				)
		),
	)
);



/**
 *EOF
 */