<?php

return array(
	'title' => __('Maxshop Theme Options', SH_NAME),
	'logo' => get_template_directory_uri().'/images/logo.png',
	'menus' => array(
		array(
			'title' => __('General Settings', SH_NAME),
			'name' => 'general_settings',
			'icon' => 'font-awesome:fa-cogs fa',
			'menus' => array(
				array(
					'title' => __('General Settings', SH_NAME),
					'name' => 'general_settings',
					'icon' => 'font-awesome:fa-cogs fa',
					'controls' => array(
						
						array(
							'type' => 'section',
							'title' => __('Color Scheme', SH_NAME),
							'name' => 'color_schemes',
							'description' => __('This section contain the information about twitter api settings', SH_NAME),
							'fields' => array(
								array(
									'type' => 'color',
									'name' => 'color_scheme',
									'label' => __('Color Scheme', SH_NAME),
									'description' => __('choose the color scheme', SH_NAME),
									'default' => '#f71919',
									
									
								),
							),
						),
						
						array(
							'type' => 'section',
							'title' => __('Twitter Settings', SH_NAME),
							'name' => 'twitter_settings',
							'description' => __('This section contain the information about twitter api settings', SH_NAME),
							'fields' => array(
								array(
									'type' => 'textbox',
									'name' => 'api',
									'label' => __('API Key', SH_NAME),
									'description' => __('Enter the twitter API key, You can get the api at http://developer.twitter.com', SH_NAME),
									'default' => '',
								),
								array(
									'type' => 'textbox',
									'name' => 'api_secret',
									'label' => __('API Secret', SH_NAME),
									'description' => __('Enter the API secret', SH_NAME),
									'default' => '',
								),
								array(
									'type' => 'textbox',
									'name' => 'token',
									'label' => __('Token', SH_NAME),
									'description' => __('Enter the twitter api token', SH_NAME),
									'default' => '',
								),
								array(
									'type' => 'textbox',
									'name' => 'token_secret',
									'label' => __('Token Secret', SH_NAME),
									'description' => __('Enter the API token secret', SH_NAME),
									'default' => '',
								),
								
							),
						),
						
					),
				
				),
				
				/** Submenu for heading settings */
				array(
					'title' => __('Header Settings', SH_NAME),
					'name' => 'header_settings',
					'icon' => 'font-awesome:fa-strikethrough fa',
					'controls' => array(
						array(
							'type' => 'upload',
							'name' => 'favicon',
							'label' => __('Favicon', SH_NAME),
							'description' => __('Upload the favicon, should be 16x16', SH_NAME),
							'default' => '',
						),
						array(
							'type' => 'upload',
							'name' => 'logo_image',
							'label' => __('Logo Image', SH_NAME),
							'description' => __('Inser the logo image', SH_NAME),
							'default' => get_template_directory_uri().'/images/logo.png'
						),
						array(
							'type' => 'slider',
							'name' => 'logo_width',
							'label' => __('Logo Width', SH_NAME),
							'description' => __('choose the logo width', SH_NAME),
							'default' => '144',
							'mix' => 20,
							'max' => 400
						),
						array(
							'type' => 'slider',
							'name' => 'logo_height',
							'label' => __('Logo Height', SH_NAME),
							'description' => __('choose the logo height', SH_NAME),
							'default' => '45',
							'mix' => 20,
							'max' => 400
						),
						array(
							'type' => 'codeeditor',
							'name' => 'header_css',
							'label' => __('Header CSS', SH_NAME),
							'description' => __('Write your custom css to include in header.', SH_NAME),
							'theme' => 'github',
							'mode' => 'css',
						),
						array(
							'type' => 'codeeditor',
							'name' => 'header_js',
							'label' => __('Header JS', SH_NAME),
							'description' => __('Write your custom js to include in header.', SH_NAME),
							'theme' => 'twilight',
							'mode' => 'javascript',
						),
					),
				
				),
				
				/** Submenu for footer area */
				array(
					'title' => __('Footer Settings', SH_NAME),
					'name' => 'footer_settings',
					'icon' => 'font-awesome:fa-gear fa',
					'controls' => array(
						array(
							'type' => 'color',
							'name' => 'footer_background_color',
							'label' => __('Footer Background Color', SH_NAME),
							'description' => __('Choose the footer background color.', SH_NAME),
							'default' => '#555555',
						),
						array(
								'type' => 'toggle',
								'name' => 'footer_banner_strip',
								'label' => __('Footer Banner Strip', SH_NAME),
								'description' => __('Show footer banner strip or not', SH_NAME),
							),
						array(
							'type' => 'upload',
							'name' => 'footer_banner_image',
							'label' => __('Footer Banner Strip Image', SH_NAME),
							'description' => __('Upload footer banner strip image', SH_NAME),
							'default' => '',
						),
						array(
							'type' => 'textarea',
							'name' => 'bannerstrip_text',
							'label' => __('Banner Strip Text', SH_NAME),
							'description' => __('Enter the footer banner strip text', SH_NAME),
						),
						array(
							'type' => 'textbox',
							'name' => 'banner_label',
							'label' => __('Button Label', SH_NAME),
							'description' => __('Enter the footer banner button label', SH_NAME),
						),
						array(
							'type' => 'textbox',
							'name' => 'banner_url',
							'label' => __('Banner URL', SH_NAME),
							'description' => __('Enter the Footer Banner URL', SH_NAME),
							'default' => '',
						),
						array(
							'type' => 'textarea',
							'name' => 'copyright_text',
							'label' => __('Footer Copyright Text', SH_NAME),
							'description' => __('Enter the copyright text to show in footer area', SH_NAME),
						),
						array(
							'type' => 'codeeditor',
							'name' => 'footer_js',
							'label' => __('Footer JS', SH_NAME),
							'description' => __('Write your custom js to include in footer.', SH_NAME),
							'theme' => 'twilight',
							'mode' => 'javascript',
						),
					)
				), //End of submenu
			),
			
			
		),
			
		/** Social Network Settings */
		array(
				'title' => __('Socializing Settings', SH_NAME),
				'name' => 'socializing_settings',
				'icon' => 'font-awesome:fa-share-square fa',
				'controls' => array(
					array(
						'name'  => 'use_pb',
						'label' => 'Enable Footer Socializing',
						'type'  => 'toggle',
					),
					array(
						'type'      => 'textbox',
						'name'      => 'fb',
						'label'     => __('Facebook', SH_NAME),
						'description' => '',
						'default'	=> '',
					),
					array(
						'type'      => 'textbox',
						'name'      => 'twet',
						'label'     => __('Twitter', SH_NAME),
						'description' => '',
						'default'	=> '',
					),
					array(
						'type'      => 'textbox',
						'name'      => 'rss',
						'label'     => __('RSS', SH_NAME),
						'description' => '',
						'default'	=> '',
					),
					array(
						'type'      => 'textbox',
						'name'      => 'pin',
						'label'     => __('Pinterest', SH_NAME),
						'description' => '',
						'default'	=> '',
					),
					array(
						'type'      => 'textbox',
						'name'      => 'google',
						'label'     => __('Google+', SH_NAME),
						'description' => '',
						'default'	=> '',
					),
					
				)
			),
			
		/** Social Network Settings */
		array(
				'title' => __('404 Page Settings', SH_NAME),
				'name' => '404_page_settings',
				'icon' => 'font-awesome:fa-share-square fa',
				'controls' => array(
					
					array(
						'type'      => 'upload',
						'name'      => '404_header_image',
						'label'     => __('Header Image', SH_NAME),
						'description' => __('Uploaded the header banner image', SH_NAME),
						'default'	=> get_template_directory_uri().'/images/banner.png',
					),
					array(
						'type'      => 'textarea',
						'name'      => '404_page_error',
						'label'     => __('Error Text', SH_NAME),
						'description' => '',
						'default'	=> __( 'It looks like nothing was found at this location. Maybe try a search?', SH_NAME ),
					),
					
				)
			),
			
		/** Font settings */	
		array(
				'title' => __('Font Settings', SH_NAME),
				'name' => 'font_settings',
				'icon' => 'font-awesome:fa-font fa',
				'menus' => array(
					
					/** heading font settings */
					array(
						'title' => __('Heading Font', SH_NAME),
						'name' => 'heading_font_settings',
						'icon' => 'font-awesome:fa-text-height fa',
						
						'controls' => array(
							
							array(
								'type' => 'toggle',
								'name' => 'use_custom_font',
								'label' => __('Use Custom Font', SH_NAME),
								'description' => __('Use custom font or not', SH_NAME),
							),
							array(
								'type'  => 'section',
								'title' => __('H1 Settings', SH_NAME),
								'name'  => 'h1_settings',
								'description' => __('heading 1 font settings', SH_NAME),
								'dependency' => array(
									'field' => 'use_custom_font',
									'function' => 'vp_dep_boolean',
								),
								'fields' => array(
									array(
										'type' => 'slider',
										'label' => __('Font Size', SH_NAME),
										'name' => 'h1_font_size',
										'description' => __('Choose the font size for h1', SH_NAME),
										'default'=>'38.5',
										'min' => 12,
										'max' => 50
									),
									array(
										'type' => 'select',
										'label' => __('Font Family', SH_NAME),
										'name' => 'h1_font_family',
										'description' => __('Select the font family to use for h1', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'function',
													'value' => 'vp_get_gwf_family',
												),
											),
										),
										
									),
									array(
										'type' => 'radiobutton',
										'name' => 'h1_font_style',
										'label' => __('Font Style', SH_NAME),
										'description' => __('Select Font Style', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'h1_font_family',
													'value' => 'vp_get_gwf_style',
												),
											),
										),
										'default' => array(
											'{{first}}',
										),
									),
									array(
										'type' => 'radiobutton',
										'name' => 'h1_font_weight',
										'label' => __('Font Weight', SH_NAME),
										'description' => __('Select Font Weight', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'h1_font_family',
													'value' => 'vp_get_gwf_weight',
												),
											),
										),
									),
									array(
										'type' => 'color',
										'name' => 'h1_font_color',
										'label' => __('Font Color', SH_NAME),
										'description' => __('Choose the font color for heading h1', SH_NAME),
										'default' => '#98ed28',
									),
								),
							),
							array(
								'type'  => 'section',
								'title' => __('H2 Settings', SH_NAME),
								'name'  => 'h2_settings',
								'description' => __('heading h2 font settings', SH_NAME),
								'dependency' => array(
									'field' => 'use_custom_font',
									'function' => 'vp_dep_boolean',
								),
								'fields' => array(
									array(
										'type' => 'slider',
										'label' => __('Font Size', SH_NAME),
										'name' => 'h2_font_size',
										'description' => __('Choose the font size for h2', SH_NAME),
										'default'=>'34.5',
										'min' => 12,
										'max' => 50
									),
									array(
										'type' => 'select',
										'label' => __('Font Family', SH_NAME),
										'name' => 'h2_font_family',
										'description' => __('Select the font family to use for h2', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'function',
													'value' => 'vp_get_gwf_family',
												),
											),
										),
										
									),
									array(
										'type' => 'radiobutton',
										'name' => 'h2_font_style',
										'label' => __('Font Style', SH_NAME),
										'description' => __('Select Font Style', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'h2_font_family',
													'value' => 'vp_get_gwf_style',
												),
											),
										),
										'default' => array(
											'{{first}}',
										),
									),
									array(
										'type' => 'radiobutton',
										'name' => 'h2_font_weight',
										'label' => __('Font Weight', SH_NAME),
										'description' => __('Select Font Weight', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'h2_font_family',
													'value' => 'vp_get_gwf_weight',
												),
											),
										),
									),
									array(
										'type' => 'color',
										'name' => 'h2_font_color',
										'label' => __('Font Color', SH_NAME),
										'description' => __('Choose the font color for heading h1', SH_NAME),
										'default' => '#98ed28',
									),
								),
							),
							array(
								'type'  => 'section',
								'title' => __('H3 Settings', SH_NAME),
								'name'  => 'h3_settings',
								'description' => __('heading h3 font settings', SH_NAME),
								'dependency' => array(
									'field' => 'use_custom_font',
									'function' => 'vp_dep_boolean',
								),
								'fields' => array(
									array(
										'type' => 'slider',
										'label' => __('Font Size', SH_NAME),
										'name' => 'h3_font_size',
										'description' => __('Choose the font size for h3', SH_NAME),
										'default'=>'30.5',
										'min' => 12,
										'max' => 50
									),
									array(
										'type' => 'select',
										'label' => __('Font Family', SH_NAME),
										'name' => 'h3_font_family',
										'description' => __('Select the font family to use for h3', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'function',
													'value' => 'vp_get_gwf_family',
												),
											),
										),
										
									),
									array(
										'type' => 'radiobutton',
										'name' => 'h3_font_style',
										'label' => __('Font Style', SH_NAME),
										'description' => __('Select Font Style', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'h3_font_family',
													'value' => 'vp_get_gwf_style',
												),
											),
										),
										'default' => array(
											'{{first}}',
										),
									),
									array(
										'type' => 'radiobutton',
										'name' => 'h3_font_weight',
										'label' => __('Font Weight', SH_NAME),
										'description' => __('Select Font Weight', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'h3_font_family',
													'value' => 'vp_get_gwf_weight',
												),
											),
										),
									),
									array(
										'type' => 'color',
										'name' => 'h3_font_color',
										'label' => __('Font Color', SH_NAME),
										'description' => __('Choose the font color for heading h3', SH_NAME),
										'default' => '#98ed28',
									),
								),
							),
						)
					),
					
					/** body font settings */
					array(
						'title' => __('Body Font', SH_NAME),
						'name' => 'body_font_settings',
						'icon' => 'font-awesome:fa-text-width fa',
						'controls' => array(
							array(
								'type' => 'toggle',
								'name' => 'body_custom_font',
								'label' => __('Use Custom Font', SH_NAME),
								'description' => __('Use custom font or not', SH_NAME),
							),
							array(
								'type'  => 'section',
								'title' => __('Body Font Settings', SH_NAME),
								'name'  => 'body_font_settings',
								'description' => __('body font settings', SH_NAME),
								'dependency' => array(
									'field' => 'body_custom_font',
									'function' => 'vp_dep_boolean',
								),
								'fields' => array(
									array(
										'type' => 'slider',
										'label' => __('Font Size', SH_NAME),
										'name' => 'body_font_size',
										'description' => __('Choose the font size for body area', SH_NAME),
										'default'=>'14',
										'min' => 12,
										'max' => 50
									),
									array(
										'type' => 'select',
										'label' => __('Font Family', SH_NAME),
										'name' => 'body_font_family',
										'description' => __('Select the font family to use for body', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'function',
													'value' => 'vp_get_gwf_family',
												),
											),
										),
										
									),
									array(
										'type' => 'radiobutton',
										'name' => 'body_font_style',
										'label' => __('Font Style', SH_NAME),
										'description' => __('Select Font Style', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'body_font_family',
													'value' => 'vp_get_gwf_style',
												),
											),
										),
										'default' => array(
											'{{first}}',
										),
									),
									array(
										'type' => 'radiobutton',
										'name' => 'body_font_weight',
										'label' => __('Font Weight', SH_NAME),
										'description' => __('Select Font Weight', SH_NAME),
										'items' => array(
											'data' => array(
												array(
													'source' => 'binding',
													'field' => 'body_font_family',
													'value' => 'vp_get_gwf_weight',
												),
											),
										),
									),
									array(
										'type' => 'color',
										'name' => 'body_font_color',
										'label' => __('Font Color', SH_NAME),
										'description' => __('Choose the font color for heading body', SH_NAME),
										'default' => '#98ed28',
									),
								),
							),
						)
					)
					
				)
		),
		
		/** Blog Listing Settings */
		array(
				'title' => __('Blog Settings', SH_NAME),
				'name' => 'blog_page_settings',
				'icon' => 'font-awesome:fa-rss fa',
				'controls' => array(
					
					array(
						'type' => 'upload',
						'name' => 'home_page_header_image',
						'label' => __('Homepage Header Image', SH_NAME),
						'description' => __('Choose the header image', SH_NAME),
						
					),
					array(
						'type' => 'upload',
						'name' => 'category_page_header_image',
						'label' => __('Category Header Image', SH_NAME),
						'description' => __('Choose the header image', SH_NAME),
						
					),
					array(
						'type' => 'upload',
						'name' => 'author_page_header_image',
						'label' => __('Author Page Header Image', SH_NAME),
						'description' => __('Choose the header image', SH_NAME),
					),
					array(
						'type' => 'upload',
						'name' => 'archive_page_header_image',
						'label' => __('Archive Page Header Image', SH_NAME),
						'description' => __('Choose the header image', SH_NAME),
					),
					array(
						'type' => 'upload',
						'name' => 'search_page_header_image',
						'label' => __('Search Page Header Image', SH_NAME),
						'description' => __('Choose the header image', SH_NAME),
					),
					array(
						'type' => 'upload',
						'name' => 'tag_page_header_image',
						'label' => __('Tag Page Header Image', SH_NAME),
						'description' => __('Choose the header image', SH_NAME),
					),
				)
		),
	)
);



/**
 *EOF
 */