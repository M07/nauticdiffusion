<?php
$options = array();
$options[] =  array(
	'id'          => 'sh_post_meta',
	'types'       => array('post' , 'page', 'wpsc-product'),
	'title'       => __('Post Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 
array(
		array(
			'type'      => 'group',
			'repeating' => false,
			'length'    => 1,
			'name'      => 'sh_post_options',
			'title'     => __('General Settings', SH_NAME),
			'fields'    => 
			array(
			
				
				array(
					'type' => 'upload',
					'name' => 'page_banner',
					'label' => __('Page Banner', SH_NAME),
					'default' => get_template_directory_uri().'/media/page-title-bg.png',
				),
				array(
					'type' => 'select',
					'name' => 'sidebar',
					'label' => __('Sidebar', SH_NAME),
					'default' => '',
					'items' => sh_get_sidebars(true)	
				),
				array(
					'type' => 'radioimage',
					'name' => 'layout',
					'label' => __('Layout', SH_NAME),
					'description' => __('Choose the layout pages', SH_NAME),
					'items' => array(
						array(
							'value' => 'left',
							'label' => __('Left Sidebar', SH_NAME),
							'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cl.png',
						),
						array(
							'value' => 'right',
							'label' => __('Right Sidebar', SH_NAME),
							'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cr.png',
						),
						array(
							'value' => 'full',
							'label' => __('Full Width', SH_NAME),
							'img' => get_template_directory_uri().'/includes/vafpress/public/img/1col.png',
						),
						
					),
				),
				
			),
		),
	),
);

/** Brands Options*/
$options[] =  array(
	'id'          => 'sh_wpsc-product_settings',
	'types'       => array('wpsc-productdddd'),
	'title'       => __('Online Store Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
				array(
					'type' => 'textarea',
					'name' => 'ingredients',
					'label' => __('Ingre', SH_NAME),
					'default' => '',
					)
	),
);


/** Brands Options*/
$options[] =  array(
	'id'          => 'sh_brand_settings',
	'types'       => array('sh_brand'),
	'title'       => __('Brands Options', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
				array(
					'type' => 'textbox',
					'name' => 'link',
					'label' => __('Link', SH_NAME),
					'default' => '',
					)
	),
);

/** Brands Options*/
$options[] =  array(
	'id'          => 'sh_slide_settings',
	'types'       => array('sh_slides'),
	'title'       => __('Slide Options', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
				array(
					'type' => 'textbox',
					'name' => 'subtitle',
					'label' => __('Sub Title', SH_NAME),
					'default' => '',
					),
				array(
					'type' => 'textbox',
					'name' => 'discount',
					'label' => __('Discount Percentage', SH_NAME),
					'default' => '',
					),
				array(
					'type' => 'textbox',
					'name' => 'link',
					'label' => __('Link', SH_NAME),
					'default' => '',
					),
				
	),
);

/** Testimonial Options*/
$options[] =  array(
	'id'          => 'sh_testimonial_options',
	'types'       => array('aplus_testimonial'),
	'title'       => __('Testimonials Options', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
				array(
					'type' => 'textbox',
					'name' => 'designation',
					'label' => __('Designation', SH_NAME),
					'default' => '',
					)
	),
);



/** Partner Options Options*/
$options[] =  array(
	'id'          => 'sh_partner_meta',
	'types'       => array('aplus_partner'),
	'title'       => __('Partner Options', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
						
			array(
				'type' => 'textbox',
				'name' => 'link',
				'label' => __('Partner URL', SH_NAME),
				'default' => '',
			),
	),
);

$options[] =  array(
	'id'          => 'sh_services_option',
	'types'       => array( 'aplus_service' ),
	'title'       => __('Post Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 

			array(
				
				array(
					'type' => 'fontawesome',
					'name' => 'fontawesome',
					'label' => __('Service Icon', SH_NAME),
					'default' => '',
				),
				
	),
);

/**
 * EOF
 */
 
 
 return $options;
 
 
 
 
 
 
 
 