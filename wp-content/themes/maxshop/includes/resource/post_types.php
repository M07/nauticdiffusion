<?php

$options = array();



$options['sh_brand'] = array(
								'labels' => array(__('Brand', SH_NAME), __('Brands', SH_NAME)),
								'slug' => 'brand',
								'label_args' => array('menu_name' => __('Brands', SH_NAME)),
								'supports' => array( 'title', 'editor', 'thumbnail'),
								'label' => __('Brands', SH_NAME),
								'args'=>array('menu_icon'=>'dashicons-feedback')
							);
$options['sh_slides'] = array(
								'labels' => array(__('Slide', SH_NAME), __('Slides', SH_NAME)),
								'slug' => 'slide',
								'label_args' => array('menu_name' => __('Slides', SH_NAME)),
								'supports' => array( 'title','thumbnail'),
								'label' => __('Slides', SH_NAME),
								'args'=>array('menu_icon'=>'dashicons-feedback')
							);










