<?php
//include(get_template_directory().'/includes/resource/awesom_icons.php');
$settings = array();

$settings['post']['sidebar'] =  array(
													'type' => 'select', //builtin fields include:
													'id' => 'sidebar',
													'title' => __('Sidebar', SH_NAME),
													'desc' => __('Choose an sidebar for this deal', SH_NAME),
													'options' => sh_get_sidebars(),
													'attributes' => array('style' => 'width:40%'),
												);
$settings['page']['sidebar'] =  array(
													'type' => 'select', //builtin fields include:
													'id' => 'sidebar',
													'title' => __('Sidebar', SH_NAME),
													'desc' => __('Choose an sidebar for this deal', SH_NAME),
													'options' => sh_get_sidebars(),
													'attributes' => array('style' => 'width:40%'),
												);

$settings['bistro_service']['font_awesome'] =  array(
													'type' => 'select', //builtin fields include:
													'id' => 'font_awesome',
													'title' => __('Choos Font Awesome Icon', SH_NAME),
													'desc' => __('Choose an icon fron the font icons list', SH_NAME),
													'options' => array_values(array_map(create_function('$v', 'return ucwords($v);'), $GLOBALS['_font_awesome'])),
													'attributes' => array('style' => 'width:40%'),
												);
												
$settings['bistro_service']['sidebar'] =  array(
													'type' => 'select', //builtin fields include:
													'id' => 'sidebar',
													'title' => __('Sidebar', SH_NAME),
													'desc' => __('Choose an sidebar for this service', SH_NAME),
													'options' => sh_get_sidebars(),
													'attributes' => array('style' => 'width:40%'),
												);

$settings['bistro_deal']['start_date'] =  array(
													'type' => 'date', //builtin fields include:
													'id' => 'start_date',
													'title' => __('Start Date', SH_NAME),
													'desc' => __('Choose start date of the deal', SH_NAME),
													'attributes' => array('style' => 'width:30%'),
												);
$settings['bistro_deal']['end_date'] =  array(
													'type' => 'date', //builtin fields include:
													'id' => 'end_date',
													'title' => __('End Date', SH_NAME),
													'desc' => __('Choose end date of the deal', SH_NAME),
													'attributes' => array('style' => 'width:30%'),
												);
$settings['slides']['slide_url'] =  array(
													'type' => 'date', //builtin fields include:
													'id' => 'url',
													'title' => __('Slide Link', SH_NAME),
													'desc' => __('Enter slide link', SH_NAME),
													'attributes' => array('style' => 'width:100%'),
												);





















												