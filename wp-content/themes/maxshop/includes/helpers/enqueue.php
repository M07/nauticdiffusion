<?php

class SH_Enqueue
{
	var $opt;
	
	function __construct()
	{
		$this->opt = _WSH()->option();
		add_action( 'wp_enqueue_scripts', array( $this, 'sh_enqueue_scripts' ) );
		
		add_action( 'wp_head', array( $this, 'wp_head' ) );
		
		add_action( 'wp_footer', array( $this, 'wp_footer' ) );
	}

	function sh_enqueue_scripts()
	{
		$styles = array('family-open-sans'=>'http://fonts.googleapis.com/css?family=Open+Sans:300,700,600,800',
						'family-Oswald'=>'http://fonts.googleapis.com/css?family=Oswald:400,700',
						'family-Quattrocento'=>'http://fonts.googleapis.com/css?family=Quattrocento:400,700',
						'prettyPhoto'=>'css/prettyPhoto.css', 
						'tooltipster'=>'css/tooltipster.css',
						'ie'=>'css/ie.css', 
						'bootstrap'=>'css/bootstrap.css',
						'main-style' => 'style.css', 
						'responsive'=>'css/responsive.css',
						'main_color'	=> 'css/color.css');
								
		foreach( $styles as $name => $style )
		{
			if(strstr($style, 'http')) wp_register_style( $name, $style);
			else wp_register_style( $name, THEME_URL.$style);
			
			wp_enqueue_style($name);
		}
		
		sh_demo_color_picker();
		$scripts = array('jquery-cycle-all'=>'jquery.cycle.all.js', 'modernizr-custom'=>'modernizr.custom.17475.js','jquery-elastislide'=>'jquery.elastislide.js','jquery-carouFredSel'=>'jquery.carouFredSel-6.0.4-packed.js','jquery-selectBox'=>'jquery.selectBox.js','jquery-tooltipster'=>'jquery.tooltipster.min.js', 'jquery-prettyPhoto'=>'jquery.prettyPhoto.js','flickr-feed'=>'jflickrfeed.min.js','custom-tweets'=>'custom_tweets.js','custom-scripts'=>'custom.js','theme-script'=>'theme-script.js','nautic'=>'nautic.js');			
		foreach( $scripts as $name => $js )
		{
			wp_register_script( $name, THEME_URL.'/js/'.$js, '', SH_VERSION, true);
		}
		
		wp_enqueue_script(array('jquery','jquery-cycle-all','jquery-ui-accordion','jquery-ui-tabs', 'modernizr-custom','jquery-elastislide','jquery-carouFredSel', 'jquery-selectBox','jquery-tooltipster','jquery-prettyPhoto','flickr-feed','theme-script','custom-scripts', 'nautic'));
					
		if( is_singular() ) wp_enqueue_script('comment-reply');
		/*if( is_page_template( 'portfolio.php' ) ) wp_enqueue_script( 'portfolio-filter' );*/
		
	}
	
	function wp_head()
	{
		
		$opt = $this->opt;
		echo '<script type="text/javascript"> if( ajaxurl === undefined ) var ajaxurl = "'.admin_url('admin-ajax.php').'";</script>';?>
		<style type="text/css">
			<?php
			if( sh_set( $opt, 'body_custom_font') )
			echo sh_get_font_settings( array( 'body_font_size' => 'font-size', 'body_font_family' => 'font-family', 'body_font_style' => 'font-style', 'body_font_color' => 'color', 'body_line_height' => 'line-height' ), 'body, p {', '}' );
			
			if( sh_set( $opt, 'use_custom_font' ) )
			{
				echo sh_get_font_settings( array( 'h1_font_size' => 'font-size', 'h1_font_family' => 'font-family', 'h1_font_style' => 'font-style', 'h1_font_color' => 'color', 'h1_line_height' => 'line-height' ), 'h1 {', '}' );
				echo sh_get_font_settings( array( 'h2_font_size' => 'font-size', 'h2_font_family' => 'font-family', 'h2_font_style' => 'font-style', 'h2_font_color' => 'color', 'h2_line_height' => 'line-height' ), 'h2 {', '}' );
				echo sh_get_font_settings( array( 'h3_font_size' => 'font-size', 'h3_font_family' => 'font-family', 'h3_font_style' => 'font-style', 'h3_font_color' => 'color', 'h3_line_height' => 'line-height' ), 'h3 {', '}' );
				echo sh_get_font_settings( array( 'h4_font_size' => 'font-size', 'h4_font_family' => 'font-family', 'h4_font_style' => 'font-style', 'h4_font_color' => 'color', 'h4_line_height' => 'line-height' ), 'h4 {', '}' );
				echo sh_get_font_settings( array( 'h5_font_size' => 'font-size', 'h5_font_family' => 'font-family', 'h5_font_style' => 'font-style', 'h5_font_color' => 'color', 'h5_line_height' => 'line-height' ), 'h5 {', '}' );
				echo sh_get_font_settings( array( 'h6_font_size' => 'font-size', 'h6_font_family' => 'font-family', 'h6_font_style' => 'font-style', 'h6_font_color' => 'color', 'h6_line_height' => 'line-height' ), 'h6 {', '}' );
			}
			
			echo sh_set( $opt, 'header_css' );
			?>
		</style>
        
        <?php if( sh_set( $opt, 'header_js' ) ): ?>
			<script type="text/javascript">
                <?php echo sh_set( $opt, 'header_js' );?>
            </script>
        <?php endif;
		
		sh_theme_color_scheme();
	}
	
	function wp_footer()
	{
		$analytics = sh_set( $this->opt, 'footer_analytics');
		
		echo $analytics;
		
		
		if( $footer_js = sh_set( $this->opt, 'footer_js' ) ): ?>
			<script type="text/javascript">
                <?php echo $footer_js;?>
            </script>
        <?php endif;
	}
}