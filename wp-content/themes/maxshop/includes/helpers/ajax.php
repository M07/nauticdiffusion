<?php

class SH_Ajax
{
	
	function __construct()
	{
		add_action( 'wp_ajax__ajax_callback', array( $this, 'ajax_handler' ) );
		add_action( 'wp_ajax_nopriv__ajax_callback', array( $this, 'ajax_handler' ) );
	}
	
	function ajax_handler()
	{
		$method = sh_set( $_REQUEST, 'subaction' );
		if( method_exists( $this, $method ) ) $this->$method();
		exit;
	}
	
	function tweets()
	{
		if( !class_exists('Codebird') ) include( get_template_directory().'/includes/library/codebird.php' ); 
		$cb = new Codebird;
		$method = sh_set( $_POST, 'method' );
		$options = _WSH()->option();
		//printr($options);
		
		$cb->setConsumerKey(sh_set( $options, 'api'), sh_set( $options, 'api_secret'));

		$cb->setToken(sh_set( $options, 'token'), sh_set( $options, 'token_secret'));
		
		$reply = (array) $cb->statuses_userTimeline(array('count'=>sh_set( $_POST, 'count' ), 'screen_name'=>sh_set($_POST, 'screen_name')));
		
		if( isset( $reply['httpstatus'] ) ) unset( $reply['httpstatus'] );
		foreach( $reply as $k => $v ){
			//if( $k == 'httpstatus' ) continue;
			$text = preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" target="_blank">$1</a>', sh_set( $v, 'text'));
			echo '<p><span style="color:#90c322;">@'.$v->user->name.'</span><br>
            '.$text.'<br>
            <span style="color:#2e9aed">'.human_time_diff( date('U', strtotime($v->created_at)), current_time('timestamp') ) . ' ago</span></p>';
			//echo '<li><span class="content">'.$text.'</span></li>';
		}
	}
	
	function wishlist()
	{
		global $current_user;
      	get_currentuserinfo();
			
		if( is_user_logged_in() ){
			
			$meta = (array)get_user_meta( $current_user->ID, '_os_product_wishlist', true );
			$data_id = sh_set( $_POST, 'data_id' );
			if( $meta && is_array( $meta ) ){
				if( in_array( $data_id, $meta ) ){
					exit(json_encode(array('code'=>'exists', 'msg'=>__('You have already added this product to wish list', SH_NAME ) ) ) );
				}
				$newmeta = array_merge( array( sh_set( $_POST, 'data_id' ) ), $meta );
				update_user_meta( $current_user->ID, '_os_product_wishlist', array_unique($newmeta) );
				exit(json_encode(array('code'=>'success', 'msg'=>__('Product successfully added to wishlist', SH_NAME ) ) ) );
			}else{
				//update_user_meta( $current_user->ID, '_os_product_wishlist', array( sh_set( $_POST, 'data_id' ) ) );
				exit(json_encode(array('code'=>'fail', 'msg'=>__('There is an error edding wishlist', SH_NAME ) ) ) );
			}
		}
		else exit(json_encode(array('code'=>'fail', 'msg'=>__('Please login first to add the product to your wishlist', SH_NAME ) ) ) );
	}
	
	function wishlist_del()
	{
		global $current_user;
      	get_currentuserinfo();
			
		if( is_user_logged_in() ){
			
			$meta = get_user_meta( $current_user->ID, '_os_product_wishlist', true );
			$data_id = sh_set( $_POST, 'data_id' );
			//echo array_search( $data_id, $meta );exit;
			if( $meta && is_array( $meta ) ){
				$searched = array_search( $data_id, $meta );
				if( isset($meta[$searched]) ){
					unset( $meta[$searched] );
					update_user_meta( $current_user->ID, '_os_product_wishlist', array_unique($meta) );
					exit(json_encode(array('code'=>'del', 'msg'=>__('Product is successfully deleted from wishlist', SH_NAME ) ) ) );
				}else exit(json_encode(array('code'=>'fail', 'msg'=>__('Unable to find this product into wishlist', SH_NAME ) ) ) );
			}else{
				update_user_meta( $current_user->ID, '_os_product_wishlist', array( sh_set( $_POST, 'data_id' ) ) );
				exit(json_encode(array('code'=>'fail', 'msg'=>__('Unable to retrieve your wishlist', SH_NAME ) ) ) );
			}
		}
		else exit(json_encode(array('code'=>'fail', 'msg'=>__('Please login first to add/delete product in your wishlist', SH_NAME ) ) ) );
	}
	
	function sh_qoute_form_submit()
	{
	
		if( !count( $_POST ) ) return;
	
		_load_class( 'validation', 'helpers', true );
		$t = &$GLOBALS['_sh_base'];//printr($t);
		$settings = get_option(SH_NAME);
	
		/** set validation rules for contact form */
		$t->validation->set_rules('aplus_name','<strong>'.__('Name', SH_NAME).'</strong>', 'required|min_length[4]|max_lenth[30]');
		
		$t->validation->set_rules('aplus_l_name','<strong>'.__('Last Name', SH_NAME).'</strong>', 'required|min_length[4]|max_lenth[30]');
		$t->validation->set_rules('aplus_email','<strong>'.__('Email', SH_NAME).'</strong>', 'required|valid_email');
		
		$t->validation->set_rules('aplus_phone','<strong>'.__('Phone', SH_NAME).'</strong>', 'required|numeric');
		
		$t->validation->set_rules('aplus_service','<strong>'.__('Phone', SH_NAME).'</strong>', 'required');
		
		$t->validation->set_rules('aplus_message','<strong>'.__('Message', SH_NAME).'</strong>', 'required|min_length[5]');
		if( sh_set($settings, 'captcha_status') == 'on')
		{
			if( sh_set( $_POST, 'contact_captcha') !== sh_set( $_SESSION, 'captcha'))
			{
					$t->validation->_error_array['captcha'] = __('Invalid captcha entered, please try again.', SH_NAME);
			}
	
		}
				
		$messages = '';
		
		if($t->validation->run() !== FALSE && empty($t->validation->_error_array))
		{
			$name = $t->validation->post('aplus_name');
			$email = $t->validation->post('aplus_email');
	
			$message = $t->validation->post('aplus_message');
	
			$contact_to = ( sh_set($settings, 'contact_email') ) ? sh_set($settings, 'contact_email') : get_option('admin_email');
	
			$headers = 'From: '.$name.' <'.$email.'>' . "\r\n";
			wp_mail($contact_to, __('Contact Us Message', SH_NAME), $message, $headers);
	
			$message = sh_set($settings, 'success_message') ? $settings['success_message'] : sprintf( __('Thank you <strong>%s</strong> for using our Qoute form! Your email was successfully sent and we will be in touch with you soon.',SH_NAME), $name);
	
			$messages = '<div class="alert alert-success">'.__('SUCCESS! ', SH_NAME).$message.'</div>';
		}else
		{
	
			 if( is_array( $t->validation->_error_array ) )
			 {

				 foreach( $t->validation->_error_array as $msg )
				 {
					 $messages .= '<div class="alert alert-error">'.__('Error! ', SH_NAME).$msg.'</div>';
				 }
			}
	
		}
	
		echo $messages;exit;
		
	}
	
	function sh_contact_form_submit()
	{

		if( !count( $_POST ) ) return;
	
		_load_class( 'validation', 'helpers', true );
		$t = &$GLOBALS['_sh_base'];//printr($t);
		$settings = get_option(SH_NAME);
	
		/** set validation rules for contact form */
		$t->validation->set_rules('aplus_name','<strong>'.__('Name', SH_NAME).'</strong>', 'required|min_length[4]|max_lenth[30]');
		
		$t->validation->set_rules('aplus_l_name','<strong>'.__('Last Name', SH_NAME).'</strong>', 'required|min_length[4]|max_lenth[30]');
		$t->validation->set_rules('aplus_email','<strong>'.__('Email', SH_NAME).'</strong>', 'required|valid_email');
		
		$t->validation->set_rules('aplus_phone','<strong>'.__('Phone', SH_NAME).'</strong>', 'required|numeric');
		
		//$t->validation->set_rules('aplus_service','<strong>'.__('Phone', SH_NAME).'</strong>', 'required');
		
		$t->validation->set_rules('aplus_message','<strong>'.__('Message', SH_NAME).'</strong>', 'required|min_length[5]');
		if( sh_set($settings, 'captcha_status') == 'on')
		{
			if( sh_set( $_POST, 'contact_captcha') !== sh_set( $_SESSION, 'captcha'))
			{
					$t->validation->_error_array['captcha'] = __('Invalid captcha entered, please try again.', SH_NAME);
			}
	
		}
				
		$messages = '';
		
		if($t->validation->run() !== FALSE && empty($t->validation->_error_array))
		{
			$name = $t->validation->post('aplus_name');
			$email = $t->validation->post('aplus_email');
	
			$message = $t->validation->post('aplus_message');
	
			$contact_to = ( sh_set($settings, 'contact_email') ) ? sh_set($settings, 'contact_email') : get_option('admin_email');
	
			$headers = 'From: '.$name.' <'.$email.'>' . "\r\n";
			wp_mail($contact_to, __('Contact Us Message', SH_NAME), $message, $headers);
	
			$message = sh_set($settings, 'success_message') ? $settings['success_message'] : sprintf( __('Thank you <strong>%s</strong> for using our Contact form! Your email was successfully sent and we will be in touch with you soon.',SH_NAME), $name);
	
			$messages = '<div class="alert alert-success">'.__('SUCCESS! ', SH_NAME).$message.'</div>';
		}else
		{
	
			 if( is_array( $t->validation->_error_array ) )
			 {

				 foreach( $t->validation->_error_array as $msg )
				 {
					 $messages .= '<div class="alert alert-error">'.__('Error! ', SH_NAME).$msg.'</div>';
				 }
			}
	
		}
	
		echo $messages;exit;
		
	}
	
	function live_serch()
	{
		$query = sh_set( $_POST, 'query' );
		
		$wpquery = query_posts( array('post_type'=>'wpsc-product', 's'=> $query, 'showposts'=>10) );
		
		if( $wpquery ){
			echo '<ul>'."\n";
			
			foreach( $wpquery as $p ) echo '<li data-id="'.$p->ID.'">'.$p->post_title.'</li>'."\n";
			
			echo '</ul>';
			
		}
	}
	
	
	function live_serch_res()
	{
		$id = sh_set( $_POST, 'data_id' );
		$product_id = $id;
		$wpsc_variations_groups = new wpsc_variations( $product_id );
		$product = get_post( $id );
		$price = get_post_meta( $product_id, '_wpsc_price', true );
		if( !$product ) return;
		
		$attributes = maybe_unserialize( get_post_meta( $product_id, '_product_attributes', true ) );
		
		$product_gallery = explode(',' , get_post_meta($product_id , '_product_image_gallery' , true));
		
		?>
        
        <div class="wrap span4">
        	
        			<?php sh_product_gallery($product_id);?>
              
            <div class="product-content">
                <h3><?php echo _e('Description', SH_NAME);?></h3>
                <p><?php echo sh_set($product, 'post_content'); ?></p>
            </div>
        </div>
        <div class="span2">
            <span class="price-tax"><?php _e('Price', SH_NAME); ?>: <?php echo $price; ?></span>
             <span class="variations">
                <h3><?php _e('Variations', SH_NAME);?></h3>
                <?php foreach($wpsc_variations_groups->variation_groups as $wpsc_variations_group): ?>
                        <h4><?php echo sh_set($wpsc_variations_group, 'name'); ?>:</h4>
                 
                        <?php $variations = $wpsc_variations_groups->all_associated_variations;
         
                        $term_id = sh_set($wpsc_variations_group, 'term_id');
                        $terms = $variations[$term_id];
                        $i=0;
                        foreach($terms as $term):
                            if($i==0) echo '';
                            else echo sh_set($term, 'name').', ';
                            $i++;		 
                        endforeach;
                        ?>
                <?php endforeach; ?>
            
            </span>
        </div>
		
		<?php
	}
	
	
}