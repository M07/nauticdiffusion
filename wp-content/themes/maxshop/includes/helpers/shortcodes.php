<?php
class SH_Shortcodes
{
	
	protected $keys;
	protected $toggle_count = 0;
	
	function __construct()
	{
		$GLOBALS['sh_toggle_count'] = 0;
		add_action('init', array( $this, 'add' ) );
	}
	
	function add()
	{
		//include(get_template_directory().'/includes/resource/shortcodes.php');
		$options = array( 'product_categories', 'featured', 'recent', 'brands', 'slider', 'offer_box');

		$this->keys = $options;//array_keys( $options );
		
		foreach( $this->keys as $k )
		{
			if( method_exists( $this, $k ) ) add_shortcode( 'sh_'.$k, array( $this, $k ) );
		}
	}
	
	function product_categories($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'cat_ids' => '',
		), $atts ) );

		 $catogories = explode(",", $cat_ids);
			
		 $categories_list = get_terms( 'wpsc_product_category', array('hide_empty'=>0, 'include'=> $catogories) );
		
		$output ='<div class="categories-wrap">
                    <div class="container">
                        <div class="row">';
							foreach($categories_list as $cat):
							
								 $output .= '<div class="span4">
                                				<div class="categories">
                                    				<figure><img src="'.wpsc_category_image($cat->term_id).'">
													<div class="cate-overlay">
														<a href="'.get_term_link( $cat, 'wpsc_product_category' ).'">'.$cat->name.'</a>
													</div></figure></div></div>';
							endforeach;
							
						$output .='</div>
					</div>
				</div>';
		return $output;
	}
	
	function slider($atts, $contents = null)
	{
		global $wpsc_product, $wpsb, $post, $wpsc_variations;
		$t = $GLOBALS['_sh_base']; 
		extract( shortcode_atts( array(
			'num' => 5,
		), $atts ) );
		
		wp_enqueue_script( array( 'jquery-cycle-all' ) );
		wp_reset_query() ;
		$include = get_option('sticky_products');
		query_posts(array('post_type'=>'sh_slides', 'showposts'=>$num));
		$output = '';
		ob_start(); ?>
        
        
        <!-- SLIDER -->
        <div class="slider clearfix">
            <div class="slider-slides">
            	<?php while(have_posts()): the_post();?>
                	<?php $meta_settings = get_post_meta($post->ID, 'sh_slide_settings', true);?>
                    <div class="slides">
                        <a href="<?php echo sh_set($meta_settings, 'link');?>"><?php the_post_thumbnail('large-slide'); ?></a>
                        <div class="overlay">
                            <h1><?php the_title();?></h1>
                            <p><?php echo (sh_set($meta_settings, 'discount')) ? '<span>'.sh_set($meta_settings, 'discount').'</span> OFF<br/>' : ''; ?><?php echo sh_set($meta_settings, 'subtitle'); ?></p>
                        </div>
                    </div>
                <?php endwhile;?>
                
            </div>
            <a href="#" class="next"></a>
            <a href="#" class="prev"></a>
            <div class="slider-btn"></div>
        </div>
        <!-- SLIDER -->
        	
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		
		wp_reset_query() ;
		return $output;
	}
	
	function offer_box($atts, $contents = null)
	{
		global $wpsc_product, $wpsb, $post, $wpsc_variations;
		$t = $GLOBALS['_sh_base']; 
		extract( shortcode_atts( array(
			'season' => '',
			'discount' => '',
			'offer_period' => '',
			'offer_link' => '#',
			'image' => ''
		), $atts ) );
		
		ob_start(); ?>
        
        <div class="offers">
            <figure>
                <?php echo ($image) ? '<a href="'.$offer_link.'">'.wp_get_attachment_image( $image, '' ).'</a>' : '';?>
                <div class="overlay">
                    <h1><?php echo $season;?><span> <?php echo $discount;?></span> <small> - <?php echo $offer_period;?></small></h1>
                </div>
            </figure>
        </div>
        
        
        	
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		
		wp_reset_query() ;
		return $output;
	}
	
	function featured($atts, $contents = null)
	{
		global $wpsc_product, $wpsb, $post, $wpsc_variations;
		$t = $GLOBALS['_sh_base']; 
		extract( shortcode_atts( array(
			'title' => __('Featured', SH_NAME),
			'num' => 6,
			'sort' => 'date',
			'view' => 'span4'
		), $atts ) );
		
		wp_enqueue_script( array( 'jquery-tooltipster','jquery-prettyPhoto', ) );
		
		wp_reset_query() ;
		$include = get_option('sticky_products');
		query_posts(array('post_type'=>'wpsc-product', 'post__in'=>$include, 'showposts'=>$num, 'orderby'=>$sort));
		$output = '';
		ob_start(); ?>
        
        <?php $compare_template = sh_page_template('tpl-compare.php');
				
				
			if($compare_template) $compare_page_id = $compare_template->ID; 
			else $compare_page_id = '';
			
			$permalink = strpos(get_permalink($compare_page_id), '?');
			
			if($permalink == true){
				$seperator = "&";
			}else{
				$seperator = "?";
			}
		?>
        <div class="product_wrap">
        	<div class="container">
                
                <div class="row heading-wrap">
                    <div class="span12 heading">
                        <h2><?php echo $title; ?><span></span></h2>
                    </div>
                </div>
                
                <div class="row">
                    <?php $count = 0;
					while(have_posts()): the_post();
					 
						global $wpsc_variations;
						$classes = '';
						$count++;
						if ( 0 == ( $count - 1 ) % 6 )
							$classes = ' first';
						if ( 0 == $count % 6 )
							$classes = ' last';
                    	
						$wpsc_variations = new wpsc_variations( get_the_id() );?>
                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 product<?php echo $classes; ?>">
        
                            <div>
                                <figure class="featured">
                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                                    <div class="overlay">
                                        <a href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="zoom prettyPhoto"></a>
                                        <a href="<?php the_permalink(); ?>" class="link"></a>
                                    </div>
                                </figure>
                                <div class="detail">
                                    <span><?php wpsc_the_product_price_display( array( 'price_text'=>'%s', 'output_old_price' => false, 'output_you_save'  => false, ) )?></span>
                                    <h4><?php the_title(); ?></h4>
                                    <div class="icon">
            							<?php 
											if(wpsc_product_external_link(wpsc_the_product_id()) != '') {
												$action =  wpsc_product_external_link(wpsc_the_product_id()); 
											} else {
												$action = wpsc_this_page_url(); 
											}
                                    	?>
										<form class="product_form"  enctype="multipart/form-data" action="<?php echo $action; ?>" method="post" name="product_<?php echo wpsc_the_product_id(); ?>" id="product_<?php echo wpsc_the_product_id(); ?>" >
											<?php if(wpsc_product_has_stock() && $t->alloption('hide_addtocart_button') == 0 && get_option('addtocart_or_buynow') !='1' && !wpsc_have_variation_groups() ) : ?>
												<input type="hidden" value="add_to_cart" name="wpsc_ajax_action"/>
												<input type="hidden" value="<?php echo wpsc_the_product_id(); ?>" name="product_id"/>
												<button type="submit" name="Buy" class="wpsc_buy_button" id="product_<?php echo wpsc_the_product_id(); ?>_submit_button"><a class="one tooltip" title="<?php _e('Add to Cart', SH_NAME)?>"></a></button>
											<?php else: ?>
												<a href="<?php the_permalink(); ?>" class="one tooltip" title="<?php _e('Buy Now', SH_NAME); ?>"><?php _e('Buy Now', SH_NAME);?></a>
											<?php endif; ?>
											<div class="wpsc_loading_animation">
												<img title="" alt="<?php esc_attr_e( 'Loading', SH_NAME ); ?>" src="<?php echo wpsc_loading_animation_url(); ?>" />
											</div><!--close wpsc_loading_animation-->
										</form>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
        
        	
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		
		wp_reset_query() ;
		return $output;
	}
	
	
	function recent($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('Recent Prodcuts', SH_NAME),
			'num' => 6,
			'sort' => 'date',
			'view' => 'span4'
		), $atts ) );
		
		
		wp_reset_query() ;

		query_posts(array('post_type'=>'wpsc-product', 'showposts'=>$num, 'orderby'=>$sort));
		$output = '';
		ob_start(); ?>
        
        <section id="columns" class="container_9 clearfix col1">
        <article id="center_column" class=" grid_5">
        <div class="width-carousel">
            <div class="title-box">
                <h3 class="title-carousel first-brd"><?php echo $title; ?></h3>
            </div>
            
            <ul class="products-grid first odd bxslider">
            	<?php while(have_posts()): the_post();
				global $wpsc_variations;
				$wpsc_variations = new wpsc_variations( get_the_id() ); ?>
					<li class="item first fadeIn animated"> 
                    	<a class="fa-search-btn first-bg"  href="<?php echo get_template_directory_uri(); ?>/media/246-thickbox_default.jpg"> enlarge image <i class="icon-search"></i></a> 
                        <a class="product-image" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"> <img src="<?php echo get_template_directory_uri(); ?>/media/home-default.png" width="249" height="180" alt="product"></a>					
                        <h2 class="product-name"> <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a> </h2>
                        
                        <div class="actions"> 
                        	<?php if(wpsc_product_has_stock() && get_option('hide_addtocart_button') == 0 &&  get_option('addtocart_or_buynow') !='1' && !wpsc_have_variation_groups() ) : ?>
                                <form class="product_form"  enctype="multipart/form-data" action="<?php echo $action; ?>" method="post" name="product_<?php echo wpsc_the_product_id(); ?>" id="product_<?php echo wpsc_the_product_id(); ?>" >
                                    <input type="hidden" value="add_to_cart" name="wpsc_ajax_action"/>
                                    <input type="hidden" value="<?php echo wpsc_the_product_id(); ?>" name="product_id"/>
                                    <button type="submit" name="Buy" class="wpsc_buy_button icon-shopping-cart" id="product_<?php echo wpsc_the_product_id(); ?>_submit_button"><span><i class="icon-shopping-cart"></i></span></button>
                                    <div class="wpsc_loading_animation">
                                        <img title="" alt="<?php esc_attr_e( 'Loading', SH_NAME ); ?>" src="<?php echo wpsc_loading_animation_url(); ?>" />
                                    </div><!--close wpsc_loading_animation-->
                                </form>
                            <?php elseif( wpsc_have_variation_groups() ): ?>
                                <a href="<?php the_permalink(); ?>" class="btn-circle first-bg-hover"><i class="icon-link"></i></a>
                            <?php else: ?>
                                <a href="<?php the_permalink(); ?>" class="btn-circle first-bg-hover" title="<?php _e('Buy Now', SH_NAME); ?>"><?php _e('Buy Now', SH_NAME);?></a>
                            <?php endif; ?>
						</div>
                    </li>
                <?php endwhile; ?>
            </ul>
		</div>
		</article>
        </section>
        
        
        	
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		
		wp_reset_query() ;
		return $output;
	}
	
	/**
	 * Brands shortcode is used to show a carousel of brands custom post type
	 */
	 
	function brands($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('Our Brands', SH_NAME),
			'num' => 6,
			'sort' => 'date',
			'view' => 'span4'
		), $atts ) );
		
		wp_enqueue_script( array( 'jquery-carouFredSel', 'jquery-cycle-all' ) );
		
		wp_reset_query() ;

		query_posts(array('post_type'=>'sh_brand', 'showposts'=>$num, 'orderby'=>$sort));
		$output = '';
		ob_start(); ?>
        
		<!-- CLIENTS -->
        <div class="clients-wrap">
            <div class="container">
            	<div class="row heading-wrap">
                    <div class="span12 heading">
                        <h2><?php echo $title;?> <span></span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="span12 clients">
                    	<div class="elastislide-wrapper elastislide-horizontal">
                            <ul class="elastislide-list">
                        
                                <?php while(have_posts()): the_post(); 
                                    $meta = get_post_meta( get_the_id(), 'sh_brand_settings', true );?>
                                    
                                    <li> 
                                        <a href="<?php echo esc_url(sh_set( $meta, 'link' )); ?>" title="<?php the_title(); ?>">
                                            <?php the_post_thumbnail(); ?>
                                        </a> 
                                    </li>
                
                                <?php endwhile; ?>
                            </ul>
                            
<nav><span class="elastislide-prev" style="display: none;">Previous</span><span class="elastislide-next" style="display: inline-block;">Next</span></nav>
						</div>
                	</div>
                 </div>
        	</div>
        </div>
        	
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		
		wp_reset_query() ;
		return $output;
	}
	
}
?>