<?php
$t = &$GLOBALS['_sh_base'];

$query = $t->get_cache( 'flex_slider', 'wpquery', array('post_type'=>'flex_slider', 'showposts'=> 5) ); ?>


<div class="container">
    <div class="flexslider">
        <ul class="slides">
            <?php while( $query->have_posts() ): $query->the_post(); ?>
            <li> 
            	<?php if(has_post_thumbnail()) the_post_thumbnail('slider'); ?>
                <div class="flex-caption">
                	<h4><?php the_title(); ?></h4>
                    <?php the_excerpt(); ?>
                </div>
            </li>
            <?php endwhile; ?>
        </ul>
    </div>
</div>

<script type="text/javascript">
	jQuery(window).load(function() {
		jQuery('.flexslider').flexslider({
		  animation: "slide",
		  controlsContainer: ".flex-container"
	  });
	});
</script> 
<?php //wp_reset_query(); ?>