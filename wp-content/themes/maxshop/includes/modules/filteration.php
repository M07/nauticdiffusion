<?php
	$filteration = array(
						'pages' => range(10, 50, 10),
						'sort' => array('default'=>__('Default', SH_NAME), 'price_asc'=>__('Price (Low &gt; High)', SH_NAME), 'price_desc'=>__('Price (High &gt; Low)', SH_NAME),
										'rating_desc'=>__('Rating (Highest)', SH_NAME), 'rating_asc'=>__('Rating (Lowest)', SH_NAME), 'title_asc'=>__('Name (A - Z)', SH_NAME),
										'title_desc'=>__('Name (Z - A)', SH_NAME),),	
					);
					
$request_uri = 'http://'.$_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI'];
if(isset($_POST)){
	$_SESSION['bistro_pro_per_page'] = sh_set($_POST, 'pages');
	$_SESSION['bistro_pro_sort'] = sh_set($_POST, 'sort');
}
//echo $request_uri;exit;
?>

    
            <form method="post">   
                <div class="sorting-bar clearfix">
                    <div>
                        <label><?php _e('Show:', SH_NAME); ?></label>
                        
                        <select class="selectBox" name="pages" onchange="this.form.submit();">
                            <?php foreach( $filteration['pages'] as $page ): ?>
                          
                                <?php $selected = ( sh_set($_SESSION, 'bistro_pro_per_page') == $page ) ? 'selected="selected"' : ''; ?>
                                <option value="<?php echo $page;?>" <?php echo $selected; ?>><?php echo $page; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
        
                    <div class="show">
                       <label><?php _e('Sort:', SH_NAME); ?></label>
                        <select class="selectBox" name="sort" onchange="this.form.submit();">
                            <?php foreach( $filteration['sort'] as $k => $v ): ?>
                                <?php $selected = ( sh_set($_SESSION, 'bistro_pro_sort') == $k) ? 'selected="selected"' : ''; ?>
                                <option value="<?php echo $k; ?>" <?php echo $selected; ?>><?php echo $v; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
        
                    <div class="sorting-btn clearfix">
                         <label><?php _e('View As', SH_NAME); ?></label>
                          <?php $url_method = (strstr( $request_uri, '?' ) ) ? '&' : '?' ; ?>
                                <a id="list" data-original-title="<?php _e('View List Style', SH_NAME); ?>" data-placement="top" data-toggle="tooltip" class="list two list-btn first-bg" href="<?php echo add_query_arg('pro_view', 'list', $request_uri); ?>"></a>
                                <a data-original-title="<?php _e('View Grid Style', SH_NAME); ?>" data-placement="top" id="grid" data-toggle="tooltip" class="grid-btn one  first-bg active" href="<?php echo add_query_arg('pro_view', 'grid', $request_uri); ?>"></a>
     				</div>
                </div>
            </form>
      




