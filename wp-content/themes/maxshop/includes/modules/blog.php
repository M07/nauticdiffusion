<?php 
function get_attachment_url_by_slug( $slug ) {
  $args = array(
    'post_type' => 'attachment',
    'name' => sanitize_title($slug),
    'posts_per_page' => 1,
    'post_status' => 'inherit',
  );
  $_header = get_posts( $args );
  $header = $_header ? array_pop($_header) : null;
  return $header ? wp_get_attachment_url($header->ID) : '';
}
if( have_posts() ):
while( have_posts() ): the_post(); ?>
	
    <article <?php post_class( 'clearfix'); ?> id="post-<?php the_ID(); ?>">
        <div class="article-content">
            <h2><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a></h2>
            <p>
                <?php
                    $images = get_post_meta( wpsc_the_product_id(), '_wpsc_product_gallery', true );
                    $images_count = count($images);

                    $picture = '';
                    if ( $images && $images_count >= 1) :
                       $picture = wp_get_attachment_image(array_values($images)[0]);
                    endif;
                    $picture = $picture != '' ? $picture : "<img src='" . get_attachment_url_by_slug("no-photo-available") . "''/>";
                    echo $picture;
                    $description = get_the_excerpt() != '' ? get_the_excerpt() : _e('No description', SH_NAME);
                    echo $description;
                ?>
            </p>
            <ul class="post-meta clearfix">
                <li><a href="<?php the_permalink(); ?>#comments"><?php echo comments_number(); ?></a></li>
                <li><span><?php echo get_the_date(get_option('date_formate')); ?></span></li>
            </ul>
        
            <a href="<?php the_permalink(); ?>" class="read"><?php _e('Read More', SH_NAME);?></a>
        </div>
    </article>
<?php endwhile;

else: ?>
	<p>Aie!</p>
	<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', SH_NAME ); ?></p>
	<?php get_search_form(); ?>

<?php endif;?>