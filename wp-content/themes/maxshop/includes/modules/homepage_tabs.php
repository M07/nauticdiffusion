<?php $settings = get_option( 'wp_bistro' ); ?>
<div class="services">
    <div class="row-fluid">
        <div class="span7">
            <ul id="wdtTab" class="nav nav-tabs">
                <?php $types = array(1=>'latest_pro', 2=>'best_sellers', 3=>'daily_specials');
                foreach( $types as $k => $ty ):?>
                    <?php if( sh_set( $settings, 'product_tab'.$k.'_status') ): ?>
                        <li><a href="#<?php echo $ty; ?>" data-toggle="tab"><?php echo sh_set( $settings, 'product_tab'.$k.'_title'); ?></a></li>
                    <?php endif; ?>
                <?php endforeach;?>
            </ul>
            <div id="wdtTabContent" class="tab-content">
            <?php foreach( $types as $k => $ty ): ?>
                
                <?php if( !sh_tabs_filter_args( $ty ) ) continue; ?>
                
                <?php if( sh_set( $settings, 'product_tab'.$k.'_status') ):?>
                <div class="tab-pane fade in active" id="<?php echo $ty; ?>">
                    <div class="row-fluid">
                        <?php sh_generate_products_output( sh_tabs_filter_args( $ty ) );
                        wp_reset_query(); ?>
                    </div>
                </div>
                <?php endif; ?>
        
            <?php endforeach; ?>
            </div>
        </div>
        <div class="span5">
			<?php $query = new WP_Query( 'showposts=1&post_type=bistro_deal&meta_key=_bistro_bistro_deal_date&meta_value='.time().'&meta_compare=>&order=asc&orderby=meta_value' ); //printr($query);?>
            <?php while( $query->have_posts() ): $query->the_post(); ?>
            <h2 class="widget-title"><span><?php _e('Special Offer', SH_NAME); ?></span></h2>
            <div class="sidebar-line"><span></span></div>
            <div id="home-deal" class="row-fluid">
                <div class="get-pad">
                    <div class="span9 home-deal-wrapper"> 
                        <?php if( has_post_thumbnail() ) the_post_thumbnail('product-tab-medium', array('class' => 'img-polaroid'));?>
                        
                        <a class="btn btn-general" href="<?php the_permalink(); ?>"><?php _e('APPLY NOW!', SH_NAME); ?></a>
                        <p><?php echo character_limiter(get_the_excerpt(), 100); ?></p>
                        <div class="deal-discount-block"> 
                            <?php $meta = get_post_meta(get_the_ID(), '_bistro_bistro_deal_settings', true); //printr($meta);
                            $old_price = (int)get_post_meta(sh_set($meta, 'products'), '_wpsc_price', true);
                            $new_price = (int)get_post_meta(sh_set($meta, 'products'), '_wpsc_special_price', true);
                            $save = ($old_price - $new_price); ?>
                            
                            <span class="list-price-title"><?php _e('Value', SH_NAME); ?> <span class="price-num"><?php echo wpsc_get_currency_symbol().$new_price; ?></span></span> 
                            <span class="list-price-title"><?php _e('Discount', SH_NAME); ?> <span class="price-num"><?php echo number_format(($save/(($old_price) ? $old_price : 1))*100, 2);?>%</span></span> 
                            <span class="list-price-title"><?php _e('Save', SH_NAME); ?> <span class="price-num"><?php echo wpsc_get_currency_symbol().$save; ?></span></span> </div>
                    </div>
                    <div class="span3">
                        <div class="Counter">
                            <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    $("#countdown").countdown({
                                        date: "<?php echo date( 'd F Y h:i:s', strtotime(sh_set( $meta, 'end_date')) );?>", // add your date here.
                                        format: "on"
                                    });
                                });
                            </script>
                            <ul id="countdown">
                                <li><span class="days">00</span>
                                    <p class="timeRefDays"><?php _e('days', SH_NAME); ?></p>
                                </li>
                                <li><span class="hours">00</span>
                                    <p class="timeRefHours"><?php _e('hours', SH_NAME); ?></p>
                                </li>
                                <li><span class="minutes">00</span>
                                    <p class="timeRefMinutes"><?php _e('minutes', SH_NAME); ?></p>
                                </li>
                                <li><span class="seconds">00</span>
                                    <p class="timeRefSeconds"><?php _e('seconds', SH_NAME); ?></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
        </div>
    </div>
</div>

