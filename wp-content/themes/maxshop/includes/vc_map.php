<?php
vc_map( array(
			"name" => __("Product Categories.", SH_NAME),
			"base" => "sh_product_categories",
			"class" => "",
			"category" => __('MaxShop', SH_NAME),
			"icon" => 'catgories' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Enter Product Categories IDs", SH_NAME),
				   "param_name" => "cat_ids",
				   "description" => __("Enter categories ids comma seperated", SH_NAME)
				)
			)
	    )
);
  
  
vc_map( array(
			"name" => __("Featured Products", SH_NAME),
			"base" => "sh_featured",
			"class" => "",
			"category" => __('MaxShop', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title for About", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter Number of Posts to show.", SH_NAME)
				),
				
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sort By", SH_NAME),
				   "param_name" => "sort",
				    "value" => __( array('date'=>'Date', 'title'=>'Title', 'name'=>'Name', 'author'=>'Author', 'rand'=>'Random', 'comment_count'=>'Comments Count') ),
				   "description" => __("Choose Sort Option .", SH_NAME)
				)
			)
	    )
);

/*vc_map( array(
			"name" => __("Recent Products", SH_NAME),
			"base" => "sh_recent",
			"class" => "",
			"category" => __('OnlineSale', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title for About", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter Number of Posts to show.", SH_NAME)
				),
				
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sort By", SH_NAME),
				   "param_name" => "sort",
				    "value" => __( array('date'=>'Date', 'title'=>'Title', 'name'=>'Name', 'author'=>'Author', 'rand'=>'Random', 'comment_count'=>'Comments Count') ),
				   "description" => __("Choose Sort Option .", SH_NAME)
				)
			)
	    )
);*/


vc_map( array(
			"name" => __("Our Brands", SH_NAME),
			"base" => "sh_brands",
			"class" => "",
			"category" => __('MaxShop', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title for About", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter Number of Posts to show.", SH_NAME)
				),
				
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sort By", SH_NAME),
				   "param_name" => "sort",
				    "value" => __( array('date'=>'Date', 'title'=>'Title', 'name'=>'Name', 'author'=>'Author', 'rand'=>'Random', 'comment_count'=>'Comments Count') ),
				   "description" => __("Choose Sort Option .", SH_NAME)
				)
			)
	    )
);

vc_map( array(
			"name" => __("Slider", SH_NAME),
			"base" => "sh_slider",
			"class" => "",
			"category" => __('MaxShop', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter Number of slides to show.", SH_NAME)
				),
			)
	    )
);

vc_map( array(
			"name" => __("Offer Box", SH_NAME),
			"base" => "sh_offer_box",
			"class" => "",
			"category" => __('MaxShop', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Season", SH_NAME),
				   "param_name" => "season",
				   "description" => __("Enter the season", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Discount", SH_NAME),
				   "param_name" => "discount",
				   "description" => __("Enter the discount which you want to offer", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Offer Period", SH_NAME),
				   "param_name" => "offer_period",
				   "description" => __("Enter the offer period", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Offer URL", SH_NAME),
				   "param_name" => "offer_link",
				   "description" => __("Enter the offer link where you want to redirect user", SH_NAME)
				),
				array(
				   "type" => "attach_image",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Image", SH_NAME),
				   "param_name" => "image",
				   "description" => __("Upload image for offer box.", SH_NAME)
				),
			)
	    )
);


/*
function sh_custom_css_classes_for_vc_row_and_vc_column($class_string, $tag) {

	if($tag=='vc_row' || $tag=='vc_row_inner') {
		$class_string = str_replace('vc_row-fluid', 'row-fluid', $class_string);
	}
	if($tag=='vc_column' || $tag=='vc_column_inner') {
		$class_string = str_replace('vc_span1', 'span1', $class_string);
		$class_string = str_replace('vc_span2', 'span2', $class_string);
		$class_string = str_replace('vc_span3', 'span3', $class_string);
		$class_string = str_replace('vc_span4', 'span4', $class_string);
		$class_string = str_replace('vc_span5', 'span5', $class_string);
		$class_string = str_replace('vc_span6', 'span6', $class_string);
		$class_string = str_replace('vc_span7', 'span7', $class_string);
		$class_string = str_replace('vc_span8', 'span8', $class_string);
		$class_string = str_replace('vc_span9', 'span9', $class_string);
		$class_string = str_replace('vc_span10', 'span10', $class_string);
		$class_string = str_replace('vc_span11', 'span11', $class_string);
		$class_string = str_replace('vc_span12', 'container', $class_string);
	}
	return $class_string;
}*/
// Filter to Replace default css class for vc_row shortcode and vc_column
//add_filter('vc_shortcodes_css_class', 'sh_custom_css_classes_for_vc_row_and_vc_column', 10, 2);


function vc_theme_vc_row($atts, $content = null) {
	
   extract(shortcode_atts(array(
		'el_class'        => '',
		'bg_image'        => '',
		'bg_color'        => '',
		'bg_image_repeat' => '',
		'font_color'      => '',
		'padding'         => '',
		'margin_bottom'   => '',
		'container'		  => '',
	), $atts));
	$atts['base'] = '';
	
	wp_enqueue_style( 'js_composer_front' );
	wp_enqueue_script( 'wpb_composer_front_js' );
	wp_enqueue_style('js_composer_custom_css');
	
	$vc_row = new WPBakeryShortCode_VC_Row($atts);
	$el_class = $vc_row->getExtraClass($el_class);
	$output = '';
	$css_class =  $el_class;
	
	
	
	$style = $vc_row->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);
	
   if( $container == 'true' ) return '<section '.$style.'><div class="container"><div class="row">'.wpb_js_remove_wpautop($content).'</div></div></section>';
   else return '<section '.$style.'><div class="row">'.wpb_js_remove_wpautop($content).'</div></section>';
}

function vc_theme_vc_column($atts, $content = null) {
	
   extract( shortcode_atts( array( 'width'=> '1/1', 'el_class'=>'' ), $atts ) );
   
   $width = wpb_translateColumnWidthToSpan($width);
   $width = str_replace('vc_', '', $width);
   $el_class = ($el_class) ? ' '.$el_class : '';
   return '<div class="'.$width.$el_class.'">'.do_shortcode($content).'</div>';
}

$param = array(
  "type" => "dropdown",
  "holder" => "div",
  "class" => "",
  "heading" => __("Container", SH_NAME),
  "param_name" => "container",
  "value" => array('True'=>'true', 'False'=>'false'),
  "description" => __("Choose whether you want to add a container before row or not.", SH_NAME)
);

vc_add_param('vc_row', $param);

