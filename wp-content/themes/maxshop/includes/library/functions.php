<?php

if( !function_exists( '_WSH' ) )
{
	function _WSH()
	{
		return $GLOBALS['_sh_base'];
	}
}

/** A function to fetch the categories from wordpress */
function sh_get_categories($arg = false)
{
	global $wp_taxonomies;
	if( ! empty($arg['taxonomy']) && ! isset($wp_taxonomies[$arg['taxonomy']]))
	{
		register_taxonomy( $arg['taxonomy'], 'sh_'.$arg['taxonomy']);
	}
	
	$categories = get_categories($arg);
	$cats = array();
	
	foreach($categories as $category)
	{
		$cats[$category->term_id] = $category->name;
	}
	return $cats;
}

function sh_font_awesome( $code = false )
{
	$pattern = '/\.(icon-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"(.+)";\s+}/';
	$subject = @file_get_contents(get_template_directory().'/font-awesome/css/font-awesome.css');
	
	if( !$subject ) return array();
	
	preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);
	
	$icons = array();

	foreach($matches as $match){
		$value = str_replace( 'icon-', '', $match[1] );
		if( $code ) $icons[$match[1]] = stripslashes($match[2]);
		else $icons[$match[1]] = ucwords(str_replace('-', ' ', $value));//$match[2];
	}
	
	//$icons = var_export($icons, TRUE);
	//$icons = stripslashes($icons);
	
	//printr($icons);
	return $icons;
}


function sh_get_sidebars($multi = false)
{
	global $wp_registered_sidebars;

	$sidebars = !($wp_registered_sidebars) ? get_option('wp_registered_sidebars') : $wp_registered_sidebars;

	if( $multi ) $data[] = array('value'=>'', 'label' => 'No Sidebar');
	else $data = array('' => __('No Sidebar', SH_NAME));
	foreach( (array)$sidebars as $sidebar)
	{
		if( $multi ) $data[] = array( 'value'=> sh_set($sidebar, 'id'), 'label' => sh_set( $sidebar, 'name') );
		else $data[sh_set($sidebar, 'id')] = sh_set($sidebar, 'name');
	}

	return $data;
}

if ( ! function_exists('character_limiter'))
{
	function character_limiter($str, $n = 500, $end_char = '&#8230;', $allowed_tags = false)
	{
		if($allowed_tags) $str = strip_tags($str, $allowed_tags);
		if (strlen($str) < $n)	return $str;
		$str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

		if (strlen($str) <= $n) return $str;

		$out = "";
		foreach (explode(' ', trim($str)) as $val)
		{
			$out .= $val.' ';
			
			if (strlen($out) >= $n)
			{
				$out = trim($out);
				return (strlen($out) == strlen($str)) ? $out : $out.$end_char;
			}		
		}
	}
}


function get_social_icons()
{
	//$t = $GLOBALS['_sh_base'];
	$options = _WSH()->option();//printr($options);
	$icons = array('fb'=>__('Like us on Facebook', SH_NAME), 'twet'=>__('Follow us on Twitter', SH_NAME),'rss'=>__('Follow us on rss', SH_NAME), 'google'=>__('Circle Us on Google Plus', SH_NAME), 'pin'=>__('Follow us on Pinterest', SH_NAME));			
	if( $options ):?>
    <div class="social-icon">
    	
        	<?php foreach( $icons as $i => $str ): ?>
            	<?php if( $url = sh_set( $options, $i ) ):?>
            		<a target="_blank" class="<?php echo $i;?>" title="<?php echo $str; ?>" href="<?php echo $url; ?>"> </a>
                <?php endif;?>
            <?php endforeach;?>
       
    </div>
    <?php endif;
	
}

function sh_get_product_thumbnail($id = null, $size = 'slider')
{
	global $post;
	$id = ( $id == null ) ? sh_set( $post, 'ID') : $id;
	if ( has_post_thumbnail( $id ) ) {
		$image = get_the_post_thumbnail( $id, $size );
		return $image;
	} else{
		$attached_images = (array)get_posts( array(
				'post_type' => 'attachment',
				'numberposts' => 1,
				'post_status' => null,
				'post_parent' => $id,
				'orderby' => 'menu_order',
				'order' => 'ASC'
			) );
		$attached_image = sh_set($attached_images, 0);
		$image = wp_get_attachment_image( sh_set($attached_image, 'ID'), $size );
		return ( $image ) ? $image : false;
	}
}


function sh_get_rating( $product_id )
{
	global $wpdb;
	
	$res = $wpdb->get_results( $wpdb->prepare( "SELECT AVG(`rated`) AS `average`, COUNT(*) AS `count` FROM `" . WPSC_TABLE_PRODUCT_RATING . "` WHERE `productid`= %d ", $product_id ), ARRAY_A );
	if( $res && is_array( $res ) ){
		return sh_set( current( (array)$res ), 'average' );
	}
	return '0';
}


function sh_get_posts_array( $post_type = 'post' )
{
	global $wpdb;

	$res = $wpdb->get_results( "SELECT `ID`, `post_title` FROM `" .$wpdb->prefix. "posts` WHERE `post_type` = '$post_type' AND `post_status` = 'publish' ", ARRAY_A );
	$return = array();
	foreach( $res as $r) $return[sh_set($r, 'ID')] = sh_set($r, 'post_title');
	
	return $return;
}


if( !function_exists( 'bistro_slug' ) )
{
	function bistro_slug( $string )
	{
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
}

if( !function_exists( 'sh_page_template' ) )
{
	function sh_page_template( $tpl )
	{
		$posts = get_posts( array( 'post_type' =>'page', 'meta_key' => '_wp_page_template', 'meta_value' => $tpl ) );
		
		if( $posts )
		{
			return current( (array)$posts );
		}
		return array();
	}
}

function get_the_breadcrumb()
{
	global $_webnukes, $wp_query;
	$queried_object = get_queried_object();
	//printr($queried_object);
	$breadcrumb = '';

	if ( ! is_home() || ! is_front_page())
	{
		$breadcrumb .= '<a href="'.home_url().'">'.__('Home', SH_NAME).'</a><span class="navigation-pipe">/</span>';
		
		/** If category or single post */
		if(is_category())
		{
			$breadcrumb .= '<span class="navigation-page">'.single_cat_title('', FALSE).'</span>';
		}
		elseif(is_tax())
		{
			$breadcrumb .= '<span class="navigation-page">'.$queried_object->name.'</span>';
		}
		elseif(is_page()) /** If WP pages */
		{
			global $post;
			if($post->post_parent)
			{
                $anc = get_post_ancestors($post->ID);
                foreach($anc as $ancestor)
				{
                    $breadcrumb .= '<span class="navigation-pipe"><a href="'.get_permalink($ancestor).'">'.get_the_title($ancestor).'</a></span><span class="navigation-pipe">/</span>';
                }
				$breadcrumb .= '<span class="navigation-page">'.get_the_title($post->ID).'</span>';
				
            }else $breadcrumb .= '<span class="navigation-page">'.get_the_title().'</span>';
		}
		elseif (is_singular())
		{
			if($category = wp_get_object_terms(get_the_ID(), array('category')))
			{
				if( !is_wp_error($category) )
				{
					$breadcrumb .= '<a href="'.get_term_link(sh_set($category, '0')).'">'.sh_set( sh_set($category, '0'), 'name').'</a><span class="navigation-pipe">/</span>';
					$breadcrumb .= '<span class="navigation-page">'.get_the_title().'</span>';
				}
			}else{
				$breadcrumb .= '<span class="navigation-page">'.get_the_title().'</span>';
			}
		}
		elseif(is_tag()) $breadcrumb .= '<span class="navigation-page">'.single_tag_title('', FALSE).'</span>'; /**If tag template*/
		elseif(is_day()) $breadcrumb .= '<span class="navigation-page">'.__('Archive for ', SH_NAME).get_the_time('F jS, Y').'</a>'; /** If daily Archives */
		elseif(is_month()) $breadcrumb .= '<span class="navigation-page">'.__('Archive for ', SH_NAME).get_the_time('F, Y').'</span>'; /** If montly Archives */
		elseif(is_year()) $breadcrumb .= '<span class="navigation-page">'.__('Archive for ', SH_NAME).get_the_time('Y').'</span>'; /** If year Archives */
		elseif(is_author()) $breadcrumb .= '<span class="navigation-page">'.__('Archive for ', SH_NAME).get_the_author().'</span>'; /** If author Archives */
		elseif(is_search()) $breadcrumb .= '<span class="navigation-page">'.__('Search Results for ', SH_NAME).get_search_query().'</span>'; /** if search template */
		elseif(is_404()) $breadcrumb .= '<span class="navigation-page">'.__('404 - Not Found', SH_NAME).'</span>'; /** if search template */
		elseif($wp_query->is_posts_page == true) $breadcrumb .= '<span class="navigation_page">'.$queried_object->post_title.'</span>';
		else $breadcrumb .= '<span class="navigation-page">'.get_the_title().'</span>'; /** Default value */
	}

	return '<div class="breadcrumb">'.$breadcrumb.'</div>';
}



function sh_register_user( $data )
{
	//printr($data);
	$user_name = sh_set( $data, 'user_login' );
	$user_email = sh_set( $data, 'user_email' );
	$user_pass = sh_set( $data, 'user_password' );
	$policy = sh_set( $data, 'policy_agreed');
	
	$user_id = username_exists( $user_name );
	$message = '<div class="alert-error" style="margin-bottom:10px;padding:10px"><h5>'.__('You must agreed the policy', SH_NAME).'</h5></div>';;
	if( !$policy ) $message = '';
	if ( !$user_id && email_exists($user_email) == false ) {

		if( $policy ){

			$random_password = ( $user_pass ) ? $user_pass : wp_generate_password( $length=12, $include_standard_special_chars=false );
			$user_id = wp_create_user( $user_name, $random_password, $user_email );
			if ( is_wp_error($user_id) && is_array( $user_id->get_error_messages() ) ) 
			{
				foreach($user_id->get_error_messages() as $message)	$message .= '<div class="alert-error" style="margin-bottom:10px;padding:10px"><h5>'.$message.'</h5></div>';
			}
			else $message = '<div class="alert-success" style="margin-bottom:10px;padding:10px"><h5>'.__('Registration Successful - An email is sent', SH_NAME).'</h5></div>';
		}
		
	} else {
		$message .= '<div class="alert-error" style="margin-bottom:10px;padding:10px"><h5>'.__('Username or email already exists.  Password inherited.', SH_NAME).'</h5></div>';
	}

	return $message;
}




function sh_list_comments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment; 
	//if(get_comment_type() == 'comment'):?>
    
            <li id="comment-<?php echo comment_ID(); ?>" class="clearfix">
                
                  <figure><a href="<?php echo get_comment_author_url(); ?> "><?php echo get_avatar( $comment, 70 ); ?></a></figure>
                  <div class="post-content">
                    <p>
						<?php echo get_comment_author_link(); ?> 
                        <?php printf( __('%1$s at %2$s', SH_NAME), get_comment_date(),  get_comment_time()) ?>
                    </p>
                   <?php comment_text(); ?>
                    <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?> 
                  </div>
               
               
        
            


	<?php
	//endif;
}



/**
 * returns the formatted form of the comments
 *
 * @param	array	$args		an array of arguments to be filtered
 * @param	int		$post_id	if form is called within the loop then post_id is optional
 *
 * @return	string	Return the comment form
 */
function sh_comment_form( $args = array(), $post_id = null, $review = false )
{
	if ( null === $post_id )
		$post_id = get_the_ID();
	else
		$id = $post_id;

	$commenter = wp_get_current_commenter();
	$user = wp_get_current_user();
	$user_identity = $user->exists() ? $user->display_name : '';

	$args = wp_parse_args( $args );
	if ( ! isset( $args['format'] ) )
		$args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';

	$req      = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );
	$html5    = 'html5' === $args['format'];
	$fields   =  array(
		'author' =>  '<input id="author" class="span9" name="author" placeholder="'.__('Name', SH_NAME).'" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />',
		'email'  => ''.
		            '<input id="email" placeholder="'.__('Email', SH_NAME).'" class="span9" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />',
		'url'    => '<input id="url" placeholder="'.__('Website', SH_NAME).'" class="span9" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" />',
	);

	$required_text = sprintf( ' ' . __('Required fields are marked %s', SH_NAME), '<span class="required">*</span>' );

	/**
	 * Filter the default comment form fields.
	 *
	 * @since 3.0.0
	 *
	 * @param array $fields The default comment fields.
	 */
	 $comment_field_class = (is_user_logged_in() ) ? 'span12' : 'span9';
	$fields = apply_filters( 'comment_form_default_fields', $fields );
	$defaults = array(
		'fields'               => $fields,
		'comment_field'        => '<textarea id="comment" class="'.$comment_field_class.'" name="comment" cols="45" rows="8" aria-required="true"></textarea>',
		'must_log_in'          => '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.', SH_NAME ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
		'logged_in_as'         => '<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', SH_NAME ), get_edit_user_link(), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
		'comment_notes_before' => '<p class="comment-notes">' . __( 'Your email address will not be published.', SH_NAME ) . ( $req ? $required_text : '' ) . '</p>',
		'comment_notes_after'  => '<p class="form-allowed-tags">' . sprintf( __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s' ), ' <code>' . allowed_tags() . '</code>' ) . '</p>',
		'id_form'              => 'commentform',
		'id_submit'            => 'submit',
		'title_reply'          => __( 'Leave a Reply', SH_NAME ),
		'title_reply_to'       => __( 'Leave a Reply to %s', SH_NAME ),
		'cancel_reply_link'    => __( 'Cancel reply', SH_NAME ),
		'label_submit'         => __( 'Add a Comment', SH_NAME ),
		'format'               => 'xhtml',
	);

	/**
	 * Filter the comment form default arguments.
	 *
	 * Use 'comment_form_default_fields' to filter the comment fields.
	 *
	 * @since 3.0.0
	 *
	 * @param array $defaults The default comment form arguments.
	 */
	$args = wp_parse_args( $args, apply_filters( 'comment_form_defaults', $defaults ) );

	?>
		<?php if ( comments_open( $post_id ) ) : ?>
			<?php
			/**
			 * Fires before the comment form.
			 *
			 * @since 3.0.0
			 */
			do_action( 'comment_form_before' );
			?>
			<div id="respond" class="contact-form">
				<h3 id="reply-title"><?php comment_form_title( $args['title_reply'], $args['title_reply_to'] ); ?> <small><?php cancel_comment_reply_link( $args['cancel_reply_link'] ); ?></small></h3>
				<?php if ( get_option( 'comment_registration' ) && !is_user_logged_in() ) : ?>
					<?php echo $args['must_log_in']; ?>
					<?php
					/**
					 * Fires after the HTML-formatted 'must log in after' message in the comment form.
					 *
					 * @since 3.0.0
					 */
					do_action( 'comment_form_must_log_in_after' );
					?>
				<?php else : ?>
					<form action="<?php echo site_url( '/wp-comments-post.php' ); ?>" method="post" id="<?php echo esc_attr( $args['id_form'] ); ?>" class="form-add-comment comment-form"<?php echo $html5 ? ' novalidate' : ''; ?>>
						<?php
						/**
						 * Fires at the top of the comment form, inside the <form> tag.
						 *
						 * @since 3.0.0
						 */
						do_action( 'comment_form_top' );
						?>
                        
						<?php if ( is_user_logged_in() ) : ?>
							<?php
							/**
							 * Filter the 'logged in' message for the comment form for display.
							 *
							 * @since 3.0.0
							 *
							 * @param string $args['logged_in_as'] The logged-in-as HTML-formatted message.
							 * @param array  $commenter            An array containing the comment author's username, email, and URL.
							 * @param string $user_identity        If the commenter is a registered user, the display name, blank otherwise.
							 */
							echo apply_filters( 'comment_form_logged_in', $args['logged_in_as'], $commenter, $user_identity );
							?>
							<?php
							/**
							 * Fires after the is_user_logged_in() check in the comment form.
							 *
							 * @since 3.0.0
							 *
							 * @param array  $commenter     An array containing the comment author's username, email, and URL.
							 * @param string $user_identity If the commenter is a registered user, the display name, blank otherwise.
							 */
							do_action( 'comment_form_logged_in_after', $commenter, $user_identity );
							?>
						<?php else : ?>
							<?php echo $args['comment_notes_before']; ?>
                            <fieldset>
							<?php
							/**
							 * Fires before the comment fields in the comment form.
							 *
							 * @since 3.0.0
							 */
							do_action( 'comment_form_before_fields' );
							foreach ( (array) $args['fields'] as $name => $field ) {
								/**
								 * Filter a comment form field for display.
								 *
								 * The dynamic portion of the filter hook, $name, refers to the name
								 * of the comment form field. Such as 'author', 'email', or 'url'.
								 *
								 * @since 3.0.0
								 *
								 * @param string $field The HTML-formatted output of the comment form field.
								 */
								echo apply_filters( "comment_form_field_{$name}", $field ) . "\n";
							}
							/**
							 * Fires after the comment fields in the comment form.
							 *
							 * @since 3.0.0
							 */
							do_action( 'comment_form_after_fields' );
							?>
                            </fieldset>
						<?php endif; ?>
						<?php
						/**
						 * Filter the content of the comment textarea field for display.
						 *
						 * @since 3.0.0
						 *
						 * @param string $args['comment_field'] The content of the comment textarea field.
						 */
						echo apply_filters( 'comment_form_field_comment', $args['comment_field'] );
						?>
						<?php echo $args['comment_notes_after']; ?>
						<div class="btn-set">
							<input name="submit" type="submit" class="button btn-general" id="<?php echo esc_attr( $args['id_submit'] ); ?>" value="<?php echo esc_attr( $args['label_submit'] ); ?>" />
							<?php comment_id_fields( $post_id ); ?>
						</div>
						<?php
						/**
						 * Fires at the bottom of the comment form, inside the closing </form> tag.
						 *
						 * @since 1.5.2
						 *
						 * @param int $post_id The post ID.
						 */
						do_action( 'comment_form', $post_id );
						?>
					</form>
				<?php endif; ?>
			</div><!-- #respond -->
			<?php
			/**
			 * Fires after the comment form.
			 *
			 * @since 3.0.0
			 */
			do_action( 'comment_form_after' );
		else :
			/**
			 * Fires after the comment form if comments are closed.
			 *
			 * @since 3.0.0
			 */
			do_action( 'comment_form_comments_closed' );
		endif;
}





function sh_contact_form_submit()
{
	if( !count( $_POST ) ) return;
	
	
	_load_class( 'validation', 'helpers', true );
	$t = &$GLOBALS['_sh_base'];//printr($t);
	$settings = get_option('wp_bistro');

	/** set validation rules for contact form */
	$t->validation->set_rules('contact_name','<strong>'.__('Name', SH_NAME).'</strong>', 'required|min_length[4]|max_lenth[30]');
	$t->validation->set_rules('contact_email','<strong>'.__('Email', SH_NAME).'</strong>', 'required|valid_email');
	$t->validation->set_rules('contact_message','<strong>'.__('Message', SH_NAME).'</strong>', 'required|min_length[5]');
	if( sh_set($settings, 'captcha_status') == 'on')
	{
		if( sh_set( $_POST, 'contact_captcha') !== sh_set( $_SESSION, 'captcha'))
		{
			$t->validation->_error_array['captcha'] = __('Invalid captcha entered, please try again.', SH_NAME);
		}
	}
	
	$messages = '';
	
	if($t->validation->run() !== FALSE && empty($t->validation->_error_array))
	{
		
		$name = $t->validation->post('contact_name');
		$email = $t->validation->post('contact_email');
		$message = $t->validation->post('contact_message');
		$contact_to = ( sh_set($settings, 'contact_email') ) ? sh_set($settings, 'contact_email') : get_option('admin_email');
		
		$headers = 'From: '.$name.' <'.$email.'>' . "\r\n";
		wp_mail($contact_to, __('Contact Us Message', SH_NAME), $message, $headers);
		
		$message = sh_set($settings, 'success_message') ? $settings['success_message'] : sprintf( __('Thank you <strong>%s</strong> for using our contact form! Your email was successfully sent and we will be in touch with you soon.',SH_NAME), $name);

		$messages = '<div class="alert alert-success">
						<p class="title">'.__('SUCCESS! ', SH_NAME).$message.'</p>
					</div>';
							
	}else
	{
		 if( is_array( $t->validation->_error_array ) )
		 {
			 foreach( $t->validation->_error_array as $msg )
			 {
				 $messages .= '<div class="alert alert-error">
									<p class="title">'.__('Error! ', SH_NAME).$msg.'</p>
								</div>';
			 }
		 }
	}
	
	return $messages;
}


function sh_blog_excerpt_more( $more ) {
	return '';
}
add_filter('excerpt_more', 'sh_blog_excerpt_more');



function _the_pagination($args = array(), $echo = 1)
{
	
	global $wp_query;
	
	$default =  array('base' => str_replace( 99999, '%#%', esc_url( get_pagenum_link( 99999 ) ) ), 'format' => '?paged=%#%', 'current' => max( 1, get_query_var('paged') ),
						'total' => $wp_query->max_num_pages, 'next_text' => '>', 'prev_text' => '<', 'type'=>'list');
						
	$args = wp_parse_args($args, $default);			
	
	$pagination = '<div class="pagination clearfix">'.paginate_links($args).'</div>';
	
	if(paginate_links(array_merge(array('type'=>'array'),$args)))
	{
		if($echo) echo $pagination;
		return $pagination;
	}
}


function sh_demo_color_picker()
{
	wp_enqueue_style( 'wp-color-picker' );
	
	wp_enqueue_script(
        'iris',
        admin_url( 'js/iris.min.js' ),
        array( 'jquery-ui-draggable', 'jquery-ui-slider', 'jquery-touch-punch' ),
        false,
        1
    );
    wp_enqueue_script(
        'wp-color-picker',
        admin_url( 'js/color-picker.min.js' ),
        array( 'iris' ),
        false,
        1
    );
    $colorpicker_l10n = array(
        'clear' => __( 'Clear', SH_NAME ),
        'defaultString' => __( 'Default', SH_NAME ),
        'pick' => __( 'Select Color', SH_NAME )
    );
    wp_localize_script( 'wp-color-picker', 'wpColorPickerL10n', $colorpicker_l10n );
}


function sh_get_post_format_output($settings = array())
{
	global $post;
	
	if( ! $settings ) return;
	
	$format = get_post_format();
	
	$output = '';
	
	switch( $format )
	{
		case 'standard':
		case 'image':
			$output = get_the_post_thumbnail(get_the_id(), '700x400');
		break;
		case 'gallery':
			$attachments = get_posts( 'post_type=attachment&post_parent='.get_the_id() );
			if( $attachments ){
				$output = '<div class="slider-post flexslider flex-direction-nav-on-top">
                      <ul class="slides">';
				foreach( $attachments as $att ){
					$output .= '
                        <li>
                          '.wp_get_attachment_image( $att->ID, 'full').'
                        </li>';
				}
				$output .= '</ul></div>';
			}
		break;
		case 'video':
			$output = sh_set( $settings, 'video');
		break;
		case 'audio':
			$output = sh_set( $settings, 'audio');
		break;
		case 'quoted':
		case 'link':
			
		break;
		default:
			$output = get_the_post_thumbnail(get_the_id(), '700x400');
		break;
	}
	
	return $output;
}

function sh_pro_list_view( $p =  null )
{
	global $post, $wpsc;
	$post = is_object( $p ) ? $p : $post;
	 
	$t = $GLOBALS['_sh_base'];
	?>
    <?php $compare_template = sh_page_template('tpl-compare.php');
		$compare_page_id = $compare_template->ID;
		
		$permalink = strpos(get_permalink($compare_page_id), '?');
		
		if($permalink == true){
			$seperator = "&";
		}else{
			$seperator = "?";
		}
	?>
    <div class="product clearfix">
    	<?php if(wpsc_show_thumbnails()) :?> 
            <figure class="categories-list">
                <a href="<?php the_permalink(); ?>"><?php echo sh_get_product_thumbnail( wpsc_the_product_id());?></a>
                <div class="overlay">
                    <a href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="zoom"></a>
                    <a href="<?php the_permalink(); ?>" class="link"></a>
                </div>
            </figure>
        <?php endif;?>
        <div class="detail">
        	<?php if( get_option('hide_name_link') == 1 ): ?>
            	<h4><?php the_title(); ?></h4>
            <?php else: ?>
            	<h4><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></h4></a>
            <?php endif;?>
            <span><?php wpsc_the_product_price_display( array( 'price_text'=>'%s', 'output_old_price' => false, 'output_you_save'  => false, ) ); ?></span>
            <div class="box">
            	<div class="description-product">
            		<?php echo wpsc_the_product_description(); ?>
            	</div>
            </div>
            <div class="icon">
                
                  <?php 
					if(wpsc_product_external_link(wpsc_the_product_id()) != '') $action =  wpsc_product_external_link(wpsc_the_product_id()); 
					else $action = wpsc_this_page_url(); ?>
                     <form class="product_form"  enctype="multipart/form-data" action="<?php echo $action; ?>" method="post" name="product_<?php echo wpsc_the_product_id(); ?>" id="product_<?php echo wpsc_the_product_id(); ?>" >
                	  <?php if(wpsc_product_has_stock() && get_option('hide_addtocart_button') == 0 &&  get_option('addtocart_or_buynow') !='1' && !wpsc_have_variation_groups()) : ?>
                       
                    <input type="hidden" value="add_to_cart" name="wpsc_ajax_action"/>
                    <input type="hidden" value="<?php echo wpsc_the_product_id(); ?>" name="product_id"/>
                    <button type="submit" name="Buy" class="wpsc_buy_button" id="product_<?php echo wpsc_the_product_id(); ?>_submit_button"><a class="one tooltip" title="<?php _e('Add to Cart', SH_NAME)?>"></a></button>
                   
                      <?php elseif( wpsc_have_variation_groups() ): ?>
                        <a href="<?php the_permalink(); ?>" class="one tooltip" title="<?php _e('Add to Cart', SH_NAME)?>"></a> 
                    <?php else: ?>
                        <a href="<?php the_permalink(); ?>" class="one tooltip" title="<?php _e('Buy Now', SH_NAME); ?>"><?php _e('Buy Now', SH_NAME);?></a>
                    <?php endif; ?>
                      <div class="wpsc_loading_animation">
                        <img title="" alt="<?php esc_attr_e( 'Loading', SH_NAME ); ?>" src="<?php echo wpsc_loading_animation_url(); ?>" />
                    </div><!--close wpsc_loading_animation-->
                </form>
                       
            </div>
        </div>
    </div>
    
	<?php
}

function sh_pro_grid_view( $p = null)
{
	global $post;
	$post = is_object( $p ) ? $p : $post;
	$t = $GLOBALS['_sh_base']; ?>
    
     <?php $compare_template = sh_page_template('tpl-compare.php');
			$compare_page_id = $compare_template->ID;
			
			$permalink = strpos(get_permalink($compare_page_id), '?');
			
			if($permalink == true){
				$seperator = "&";
			}else{
				$seperator = "?";
			}
		?>
    
    <?php if(wpsc_product_external_link(wpsc_the_product_id()) != '') : ?>
		<?php $action =  wpsc_product_external_link(wpsc_the_product_id()); ?>
    <?php else: ?>
        <?php $action = wpsc_this_page_url(); ?>
    <?php endif; ?>
    
    	<div class="span3 product">

            
			<?php if(wpsc_show_thumbnails()) :?> 
                <figure>
                    <a href="<?php the_permalink(); ?>"><?php echo sh_get_product_thumbnail( wpsc_the_product_id(), 'pro-list-view' );?></a>
                    <div class="overlay">
                        <a href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="zoom"></a>
                        <a href="<?php the_permalink(); ?>" class="link"></a>
                    </div>
                </figure>
            <?php endif;?>
            <div class="detail">
                <span><?php wpsc_the_product_price_display( array( 'price_text'=>'%s', 'output_old_price' => false, 'output_you_save'  => false, ) ); ?></span>
                <?php if( get_option('hide_name_link') == 1 ): ?>
                    <h4><?php the_title(); ?></h4>
                <?php else: ?>
                    <h4><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                <?php endif;?>
                <div class="icon">
            
                  <?php 
                    if(wpsc_product_external_link(wpsc_the_product_id()) != '') $action =  wpsc_product_external_link(wpsc_the_product_id()); 
                    else $action = wpsc_this_page_url(); ?>
                     <form class="product_form"  enctype="multipart/form-data" action="<?php echo $action; ?>" method="post" name="product_<?php echo wpsc_the_product_id(); ?>" id="product_<?php echo wpsc_the_product_id(); ?>" >
                      <?php if(wpsc_product_has_stock() && $t->alloption('hide_addtocart_button') == 0 &&  get_option('addtocart_or_buynow') !='1' && !wpsc_have_variation_groups() ) : ?>
                       
                    <input type="hidden" value="add_to_cart" name="wpsc_ajax_action"/>
                    <input type="hidden" value="<?php echo wpsc_the_product_id(); ?>" name="product_id"/>
                    <button type="submit" name="Buy" class="wpsc_buy_button" id="product_<?php echo wpsc_the_product_id(); ?>_submit_button"><a class="one tooltip"></a></button>
                   
                      <?php elseif( wpsc_have_variation_groups() ): ?>
                        <a href="<?php the_permalink(); ?>" class="one tooltip" title="<?php _e('Add to Cart', SH_NAME)?>"></a> 
                    <?php else: ?>
                        <a href="<?php the_permalink(); ?>" class="one tooltip" title="<?php _e('Buy Now', SH_NAME); ?>"><?php _e('Buy Now', SH_NAME);?></a>
                    <?php endif; ?>
                    <div class="wpsc_loading_animation">
                        <img title="" alt="<?php esc_attr_e( 'Loading', SH_NAME ); ?>" src="<?php echo wpsc_loading_animation_url(); ?>" />
                    </div><!--close wpsc_loading_animation-->
                </form>
                   
                </div>
            </div>
            
        </div>
        
   <?php
}

function fw_pro_page_filters( $query ) 
{
	
	$post_type = sh_set($query->query, 'post_type');

	if( is_page() && $post_type == 'wpsc-product' ){

		if( $per_page = sh_set( $_SESSION, 'bistro_pro_per_page' ) ) $query->posts_per_page = $per_page;
		
		if( $post_sort = sh_set( $_SESSION, 'bistro_pro_sort' ) ){
			switch( $post_sort ){
				case 'price_asc':
					$query->set('meta_key', '_wpsc_price');
					$query->set('orderby', 'meta_value');
					$query->set('order', 'ASC');
				break;
				case 'price_desc':
					$query->set('meta_key', '_wpsc_price');
					$query->set('orderby', 'meta_value');
					$query->set('order', 'DESC');
				break;
				case 'title_asc':
					$query->set('orderby', 'title');
					$query->set('order', 'ASC');
				break;
				case 'title_desc':
					$query->set('orderby', 'title');
					$query->set('order', 'DESC');
				break;
			}
		}
		
	}
	//printr($query);
    return $query;
};

add_filter('pre_get_posts','fw_pro_page_filters');


function sh_get_font_settings( $FontSettings = array(), $StyleBefore = '', $StyleAfter = '' )
{
	$i = 1;
	$settings = _WSH()->option();
	$Style = '';
	foreach( $FontSettings as $k => $v )
	{
		if( $i == 1 || $i == 5 )
		{
			$Style .= ( sh_set( $settings, $k )  ) ? $v.':'.sh_set( $settings, $k ).'px;': '';
		}
		else
		{
			$Style .= ( sh_set( $settings, $k  )  ) ? $v.':'.sh_set( $settings, $k ).';': '';
		}
		$i++;
	}
	return ( !empty( $Style ) ) ? $StyleBefore.$Style.$StyleAfter: '';
}

function sh_product_gallery($postid = '')
{
	global $wpsc_product;
	if($postid) $postid = $postid;
	else $postid = get_the_id();

	$images = get_post_meta( $postid, '_wpsc_product_gallery', true );
	$images_count = count($images);
	if ( $images && $images_count >= 1) :
	?>
    
    	<?php if(isset($_POST['data_id'])){
			$caro= '2';
		}else{
			$caro= '';
		}?>
	
		<div id="carousel-wrapper">
			<div id="carousel<?php echo $caro;?>" class="cool-carousel">
				<?php  
					$i =1;
					foreach ( $images as $image_id ) {
						
					   echo '<span id="image'.$i.'">'.wp_get_attachment_image( $image_id, 'full' ).'</span>';
					   $i++;
					  }
				 ?>
				
			</div>
			<a href="#" class="prev"></a><a href="#" class="next"></a>
		</div>
		<?php if($images_count > 1): ?>
			<div class="bottom">
				<div id="thumbs-wrapper">
					<div id="thumbs">
						<?php 
							$i =1;
							foreach ( $images as $image_id ) {
								
							   echo '<a href="#image'.$i.'">'.wp_get_attachment_image( $image_id, 'pro-single-slide' ).'</a>';
							   $i++;
							}
							
						?>	
					</div>
					<a id="prev" href="#"></a>
					<a id="next" href="#"></a>
				</div>
			</div>
	<?php  
		endif;
	else: 
	?>
    
    <a href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>">
    	<img class="no-image" id="product_image_<?php echo wpsc_the_product_id(); ?>" alt="No Image" title="<?php echo wpsc_the_product_title(); ?>" src="<?php echo WPSC_CORE_THEME_URL; ?>wpsc-images/noimage.png" width="<?php echo get_option('product_image_width'); ?>" height="<?php echo get_option('product_image_height'); ?>" /></a>
        
        
		
	<?php endif;
}

function sh_page_template( $tpl )
{
	$page = get_pages(array('meta_key' => '_wp_page_template','meta_value' => $tpl));
	if($page) return current( (array)$page);
	else return false;
}


function sh_theme_color_scheme()
{
	$dir = get_template_directory();
	include_once($dir.'/includes/thirdparty/lessc.inc.php');
	$styles = _WSH()->option('color_scheme');
	$transient = get_transient( '_sh_color_scheme' );

	$update = ( $styles != $transient ) ? true : false;

	if( !$update ) return;

	set_transient( '_sh_color_scheme', $styles, DAY_IN_SECONDS );
	
	$less = new lessc;

	$less->setVariables(array(
	  "sh_color" => $styles,
	));
	
	// create a new cache object, and compile
	$cache = $less->cachedCompile($dir."/css/color.less");

	file_put_contents($dir.'/css/color.css', $cache["compiled"]);
	
}

/**
 *
 * Use: [wpsc_children_categories parent_cat_id='test']
 *
 */

add_shortcode( 'wpsc_children_categories', 'mt_wpsc_children_categories' );

function mt_get_category_id_by_slug( $slug ) {
	$category_data = get_terms('wpsc_product_category','hide_empty=0', OBJECT, 'display');
	
	foreach((array)$category_data as $category_row) {
		if($category_row->slug === $slug) {
			return $category_row->term_id;
		}
	}
	return -1;
}

function mt_wpsc_children_categories( $atts ) {

  extract(shortcode_atts(array(
    'parent_cat_id'  => ''
    ), $atts));
  	global $wp_query;
	if(isset($wp_query->query_vars['category'])) {
		$parent_cat_id = urldecode($wp_query->query_vars['category']);
	}
	ob_start();
	$categoryId = mt_get_category_id_by_slug($parent_cat_id);
	//wpsc_print_category_url() 
	?>
	<div class="wpsc_categories wpsc_category_grid group">
		<?php wpsc_start_category_query(array('category_group'=> get_option('wpsc_default_category'), 'show_thumbnails'=> 1, 'parent_category_id' => $categoryId)); ?>
			<div class="category-box col-xs-6 col-md-4 col-lg-3">
				<div class="content">
					<a href="[wpsc_sub_catgories_url]" class=" <?php wpsc_print_category_classes_section(); ?>" title="<?php wpsc_print_category_name(); ?>">
						<?php wpsc_print_category_image(220, 220); ?>
						<span>[wpsc_category_name]</span>
					</a>
				</div>
			</div>
		<?php wpsc_end_category_query(); ?>
	</div><!--close wpsc_categories-->
	<?php
	return ob_get_clean();
}

add_filter('query_vars', 'mt_query_vars', 10, 1);

function mt_query_vars($vars) {
    $vars[] = 'category'; 
    return $vars;
}


