<?php
// Flicker Gallery
class SH_Flickr extends WP_Widget
{

	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Flickr', /* Name */__('Flickr Feed',SH_NAME), array( 'description' => __('Fetch the latest feed from Flickr', SH_NAME )) );
	}


	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		wp_enqueue_script(array('flickr-feed'));
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>

		<?php echo $before_title.$title.$after_title; ?>
        <?php $limit = ($instance['number']) ? $instance['number'] : 8; ?>
         
        <div class="flcker clearfix">
            
    
                <script type="text/javascript">
					   jQuery(document).ready(function($) {
							$('.flcker').jflickrfeed({
								 limit: <?php echo $limit;?> ,
								 qstrings: {id: '<?php echo $instance['flickr_id']; ?>'},
								 itemTemplate: '<a href="{{link}}"><img src="{{image_s}}" alt="{{title}}" /></a>'
							});
					   });
				</script>
            
		</div>
	 	
		<?php echo $after_widget;
	}


	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
	 $instance = $old_instance;
	
	
	 $instance['title'] = strip_tags($new_instance['title']);
	 $instance['flickr_id'] = $new_instance['flickr_id'];
	 $instance['number'] = $new_instance['number'];
	
	
	 return $instance;
	}


	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Recent From', SH_NAME);
		
		$flickr_id = ($instance) ? esc_attr($instance['flickr_id']) : '';
		
		$number = ( $instance ) ? esc_attr($instance['number']) : 8;?>
		   
        <p>
           <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
           <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
           <label for="<?php echo $this->get_field_id('flickr_id'); ?>"><?php _e('Flickr ID:', SH_NAME); ?></label>
           <input class="widefat" id="<?php echo $this->get_field_id('flickr_id'); ?>" name="<?php echo $this->get_field_name('flickr_id'); ?>" type="text" value="<?php echo esc_attr($flickr_id); ?>" />
        </p>
        <p>
           <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of Tweets:', SH_NAME); ?></label>
           <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" />
        </p>
		   
				   
	 <?php 
	}

}



// Happy Clients 


class SH_Clientslider extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Clientslider', /* Name */__('Aplus Clients Slider',SH_NAME), array( 'description' => __('Client Slider to show', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		$number = $instance['number'];
		$text = $instance['text'];
		
		$args = array('post_type' => 'aplus_clients' , 'showposts'=> $number);
		$clients = get_posts($args);
		$clients_chunks = array_chunk($clients ,3 );
		
		echo $before_widget;
		
		?>
        <?php echo $before_title.$title.$after_title ; ?>
        <p><i class="icon-quote-left"></i> <?php echo $text ; ?> <i class="icon-quote-right"></i></p>
        <div class="clients flexslider flex-direction-nav">
        <ul class="slides">
         <?php foreach($clients_chunks as $client_chunk) : ?>
            <li>
                <?php foreach($client_chunk as $client) : ?>
                <div class="span4">
                    <?php get_the_post_thumbnail(sh_set($client , 'ID') , '');?>
                </div>
                <?php endforeach; ?>
            </li>
          <?php endforeach ; ?> 
        </ul>
        </div>
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		$instance['text'] = $new_instance['text'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Clients', SH_NAME);
		$number = ($instance) ? esc_attr($instance['number']) : '';
		$text = ($instance) ? esc_attr($instance['text']) : '';
		?>
        
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of Slides:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" />
        </p>
         <p>
            <label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text:', SH_NAME); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" ><?php echo $text; ?></textarea>
        </p>
        
                
		<?php 
	}
}

// Subscribe to our mailing list
class SH_partner extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_partners', /* Name */__('Aplus partners slider',SH_NAME), array( 'description' => __('Show the slider of partners custom post type', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
        
        <?php echo $before_title.$title.$after_title; ?>
        <?php wp_enqueue_script(array('jquery_flexslider_min')); ?>
        <p><i class="icon-quote-left"></i><?php echo $instance['text']; ?><i class="icon-quote-right"></i></p>
        
        <?php $posts = get_posts('post_type=aplus_partner&posts_per_page='.$instance['number']); 
		
		if( !$posts ) return;
		
		$chunk = array_chunk($posts, 3 );?>
        
        <?php if( $chunk ): ?>
        
        <div class="clients flexslider flex-direction-nav">
            <ul class="slides">
                <?php foreach( $chunk as $ch ): ?>
                <li>
                    <?php foreach( $ch as $c ): ?>
                        <div class="span4">
                            <?php $meta = get_post_meta( $c->ID, 'sh_partner_meta', true ); ?>

								<?php echo get_the_post_thumbnail( $c->ID, 'full' ); ?>

                        </div>
                    <?php endforeach; ?>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
        
        <?php endif; ?>
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['text'] = $new_instance['text'];
		$instance['number'] = $new_instance['number'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Happy Client', SH_NAME);
		$text = ($instance) ? esc_attr($instance['text']) : '';
		$number = ($instance) ? esc_attr($instance['number']) : 6;
?>        
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text:', SH_NAME); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" ><?php echo $text; ?></textarea>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number:', SH_NAME); ?></label>
            <input class="" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" />
        </p>
       
		<?php 
	}
}


class SH_twitter extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */ 'SH_twitter', /* Name */ 'Shop Twitter', array( 'description' => 'Twitter tweets' ) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{	
		extract( $args );
		$title = @apply_filters( 'widget_title', $instance['twitter_title'] );
		echo $before_widget;
				
		FW_Twitter(array('Template'=>'p','screen_name'=>kvalue($instance, 'twitter_id'), 'count'=>kvalue($instance, 'tweets_num'), 'selector'=>'.tweets'));?>
        
        <?php if($title) echo $before_title.$title.$after_title; ?>
        
       	<div class="tweets"></div>
			
		<?php
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;
		$instance['twitter_title'] = strip_tags($new_instance['twitter_title']);
		$instance['twitter_id'] = strip_tags($new_instance['twitter_id']);
		$instance['tweets_num'] = strip_tags($new_instance['tweets_num']);

		return $instance;
	}

	/** @see WP_Widget::form */
	function form( $instance )
	{
		if ( $instance )
		{
			$twitter_title = esc_attr( $instance[ 'twitter_title' ] );
			$twitter_id = esc_attr($instance['twitter_id']);
			$tweets_num = esc_attr($instance['tweets_num']);

		}
		else
		{
			$twitter_title = _e( 'Latest Tweets', SH_NAME );
			$twitter_id = 'wordpress';
			$tweets_num = 3;

			$follow_label = '';
		}

	?>    
            <label for="<?php echo $this->get_field_id('twitter_title'); ?>"><?php _e('Title:', SH_NAME); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('twitter_title'); ?>" name="<?php echo $this->get_field_name('twitter_title'); ?>" type="text" value="<?php echo esc_attr($twitter_title); ?>" />
            <label for="<?php echo $this->get_field_id('twitter_id'); ?>"><?php _e('Twitter ID:', SH_NAME); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('twitter_id'); ?>" name="<?php echo $this->get_field_name('twitter_id'); ?>" type="text" value="<?php echo esc_attr($twitter_id); ?>" />

            <label for="<?php echo $this->get_field_id('tweets_num'); ?>"><?php _e('Number of Tweets:', SH_NAME); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('tweets_num'); ?>" name="<?php echo $this->get_field_name('tweets_num'); ?>" type="text" value="<?php echo esc_attr($tweets_num); ?>" />

		</p>
	<?php 
	}
}

class SH_contact_us extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */ 'FW_contact_us', /* Name */ 'Maxshop Contact Us', array( 'description' => 'Show the contact us form' ) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		echo $before_widget;
		
		$title = @apply_filters( 'widget_title', $instance['title'] );?>
        <?php if($title) echo $before_title.$title.$after_title; ?>
        
        <ul>
            <li><a href="mailto:<?php echo $instance['email'];?>"><?php echo $instance['email'];?></a></li>
            <li><?php echo $instance['phone'];?></li>
            <li><?php echo $instance['address'];?></li>
        </ul>
	  
		<?php	echo $after_widget;
	}
	
	/** @see WP_Widget::update */
	function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;
		
		$instance['title']		= strip_tags($new_instance['title']);
		$instance['address']	= strip_tags($new_instance['address']);
		$instance['phone']		= strip_tags($new_instance['phone']);
		$instance['email']		= strip_tags($new_instance['email']);
		return $instance;
	}

	/** @see WP_Widget::form */
	function form( $instance )
	{
		if ( $instance )
		{
			$title 				= esc_attr($instance['title']);
			$address 			= esc_attr($instance['address']);
			$phone 				= esc_attr($instance['phone']);
			$email 				= esc_attr($instance['email']);
		}
		else
		{
			$title 				= __( 'Contact Us', THEME_NAME );
			$address 			= "";
			$phone 				= "";
			$email 				= "";
		}?>
            
           
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', THEME_NAME); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            <label for="<?php echo $this->get_field_id('address'); ?>"><?php _e('Address:', THEME_NAME); ?></label> 
            <textarea class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>" ><?php echo $address; ?></textarea>
            
            <label for="<?php echo $this->get_field_id('phone'); ?>"><?php _e('Contact No:', THEME_NAME); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo esc_attr($phone); ?>" />
            
            <label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email ID:', THEME_NAME); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo esc_attr($email); ?>" />
            
		<?php 
	}
}// contact us

//add_action('widgets_init', create_function('', 'register_widget("SH_twitter");'));