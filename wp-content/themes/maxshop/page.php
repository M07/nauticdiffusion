<?php wp_reset_query();
get_header(); ?>
<?php global $post;

if( !is_tax( 'wpsc_product_category' ) ) {
$meta_settings = get_post_meta($post->ID, 'sh_post_meta', true);
$meta_setting = sh_set( $meta_settings, 'sh_post_options' );  //printr($meta_setting);
$page_banner = sh_set( sh_set($meta_setting, 0), 'page_banner');
$layout = sh_set( sh_set($meta_setting, 0), 'layout');
$sidebar = sh_set( sh_set($meta_setting, 0), 'sidebar', 'default-sidebar');

} else {
	$page_banner = '';
	$layout = 'right';
	$sidebar = 'product-sidebar';
}?>


<!-- BAR -->
<div class="bar-wrap">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="title-bar" <?php echo ($page_banner)? 'style="background-image:url('.$page_banner.');"' : ''; ?>>
                    <h1><?php if( is_tax('wpsc_product_category') ) single_term_title();
						else echo get_the_title(); ?>
					</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BAR -->

<!-- PRODUCT-OFFER -->
<div class="blog_wrap">
    <div class="container">
        
        	<?php if( function_exists('is_products_page') && is_products_page()):?>
            	<?php 

					$_SESSION['bistro_pro_view'] = isset($_SESSION['bistro_pro_view']) ? $_SESSION['bistro_pro_view'] : 'list';
					 if( sh_set( $_GET, 'pro_view' ) ){
						 $_SESSION['bistro_pro_view'] = (sh_set( $_GET, 'pro_view' )) == 'grid' ? 'grid' : 'list';
					 }
					 $default_view = $_SESSION['bistro_pro_view'];
				
				?>
                <div class="row">
        			<div class="span12">
            			<?php get_template_part( 'includes/modules/filteration' ); ?>
                	</div>
                </div>
            <?php endif;?>
            <div class="row">
        	
			<?php if($sidebar && $layout == 'left'):?>
            	<div class="span3">
                    <div id="sidebar2">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
            	</div>
            <?php endif;?>
			
            <div class="<?php echo ($layout == 'full' || $layout == '') ? 'span12' : 'span9';?> <?php echo (function_exists('is_products_page') && is_products_page() && isset($_GET['pro_view']) && $_GET['pro_view'] == 'grid') ? 'product-grid' : 'blog'; ?>">
			   <?php while( have_posts() ): the_post(); ?>
                    
                    <div class="post-content clearfix">
						<?php the_content();?>
                    </div>
                    
               <?php endwhile;?>
        	</div>
            
             <?php if($sidebar && $layout == 'right'):?>
            	<div class="span3">
                    <div id="sidebar2">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
            	</div>
            <?php endif;?>

            
    
		</div>
	</div>
</div>

<?php get_footer(); ?>


