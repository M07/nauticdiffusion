<?php get_header(); ?>
<?php $t = &$GLOBALS['_sh_base']; 
global $post;
$meta_settings = get_post_meta($post->ID, 'sh_post_meta', true);
$meta_setting = sh_set( $meta_settings, 'sh_post_options' ); 
$page_banner = sh_set( sh_set($meta_setting, 0), 'page_banner');

// Setup globals
// @todo: Get these out of template
global $wp_query, $wpsc_variations, $wpsc_custom_meta;?>

<!-- BAR -->
<div class="bar-wrap">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="title-bar" <?php echo ($page_banner)? 'style="background-image:url('.$page_banner.');"' : ''; ?>>
                    <h1><?php echo get_the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BAR -->  



<!-- PRODUCT-OFFER -->
<div class="blog_wrap">
    <div class="container">
        <div class="row">
        	<div class="span9">
				<?php while( have_posts() ): the_post(); 
                    $wpsc_variations = new wpsc_variations( get_the_id() );
                    $wpsc_custom_meta = new wpsc_custom_meta( get_the_id() );?>
                    <!-- Center -->
                    <div class="row">
                    	<div class="single clearfix">
                        <div class="wrap span5">
                        	
                        	<?php sh_product_gallery();?>
                               
						</div>

                        <div class="span4">
                        	<?php $action = ''; ?>
                            <form class="product_form" enctype="multipart/form-data" action="<?php echo $action; ?>" method="post" name="product_<?php echo wpsc_the_product_id(); ?>" id="product_<?php echo wpsc_the_product_id(); ?>" >	
                                <div class="product-detail">
                                    <h4><?php the_title(); ?></h4>
                                    <span>
                                        <?php if(wpsc_product_is_donation()) : ?>
                                            <label for="donation_price_<?php echo wpsc_the_product_id(); ?>"><?php _e('Donation', SH_NAME); ?>: </label>
                                            <input type="text" class="input-small" id="donation_price_<?php echo wpsc_the_product_id(); ?>" name="donation_price" value="<?php echo wpsc_calculate_price(wpsc_the_product_id()); ?>" size="6" />
                                        <?php else : ?>
                                            <?php wpsc_the_product_price_display(); ?>
                                        
                                            <?php if(wpsc_product_has_multicurrency()) : ?>
                                                <?php echo wpsc_display_product_multicurrency(); ?>
                                            <?php endif; ?>
                                        
                                            <?php if(wpsc_show_pnp()) : ?>
                                                <span class="price-tax"><?php _e('Shipping', SH_NAME); ?>: <?php echo wpsc_product_postage_and_packaging(); ?></span>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </span>
                                    <?php do_action('wpsc_product_before_description', wpsc_the_product_id(), $wp_query->post);
                                    do_action('wpsc_product_addons', wpsc_the_product_id()); ?>
                                    <?php echo wpsc_the_product_description(); ?>
                                     <?php if( wpsc_show_stock_availability() ): ?>
                                        <?php if(wpsc_product_has_stock()) : ?>
                                            <div id="stock_display_<?php echo wpsc_the_product_id(); ?>" class="in_stock"><?php _e('Product in stock', SH_NAME); ?></div>
                                        <?php else: ?>
                                            <div id="stock_display_<?php echo wpsc_the_product_id(); ?>" class="out_of_stock"><?php _e('Product not in stock', SH_NAME); ?></div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    
                                     <?php if(wpsc_product_external_link(wpsc_the_product_id()) != '') : ?>
                                        <?php $action =  wpsc_product_external_link(wpsc_the_product_id()); ?>
                                    <?php else: ?>
                                        <?php $action = wpsc_this_page_url(); ?>
                                    <?php endif; ?>
                                     <div class="wpsc_loading_animation">
                                                    <img title="" alt="<?php esc_attr_e( 'Loading', SH_NAME ); ?>" src="<?php echo wpsc_loading_animation_url(); ?>" />
                                    </div>
                                
                                </div>
                                
                               <input type="hidden" value="add_to_cart" name="wpsc_ajax_action"/>
                               <input type="hidden" value="<?php echo wpsc_the_product_id(); ?>" name="product_id"/>
                                <div class="product-type clearfix">
                                 	
                                    <?php /** the variation group HTML and loop */?>
                                    <?php if (wpsc_have_variation_groups()) { ?>
                            
                                        <div class="wpsc_variation_forms">
                                        
                                        <?php while (wpsc_have_variation_groups()) : wpsc_the_variation_group(); ?>
                                           
                                                <label for="<?php echo wpsc_vargrp_form_id(); ?>"><?php echo wpsc_the_vargrp_name(); ?>:</label>
                                            <?php /** the variation HTML and loop */?>
                                            <select class="wpsc_select_variation noselectbox" name="variation[<?php echo wpsc_vargrp_id(); ?>]" id="<?php echo wpsc_vargrp_form_id(); ?>">
                                            <?php while (wpsc_have_variations()) : wpsc_the_variation(); ?>
                                                <option value="<?php echo wpsc_the_variation_id(); ?>" <?php echo wpsc_the_variation_out_of_stock(); ?>><?php echo wpsc_the_variation_name(); ?></option>
                                            <?php endwhile; ?>
                                            </select>
                                           
                                        <?php endwhile; ?>
                                        
                                        </div>
                            
                                    <?php } ?>
                                    <?php if(wpsc_has_multi_adding()): ?>
                                        <div>
                                                <label><?php _e('Quantity', SH_NAME)?></label>
                                                <input type="hidden" name="key" value="<?php echo wpsc_the_cart_item_key(); ?>"/>
                                                <input type="hidden" name="wpsc_update_quantity" value="true" />
                                                <input type="text" maxlength="3" size="2" value="1" class="input-text qty text" id="wpsc_quantity_update_<?php echo wpsc_the_product_id(); ?>" name="wpsc_quantity_update">
                                              
                                        </div>
                                    <?php endif; ?> 
                                    <?php do_action ( 'wpsc_product_form_fields_begin' ); ?>
                                </div>
                                <div class="buttons">
                                    <?php if(wpsc_product_has_stock() && $t->alloption('hide_addtocart_button') == 0 &&  $t->alloption('addtocart_or_buynow') !='1') : ?>
                                    <div class="wpsc-add-to-cart-button">
                                        <input type="hidden" value="add_to_cart" name="wpsc_ajax_action"/>
                                        <input type="hidden" value="<?php echo wpsc_the_product_id(); ?>" name="product_id"/>
                                        <button title="<?php _e('Add To Cart', SH_NAME); ?>" class="wpsc_buy_button btn-icon btn" id="product_<?php echo wpsc_the_product_id(); ?>_submit_button" > <span class="cart big-button"><?php _e('Add to Cart', SH_NAME); ?></span></button>                            
                                    </div>   
                                    
                                    <?php else: ?>
                                    <a href="<?php the_permalink(); ?>" title="<?php _e('Buy Now', SH_NAME); ?>"><?php _e('Buy Now', SH_NAME);?></a>
                                    <?php endif; ?>
                                    <a href="javascript:void(0);" class="wish big-button add_to_wishlist" data-id="<?php echo get_the_id(); ?>"><?php _e('Add to Wishlist', SH_NAME);?></a>
                                     <?php $compare_template = sh_page_template('tpl-compare.php');
				
				
										if($compare_template) $compare_page_id = $compare_template->ID; 
										else $compare_page_id = '';
										
										$permalink = strpos(get_permalink($compare_page_id), '?');
										
										if($permalink == true){
											$seperator = "&";
										}else{
											$seperator = "?";
										}
									?>
                                    <a href="<?php echo get_permalink($compare_page_id).$seperator;?>product_id=<?php echo get_the_id();?>" class="compare big-button"><?php _e('Add to Compare', SH_NAME)?></a>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    </div>
                    
                <?php endwhile;?>
                
                <div id="product_tabs">
                    <ul class="clearfix">
                        <li><a href="#tabs-1"><?php _e('Product Description', SH_NAME);?></a></li>
                        <?php if ($wpsc_custom_meta->have_custom_meta()) : ?>
                    		<?php $i= 2;?>
                            <?php while ( $wpsc_custom_meta->have_custom_meta() ) : $wpsc_custom_meta->the_custom_meta(); ?>
                                <?php if (stripos(wpsc_custom_meta_name(),'g:') !== FALSE || wpsc_custom_meta_name() == 'sh_post_meta' || wpsc_custom_meta_name() == 'slide_template' ) continue; ?>
                                <li><a href="#tabs-<?php echo $i++;?>"><?php echo wpsc_custom_meta_name(); ?></a></li>
                            <?php endwhile; ?>
                        
                     <?php endif; ?>
                    </ul>
                    <!--TABS-->
                    <div id="tabs-1" class="tab" >
                        <p><?php echo wpsc_the_product_description(); ?></p>
                    </div>
                                    
					<?php $wpsc_custom_meta = new wpsc_custom_meta( get_the_id() );
                      if ($wpsc_custom_meta->have_custom_meta()) :?>
                                <?php $i= 2;?>
                            <?php while ( $wpsc_custom_meta->have_custom_meta() ) : $wpsc_custom_meta->the_custom_meta(); ?>
                                <?php if (stripos(wpsc_custom_meta_name(),'g:') !== FALSE || wpsc_custom_meta_name() == 'sh_post_meta' || wpsc_custom_meta_name() == 'slide_template' ) continue; ?>
                                <div id="tabs-<?php echo $i++;?>" class="tab" >
                        
                                    <p><?php echo wpsc_custom_meta_value(); ?></p>
                                </div>
                            <?php endwhile; ?>
                        
                     <?php endif; ?>

				</div>
    		</div>
    		<div class="span3">
            	<div id="sidebar2">
					<?php dynamic_sidebar( sh_set(sh_set($meta_setting,0), 'sidebar', 'default-sidebar') ); ?>
                </div>
            </div>
		</div>
	</div>
</div>

<?php do_action( 'wpsc_theme_footer' ); ?>
<?php get_footer(); ?>