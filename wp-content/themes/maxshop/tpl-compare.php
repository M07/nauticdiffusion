<?php 
/* Template Name: Compare */
global $wpsc_variations;
get_header(); 
$product_id = sh_set($_GET , 'product_id') ;
$attributes = maybe_unserialize( get_post_meta( $product_id, '_product_attributes', true ) );
$product = get_post($product_id);//printr($product);
$product_gallery = explode(',' , get_post_meta($product_id , '_product_image_gallery' , true));
$meta_settings = get_post_meta($product_id, 'sh_post_meta', true);
$meta_setting = sh_set( $meta_settings, 'sh_post_options' ); 
$page_banner = sh_set( sh_set($meta_setting, 0), 'page_banner');
$price = get_post_meta( $product_id, '_wpsc_price', true );
$wpsc_variations_groups = new wpsc_variations( $product_id );//printr($wpsc_variations_groups);
?>

<!-- BAR -->
<div class="bar-wrap">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="title-bar" <?php echo ($page_banner)? 'style="background-image:url('.$page_banner.');"' : ''; ?>>
                    <h1><?php echo sh_set($product, 'post_title'); ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- PRODUCT-OFFER -->
<div class="blog_wrap">
    <div class="container">
        <div class="row">
        	<div class="span6">
            	<div class="row">
            		<div class="single clearfix">
                        <div class="wrap span4">
                        	
									<?php sh_product_gallery($product_id);?>
                            
                            <div class="product-content">
                            	<h3><?php echo _e('Description', SH_NAME);?></h3>
                            	<p><?php echo sh_set($product, 'post_content'); ?></p>
                            </div>
						</div>
                        <div class="span2">
                        	<span class="price-tax"><?php _e('Price', SH_NAME); ?>: <?php echo $price; ?></span>
                            <span class="variations">
                            	<h3><?php _e('Variations', SH_NAME);?></h3>
                            	<?php foreach($wpsc_variations_groups->variation_groups as $wpsc_variations_group): ?>
                                        <h4><?php echo sh_set($wpsc_variations_group, 'name'); ?>:</h4>
                                 
                                        <?php $variations = $wpsc_variations_groups->all_associated_variations;
                         
                                        $term_id = sh_set($wpsc_variations_group, 'term_id');
                                        $terms = $variations[$term_id];
                                        $i=0;
                                        foreach($terms as $term):
											if($i==0) echo '';
											else echo sh_set($term, 'name').', ';
											$i++;		 
                                        endforeach;
                                        ?>
                                <?php endforeach; ?>
                            
                            </span>
                        </div>

                    </div>
                </div>
            </div>
            <div class="span6">
            	<div class="compare-with">
					<form action="" method="post" name="compare_search_form">
						<label><?php _e("Compare with" , SH_NAME); ?></label>
						<input type="text" placeholder="<?php _e('Type product name', SH_NAME); ?>" name="search_query" />
						<input type="submit" class="submit" value="<?php _e("Search" , SH_NAME); ?>" />
						
						<div class="product-compare-list"></div>
					</form>
				</div>
				
				<div class="row" id="product-compare-with">
					
				</div>
            </div>
        </div>
    </div>
</div>
<!-- BAR -->  

<?php get_footer(); ?>