<?php get_header();?>

<!-- BAR -->
<div class="bar-wrap">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="title-bar" <?php echo ($header_image)? 'style="background-image:url('.$header_image.');"' : ''; ?>>
                    <h1>
						<?php wp_title(''); ?>
            		</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- PRODUCT-OFFER -->
<div class="blog_wrap">
    
    <div class="container">
        <div class="row">
        	<div class="span9 blog">
				<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', SH_NAME ); ?></p>
                <?php get_search_form(); ?>
        	</div>
        
			<div class="span3">
            	<div id="sidebar2">
        			<?php dynamic_sidebar( 'blog' ); ?>
                </div>
            </div>
		</div>
    </div>
    
</div>

<?php get_footer();?>