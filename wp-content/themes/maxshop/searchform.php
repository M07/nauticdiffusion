<div>
    <form id="searchbox" action="<?php echo home_url(); ?>" method="get" style="margin-top:0">
        <p>
            <input type="text" name="s" value="<?php echo get_search_query(); ?>" placeholder="<?php _e('Type and hit enter', SH_NAME); ?>">
        </p>
    </form>
</div>
