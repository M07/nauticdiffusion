<?php
/**
 * The Account > Purchase History template.
 *
 * Displays the user's order history.
 *
 * @package WPSC
 * @since WPSC 3.8.10
 */

global $col_count; ?>

<?php if ( wpsc_has_purchases() ) : ?>

	<table class="logdisplay table data-table">

	<?php if ( wpsc_has_purchases_this_month() ) : ?>
		<thead>
			<tr class="toprow header first last">
				<th class="status"><?php _e( 'Status', SH_NAME ); ?></th>
				<th class="date"><?php _e( 'Date', SH_NAME ); ?></th>
				<th class="price" colspan="2"><?php _e( 'Price', SH_NAME ); ?></th>

				<?php if ( get_option( 'payment_method' ) == 2 ) : ?>

					<th class="payment_method"><?php _e( 'Payment Method', SH_NAME ); ?></th>

				<?php endif; ?>

			</tr>
		</thead>
        <tbody>
			<?php wpsc_user_purchases(); ?>
		</tbody>
	<?php else : ?>

			<tr>
				<td colspan="<?php echo $col_count; ?>">

					<?php _e( 'No transactions for this month.', SH_NAME ); ?>

				</td>
			</tr>

	<?php endif; ?>

	</table>

<?php else : ?>

	<table>
		<tr>
			<td><?php _e( 'There have not been any purchases yet.', SH_NAME ); ?></td>
		</tr>
	</table>

<?php endif; ?>