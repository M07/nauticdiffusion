<?php

define('SH_NAME', 'maxshop');
define('SH_VERSION', '1.0');
define('SH_ROOT', get_template_directory().'/');
define('THEME_URL', get_template_directory_uri().'/');
define('THEME_PATH', get_template_directory());


include_once('includes/loader.php');
include_once('theme_functions.php');
add_action('after_setup_theme', 'sh_theme_setup');

function sh_theme_setup()
{
	global $wp_version;

	load_theme_textdomain(SH_NAME, get_template_directory() . '/languages');

	//ADD THUMBNAIL SUPPORT
	add_theme_support('post-thumbnails');
	add_theme_support('menus'); //Add menu support
	add_theme_support('automatic-feed-links'); //Enables post and comment RSS feed links to head.
	add_theme_support('widgets'); //Add widgets and sidebar support
	
	/** Register wp_nav_menus */
	if(function_exists('register_nav_menu'))
	{
		register_nav_menus(
			array(
				/** Register Main Menu location header */
				'main_menu' => __('Main Menu', SH_NAME),
			)
		);
	}
	
	if ( ! isset( $content_width ) ) $content_width = 960;
	
	add_image_size( 'blog-listing', 869, 334, true );
	add_image_size('pro-list-view', 249, 180, true);
	add_image_size('pro-single-view', 470, 261, true);
	add_image_size('pro-single-slide', 270, 180, true);
	add_image_size('shopping-cart', 97, 64, true);
	add_image_size('large-slide', 870, 373, true);
	add_image_size('small-slide', 270, 196, true);
		
}

function sh_theme_add_editor_styles() {
	add_editor_style( 'css/custom-editor-style.css' );
}
add_action( 'init', 'sh_theme_add_editor_styles' );

function sh_widget_init()
{
	global $wp_registered_sidebars;
	
	register_widget('SH_Flickr');
	register_widget("SH_twitter");
	register_widget("SH_contact_us");
	
	register_sidebar( array(
		'name' => __('Default Sidebar', SH_NAME ),
		'id' => 'default-sidebar',
		'description' => __('Default sidebar is displayed on general pages', SH_NAME ),
		'before_widget' => '<div id="%1$s" class="widget clearfix %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );

	register_sidebar( array(
		'name' => __('Blog Sidebar', SH_NAME ),
		'id' => 'blog',
		'description' => __('Blog sidebar is displayed on blog pages like category, author, archive etc', SH_NAME ),
		'before_widget' => '<div id="%1$s" class="widget clearfix %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );
	
	register_sidebar( array(
		'name' => __('Product Sidebar', SH_NAME ),
		'id' => 'product-sidebar',
		'description' => __('Product sidebar is displayed on wp-ecommerce plugin\'s products pages', SH_NAME ),
		'before_widget' => '<div id="%1$s" class="widget clearfix %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );
	
	register_sidebar( array(
		'name' => __('Footer Sidebar', SH_NAME ),
		'id' => 'footer-sidebar',
		'description' => __('footer sidebar is displayed footer area', SH_NAME ),
		'before_widget' => '<div class="span6"><div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div></div>",
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );

	update_option( 'wp_registered_sidebars', $wp_registered_sidebars );
}

add_action('widgets_init', 'sh_widget_init' );

function sh_product_listing( $atts ) {
     extract( shortcode_atts( array(
	      'title' => 'Chairs',
	      'num' => 3,
		  'cat' => 2
     ), $atts ) );
	 
	 $output = '';
	 
	 
	 
	 $output .='<div class="our-product">';
	 $output .=	($title) ? '<h3>'.$title.'</h3>' : '';
	 $arg = array('post_type'=>'wpsc-product', 'showposts'=>$num);
	// printr($arg);
	 $query = query_posts($arg);//printr($query);
	 	global $post;
	ob_start();
	
	while(have_posts()):the_post();?>
	
    				<div class="clearfix">
						<a href="<?php the_permalink()?>"><?php the_post_thumbnail($post->ID, 'shopping-cart');?></a>
						<h4><?php echo character_limiter(get_the_title(), 10, '')?></h4>
						<span><?php wpsc_the_product_price_display( array( 'price_text'=>'%s', 'output_old_price' => false, 'output_you_save'  => false, ) )?></span>
					</div>
	<?php endwhile;
	wp_reset_query();
	$output .=ob_get_contents();
	$output .='</div>';
	ob_clean();
	 
	 
     return $output;
}

function sh_categories_listing($atts)
{
	extract( shortcode_atts( array(
	      'position' => 'top',
	      'num' => 12,
		  'cat' => 0,
		  'cols' => 2
     ), $atts ) );
	 
	 $output ='';
	 ob_start();
	 $paren_term = get_term($cat, 'wpsc_product_category'); //printr($paren_term);
	 $categories_list = get_terms( 'wpsc_product_category', array('hide_empty'=>0, 'child_of' => $cat, 'number' => $num) );
	 $count = count($categories_list);
	 $columns = $count/$cols;
	 $columns = explode('.', $columns);
	 ?>
     
     <?php $category_image = wpsc_get_categorymeta( $cat, 'image' );
	 		$category_image = WPSC_CATEGORY_URL . $category_image;
	 ?>
    <?php if($position == 'top'):?>
    <figure>
        <a href="<?php echo get_term_link( $paren_term->slug, 'wpsc_product_category' );?>"><img src="<?php echo $category_image;?>" alt=""/></a>
    </figure>
    <?php endif;?>
    
	
	<div class="links">
        <h3><?php echo $paren_term->name;?></h3>
        <p>
        <?php $i=1;?>
        <?php foreach($categories_list as $catgory):?>
        	 <a href="<?php echo get_term_link( $catgory, 'wpsc_product_category');?>"><?php echo $catgory->name;?></a>
             <?php if($i%$columns[0]==0):?>
             </p><p>
             <?php endif;?>
         	 <?php $i++;?>
        <?php endforeach;?>
        </p>
    </div>
	
    <?php if($position == 'left' || $position == 'right'):?>
    	 <figure>
            <a href="<?php echo get_term_link( $paren_term->slug, 'wpsc_product_category' );?>"><img src="<?php echo $category_image;?>" alt=""/></a>
        </figure>
    <?php endif;?>
     
<?php
	$output .= ob_get_contents();
	ob_clean(); 	 
	return $output;
}

add_shortcode( 'SH_products', 'sh_product_listing' );
add_shortcode( 'SH_categories', 'sh_categories_listing' );



class SH_Enqueue
{
	var $opt;
	
	function __construct()
	{
		$this->opt = _WSH()->option();
		add_action( 'wp_enqueue_scripts', array( $this, 'sh_enqueue_scripts' ) );
		
		add_action( 'wp_head', array( $this, 'wp_head' ) );
		
		add_action( 'wp_footer', array( $this, 'wp_footer' ) );
	}

	function sh_enqueue_scripts()
	{
		$styles = array('family-open-sans'=>'http://fonts.googleapis.com/css?family=Open+Sans:300,700,600,800',
						'family-Oswald'=>'http://fonts.googleapis.com/css?family=Oswald:400,700',
						'family-Quattrocento'=>'http://fonts.googleapis.com/css?family=Quattrocento:400,700',
						'prettyPhoto'=>'css/prettyPhoto.css', 
						'tooltipster'=>'css/tooltipster.css',
						'ie'=>'css/ie.css', 
						'bootstrap'=>'css/bootstrap.css',
						'main-style' => 'style.css', 
						'responsive'=>'css/responsive.css',
						'main_color'	=> 'css/color.css',
						'jquery_bxslider'	=> 'css/jquery.bxslider.css');
								
		foreach( $styles as $name => $style )
		{
			if(strstr($style, 'http')) wp_register_style( $name, $style);
			else wp_register_style( $name, THEME_URL.$style);
			
			wp_enqueue_style($name);
		}
		
		sh_demo_color_picker();
		$scripts = array('jquery-cycle-all'=>'jquery.cycle.all.js', 'modernizr-custom'=>'modernizr.custom.17475.js','jquery-elastislide'=>'jquery.elastislide.js','jquery-carouFredSel'=>'jquery.carouFredSel-6.0.4-packed.js','jquery-selectBox'=>'jquery.selectBox.js','jquery-tooltipster'=>'jquery.tooltipster.min.js', 'jquery-prettyPhoto'=>'jquery.prettyPhoto.js','flickr-feed'=>'jflickrfeed.min.js','custom-tweets'=>'custom_tweets.js','custom-scripts'=>'custom.js','theme-script'=>'theme-script.js','nautic'=>'nautic.js','jquery.bxslider'=>'jquery.bxslider.min.js','mt.bxslider'=>'mt.bxslider.js');			

		foreach( $scripts as $name => $js )
		{
			wp_register_script( $name, THEME_URL.'js/'.$js, '', SH_VERSION, true);
		}
		
		wp_enqueue_script(array('jquery', 'modernizr-custom', 'jquery-selectBox','theme-script','custom-scripts','jquery.bxslider','mt.bxslider'));
		
		$checkoutpage = get_page_by_title( 'Checkout' );
		$products_page = get_page_by_title( 'Products Page' );
		if( is_singular('wpsc-product') ) wp_enqueue_script( array( 'jquery-ui-tabs', 'jquery-elastislide', 'jquery-prettyPhoto', 'jquery-carouFredSel'));
		if( is_singular() ) wp_enqueue_script('comment-reply');
		wp_enqueue_script( array('jquery-ui-accordion', 'nautic') );
		if( is_tax( 'wpsc_product_category' ) ) wp_enqueue_script( array( 'jquery-tooltipster','jquery-prettyPhoto', ) );
		
		if( $products_page && is_page( $products_page ) ) wp_enqueue_script( array( 'jquery-tooltipster','jquery-prettyPhoto', ) );
	}
	
	function wp_head()
	{
		
		$opt = $this->opt;
		echo '<script type="text/javascript"> if( ajaxurl === undefined ) var ajaxurl = "'.admin_url('admin-ajax.php').'";</script>';?>
		<style type="text/css">
			<?php
			if( sh_set( $opt, 'body_custom_font') )
			echo sh_get_font_settings( array( 'body_font_size' => 'font-size', 'body_font_family' => 'font-family', 'body_font_style' => 'font-style', 'body_font_color' => 'color', 'body_line_height' => 'line-height' ), 'body, p {', '}' );
			
			if( sh_set( $opt, 'use_custom_font' ) )
			{
				echo sh_get_font_settings( array( 'h1_font_size' => 'font-size', 'h1_font_family' => 'font-family', 'h1_font_style' => 'font-style', 'h1_font_color' => 'color', 'h1_line_height' => 'line-height' ), 'h1 {', '}' );
				echo sh_get_font_settings( array( 'h2_font_size' => 'font-size', 'h2_font_family' => 'font-family', 'h2_font_style' => 'font-style', 'h2_font_color' => 'color', 'h2_line_height' => 'line-height' ), 'h2 {', '}' );
				echo sh_get_font_settings( array( 'h3_font_size' => 'font-size', 'h3_font_family' => 'font-family', 'h3_font_style' => 'font-style', 'h3_font_color' => 'color', 'h3_line_height' => 'line-height' ), 'h3 {', '}' );
				echo sh_get_font_settings( array( 'h4_font_size' => 'font-size', 'h4_font_family' => 'font-family', 'h4_font_style' => 'font-style', 'h4_font_color' => 'color', 'h4_line_height' => 'line-height' ), 'h4 {', '}' );
				echo sh_get_font_settings( array( 'h5_font_size' => 'font-size', 'h5_font_family' => 'font-family', 'h5_font_style' => 'font-style', 'h5_font_color' => 'color', 'h5_line_height' => 'line-height' ), 'h5 {', '}' );
				echo sh_get_font_settings( array( 'h6_font_size' => 'font-size', 'h6_font_family' => 'font-family', 'h6_font_style' => 'font-style', 'h6_font_color' => 'color', 'h6_line_height' => 'line-height' ), 'h6 {', '}' );
			}
			
			echo sh_set( $opt, 'header_css' );
			?>
		</style>
        
        <?php if( sh_set( $opt, 'header_js' ) ): ?>
			<script type="text/javascript">
                <?php echo sh_set( $opt, 'header_js' );?>
            </script>
        <?php endif;
		
		sh_theme_color_scheme();
	}
	
	function wp_footer()
	{
		$analytics = sh_set( $this->opt, 'footer_analytics');
		
		echo $analytics;
		
		
		if( $footer_js = sh_set( $this->opt, 'footer_js' ) ): ?>
			<script type="text/javascript">
                <?php echo $footer_js;?>
            </script>
        <?php endif;
	}
}

new SH_Enqueue;

