#useful scripts

SELECT p.`ID`, p.`post_title`, pm.`meta_value` 
FROM `wp_posts` p, `wp_postmeta` pm 
WHERE p.`ID` = pm.`post_id` and pm.`meta_key` = '_wpsc_sku' and `post_status` ='publish';

#Profiling mysql 

SET GLOBAL general_log = 'ON'; 
SET GLOBAL log_output = 'TABLE';

SELECT event_time, 
            command_type, 
            argument 
FROM mysql.general_log 
ORDER BY event_time

TRUNCATE mysql.general_log

SET GLOBAL general_log = 'OFF';

# Insert 

INSERT INTO `Nautic`.`wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT @post_id, `term_id`, '0'
FROM `wp_terms` 
WHERE `slug` = @term_slug;

UPDATE `Nautic`.`wp_posts` SET `post_title` = @title WHERE `ID` = @post_id;

UPDATE `Nautic`.`wp_postmeta` SET `meta_value` = @sku WHERE `ID` = @post_id AND `meta_key` = '_wpsc_sku';