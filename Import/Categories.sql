#Generated file
DELETE FROM `wp_term_taxonomy` WHERE `taxonomy` = 'wpsc_product_category';
DELETE FROM `wp_terms` WHERE `term_group` = 1;


INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Moteurs Essence', 'moteurs-essence', 1);
SET @lastValue = LAST_INSERT_ID();
SET @parentValue = @lastValue;
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', 0, 0);
	
/*
	CHILDREN OF {Moteurs Essence}
*/
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Moteurs remanufacturés USA', 'moteurs-remanufactures-usa', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Moteurs neufs', 'moteurs-neufs', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
/*
	END OF CHILDREN OF
*/
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Turbo', 'turbo', 1);
SET @lastValue = LAST_INSERT_ID();
SET @parentValue = @lastValue;
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', 0, 0);
	
/*
	CHILDREN OF {Turbo}
*/
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Turbo Volvo Penta', 'turbo-volvo-penta', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Turbo Cummins', 'turbo-cummins', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Turbo Caterpillar', 'turbo-caterpillar', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Turbo Yanmar', 'turbo-yanmar', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Turbo Mercruiser VM', 'turbo-mercruiser-vm', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Turbo Detroit Diesel', 'turbo-detroit-diesel', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
/*
	END OF CHILDREN OF
*/
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Embases', 'embases', 1);
SET @lastValue = LAST_INSERT_ID();
SET @parentValue = @lastValue;
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', 0, 0);
	
/*
	CHILDREN OF {Embases}
*/
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Embase Mercruiser', 'embase-mercruiser', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Embase Mercury', 'embase-mercury', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Embase Yamaha', 'embase-yamaha', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Embase Johnson', 'embase-johnson', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Embase Volvo Penta', 'embase-volvo-penta', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
/*
	END OF CHILDREN OF
*/
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Pièces détachées', 'pieces-detachees', 1);
SET @lastValue = LAST_INSERT_ID();
SET @parentValue = @lastValue;
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', 0, 0);
	
/*
	CHILDREN OF {Pièces détachées}
*/
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Alarmes, Sondes, Capteurs', 'alarmes-sondes-capteurs', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Culasses, joints de culasses, joints', 'culasses-joints-de-culasses-joints', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
/*
	CHILDREN OF {Culasses, joints de culasses, joints}
*/
	
	
SET @oldParentValue = @parentValue;
SET @parentValue = @lastValue;


INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Culasses', 'culasses', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Joints de culasses', 'joints-de-culasses', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Joints d''admission', 'joints-dadmission', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Joints collecteurs échappements', 'joints-collecteurs-echappements', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Pochettes de joints complètes', 'pochettes-de-joints-completes', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Joints carter d''huile', 'joints-carter-dhuile', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
	
SET @parentValue = @oldParentValue;


/*
	END OF CHILDREN OF
*/
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Balancier d''équilibrage', 'balancier-dequilibrage', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Bougies, Fils de bougie, Têtes de Delco, Distributeurs, câblages', 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-câblages', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Cache culbuteurs', 'cache-culbuteurs', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Carters de distribution', 'carters-de-distribution', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Carters d''huile', 'carters-dhuile', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Filtres à essence', 'filtres-a-essence', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Filtres à huile', 'filtres-a-huile', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Injecteurs, Carburateurs, couvres-filtres, kit injection', 'injecteurs-carburateurs-couvres-filtres-kit-injection', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Pipes d''admission', 'pipes-dadmission', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Pompes à essence', 'pompes-a-essence', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Pompes à huile, pompes direction assistée', 'pompes-a-huile-pompes-direction-assistee', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Poussoirs hydroliques', 'poussoirs-hydroliques', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Villebrequins', 'villebrequins', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Volants moteur', 'volants-moteur', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Alternateurs', 'alternateurs', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
/*
	CHILDREN OF {Alternateurs}
*/
	
	
SET @oldParentValue = @parentValue;
SET @parentValue = @lastValue;


INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Alternateur Mercruiser', 'alternateur-mercruiser', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Alternateur Mercury', 'alternateur-mercury', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Alternateur Yanmar', 'alternateur-yanmar', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Alternateur Volvo Penta', 'alternateur-volvo-penta', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Alternateur Caterpillar', 'alternateur-caterpillar', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Alternateur Cummings', 'alternateur-cummings', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Alternateur Detroit Diesel', 'alternateur-detroit-diesel', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
	
SET @parentValue = @oldParentValue;


/*
	END OF CHILDREN OF
*/
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Démarreurs', 'demarreurs', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
/*
	CHILDREN OF {Démarreurs}
*/
	
	
SET @oldParentValue = @parentValue;
SET @parentValue = @lastValue;


INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Démarreur Mercruiser', 'demarreur-mercruiser', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Démarreur Mercury', 'demarreur-mercury', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Démarreur Yanmar', 'demarreur-yanmar', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Démarreur Volvo Penta', 'demarreur-volvo-penta', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Démarreur Caterpillar', 'demarreur-caterpillar', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Démarreur Cummings', 'demarreur-cummings', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Démarreur Detroit Diesel', 'demarreur-detroit-diesel', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
	
SET @parentValue = @oldParentValue;


/*
	END OF CHILDREN OF
*/
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Collecteurs d''échappement', 'collecteurs-dechappement', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	
INSERT INTO `wp_terms` (`name`,`slug`,`term_group`) VALUES ('Brasseurs d''eau et pompes eau de mer', 'brasseurs-deau-et-pompes-eau-de-mer', 1);
SET @lastValue = LAST_INSERT_ID();
INSERT INTO `wp_term_taxonomy` (`term_id`,`taxonomy`,`description`,`parent`,`count`) VALUES (@lastValue,'wpsc_product_category','', @parentValue, 0);
	