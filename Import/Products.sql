#Generated file
#Init 283 products
INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 756, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Sonde de temperature d''eau filetage 1/2-14' WHERE `ID` = 756;

UPDATE  `wp_postmeta` SET `meta_value` = '5898' WHERE `post_id` = 756 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 757, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Sonde de temperature d''eau pour Mercruiser et Volvo Penta' WHERE `ID` = 757;

UPDATE  `wp_postmeta` SET `meta_value` = 'TX3' WHERE `post_id` = 757 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 758, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Capteur de cognement Mercruiser Volvo Penta' WHERE `ID` = 758;

UPDATE  `wp_postmeta` SET `meta_value` = 'KS3' WHERE `post_id` = 758 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 759, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Capteur alarme Mercruiser et Volvo Penta' WHERE `ID` = 759;

UPDATE  `wp_postmeta` SET `meta_value` = 'KS213' WHERE `post_id` = 759 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 760, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Capteur de villebrequin vortec V6 V8 Mercruiser et Volvo Penta' WHERE `ID` = 760;

UPDATE  `wp_postmeta` SET `meta_value` = 'PC123' WHERE `post_id` = 760 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 761, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Cassette electronique modele delco Mercruiser et Volvo Penta' WHERE `ID` = 761;

UPDATE  `wp_postmeta` SET `meta_value` = 'LX384' WHERE `post_id` = 761 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 762, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Cassette electronique modele delco Mercruiser et Volvo Penta' WHERE `ID` = 762;

UPDATE  `wp_postmeta` SET `meta_value` = 'M1599' WHERE `post_id` = 762 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 764, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Valve anti retour Mercruiser et Volvo Penta' WHERE `ID` = 764;

UPDATE  `wp_postmeta` SET `meta_value` = 'CV754C' WHERE `post_id` = 764 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 765, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Iac 4.3l 5.7l Volvo Penta ' WHERE `ID` = 765;

UPDATE  `wp_postmeta` SET `meta_value` = 'AC6' WHERE `post_id` = 765 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 766, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Capteur pour delco Mercruiser Volvo Penta' WHERE `ID` = 766;

UPDATE  `wp_postmeta` SET `meta_value` = 'LX756' WHERE `post_id` = 766 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 768, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Relais Mercruiser' WHERE `ID` = 768;

UPDATE  `wp_postmeta` SET `meta_value` = 'RY282' WHERE `post_id` = 768 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 769, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Sonde de temperature d''eau filetage 3/8-18' WHERE `ID` = 769;

UPDATE  `wp_postmeta` SET `meta_value` = 'S898' WHERE `post_id` = 769 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 770, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Capteur d''alumage delco' WHERE `ID` = 770;

UPDATE  `wp_postmeta` SET `meta_value` = '10005' WHERE `post_id` = 770 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 771, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Capteur temperature d''air Mercruiser efi Volvo Penta' WHERE `ID` = 771;

UPDATE  `wp_postmeta` SET `meta_value` = 'AX1' WHERE `post_id` = 771 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 772, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Iac Mercruiser mpi Volvo Penta' WHERE `ID` = 772;

UPDATE  `wp_postmeta` SET `meta_value` = 'AC423' WHERE `post_id` = 772 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 773, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Iac Mercruiser mpi' WHERE `ID` = 773;

UPDATE  `wp_postmeta` SET `meta_value` = 'AC5' WHERE `post_id` = 773 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 774, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Map V6 4.3l .v8 5.7l Mercruiser et Volvo Penta' WHERE `ID` = 774;

UPDATE  `wp_postmeta` SET `meta_value` = 'AS59' WHERE `post_id` = 774 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 775, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Map V8 Volvo Penta' WHERE `ID` = 775;

UPDATE  `wp_postmeta` SET `meta_value` = 'AS165' WHERE `post_id` = 775 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 777, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Tps V6. V8' WHERE `ID` = 777;

UPDATE  `wp_postmeta` SET `meta_value` = 'TH42' WHERE `post_id` = 777 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 782, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Tps V8 mpi pour Mercruiser' WHERE `ID` = 782;

UPDATE  `wp_postmeta` SET `meta_value` = 'TH149' WHERE `post_id` = 782 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 784, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Capteur d''arbre a came pour V8 vortec 8.1l Mercruiser' WHERE `ID` = 784;

UPDATE  `wp_postmeta` SET `meta_value` = 'PC129' WHERE `post_id` = 784 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 785, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Iac 454 7.4 l pour Mercruiser' WHERE `ID` = 785;

UPDATE  `wp_postmeta` SET `meta_value` = 'AC147' WHERE `post_id` = 785 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 786, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Capteur de villebrequin V8 8.1l pour Mercruiser (gris)' WHERE `ID` = 786;

UPDATE  `wp_postmeta` SET `meta_value` = 'PC340' WHERE `post_id` = 786 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 787, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Capteur de villebrequin V8 8.1l pour Mercruiser' WHERE `ID` = 787;

UPDATE  `wp_postmeta` SET `meta_value` = 'PC620' WHERE `post_id` = 787 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 788, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Capteur landa' WHERE `ID` = 788;

UPDATE  `wp_postmeta` SET `meta_value` = 'WAL 93224006' WHERE `post_id` = 788 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 789, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Sonde de pression d''huile pour tous modeles Mercruiser et Volvo Penta' WHERE `ID` = 789;

UPDATE  `wp_postmeta` SET `meta_value` = 'PS377' WHERE `post_id` = 789 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 790, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Switch declanchement pompe a essence electrique' WHERE `ID` = 790;

UPDATE  `wp_postmeta` SET `meta_value` = 'A68301' WHERE `post_id` = 790 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 810, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alarmes-sondes-capteurs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alarmes-sondes-capteurs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Ts pour V8 351w base Ford carburateur ou efi pour Volvo Penta Omc et pcm' WHERE `ID` = 810;

UPDATE  `wp_postmeta` SET `meta_value` = 'HS9281' WHERE `post_id` = 810 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 584, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alternateur-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alternateur-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Alternateur neuf pour Mercruiser 1988 &gt; 1995 4cyl -v6-v8' WHERE `ID` = 584;

UPDATE  `wp_postmeta` SET `meta_value` = 'AMN002' WHERE `post_id` = 584 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 586, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alternateur-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alternateur-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Alternateur neuf pour Mercruiser V8 807653t 1995 &gt; ' WHERE `ID` = 586;

UPDATE  `wp_postmeta` SET `meta_value` = 'AMN0005' WHERE `post_id` = 586 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 587, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alternateur-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alternateur-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Alternateur neuf pour Mercruiser V6-v8 efi or mpi' WHERE `ID` = 587;

UPDATE  `wp_postmeta` SET `meta_value` = 'ADR0299' WHERE `post_id` = 587 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 588, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alternateur-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alternateur-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Alternateur neuf pour Mercruiser ' WHERE `ID` = 588;

UPDATE  `wp_postmeta` SET `meta_value` = 'missing value' WHERE `post_id` = 588 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 589, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alternateur-volvo-penta' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alternateur-volvo-penta' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Alternateur neuf pour Volvo Penta 4 cyl-v6-v8' WHERE `ID` = 589;

UPDATE  `wp_postmeta` SET `meta_value` = 'AMN002' WHERE `post_id` = 589 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 590, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alternateur-volvo-penta' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alternateur-volvo-penta' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Alternateur neuf pour Volvo Penta valeo marine' WHERE `ID` = 590;

UPDATE  `wp_postmeta` SET `meta_value` = 'APR0018' WHERE `post_id` = 590 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 591, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'alternateur-volvo-penta' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'alternateur-volvo-penta' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Alternateur neuf pour Volvo Penta diesel valeo marine' WHERE `ID` = 591;

UPDATE  `wp_postmeta` SET `meta_value` = 'APR0019' WHERE `post_id` = 591 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 704, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'balancier-dequilibrage' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'balancier-dequilibrage' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Balancier d''equilibrage V6 .v8 . 4.3l. 5.0l . 5.7l 1998 &gt;  1992 carburateur Mercruiser et Volvo Penta Omc' WHERE `ID` = 704;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM594012' WHERE `post_id` = 704 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 705, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'balancier-dequilibrage' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'balancier-dequilibrage' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Balancier d''equilibrage (damper) V6 vortec Mercruiser mpi Volvo Penta gxi' WHERE `ID` = 705;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM594181' WHERE `post_id` = 705 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 706, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'balancier-dequilibrage' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'balancier-dequilibrage' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Balancier d''equilibrage(damper) V8 vortec 5.0l .5.7l Mercruiser mpi Volvo Penta gxi' WHERE `ID` = 706;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM594121' WHERE `post_id` = 706 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 707, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'balancier-dequilibrage' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'balancier-dequilibrage' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Balancier d''equilibrage (damper) V8 pre-vortec carburateur Mercruiser et Volvo Penta' WHERE `ID` = 707;

UPDATE  `wp_postmeta` SET `meta_value` = '594002' WHERE `post_id` = 707 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 708, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'balancier-dequilibrage' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'balancier-dequilibrage' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Balancier d''equilibrage (damper) gm 7.4l 454 cid Mercruiser Volvo Penta' WHERE `ID` = 708;

UPDATE  `wp_postmeta` SET `meta_value` = '594010' WHERE `post_id` = 708 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 709, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'balancier-dequilibrage' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'balancier-dequilibrage' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Balancier d''equilibrage(damper) V8 base Ford 5.ol . 5.8l VolvoPenta .Omc' WHERE `ID` = 709;

UPDATE  `wp_postmeta` SET `meta_value` = '594083' WHERE `post_id` = 709 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 879, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Cablage moteur male pour Mercruiser' WHERE `ID` = 879;

UPDATE  `wp_postmeta` SET `meta_value` = '620-08900' WHERE `post_id` = 879 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 668, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Bougie tous models 4 cyl 3.0l gm 181 cid' WHERE `ID` = 668;

UPDATE  `wp_postmeta` SET `meta_value` = 'ACR43LTS' WHERE `post_id` = 668 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 669, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Bougie pour V6 4.3l gm non vortec 1988 a 1990 carburateur' WHERE `ID` = 669;

UPDATE  `wp_postmeta` SET `meta_value` = 'ACR43T' WHERE `post_id` = 669 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 670, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Bougie V6 vortec carburateur 4.3l 1991 &gt; 2000' WHERE `ID` = 670;

UPDATE  `wp_postmeta` SET `meta_value` = 'ACR43LTS' WHERE `post_id` = 670 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 671, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Bougie pour V6 vortec 4.3l mpi efi irridium' WHERE `ID` = 671;

UPDATE  `wp_postmeta` SET `meta_value` = 'AC41-993' WHERE `post_id` = 671 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 672, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'B0ugie pour V8 gm 5.0l 5.7l 7.4l 8.2l carburateur 1986 &gt; 1990' WHERE `ID` = 672;

UPDATE  `wp_postmeta` SET `meta_value` = 'ACR43T' WHERE `post_id` = 672 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 673, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Bougie pour V8 base Ford carburateur Volvo Penta . Omc. pcm' WHERE `ID` = 673;

UPDATE  `wp_postmeta` SET `meta_value` = 'ACR43T' WHERE `post_id` = 673 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 674, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Bougie irridium pour V8 vortec efi et mpi Volvo Penta et Mercruiser' WHERE `ID` = 674;

UPDATE  `wp_postmeta` SET `meta_value` = 'AC41-993' WHERE `post_id` = 674 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 681, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Fil de bougies V6 thunderbolt Mercruiser' WHERE `ID` = 681;

UPDATE  `wp_postmeta` SET `meta_value` = '4810M-6' WHERE `post_id` = 681 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 682, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Fils de bougies pour V8 thunderbolt Mercruiser' WHERE `ID` = 682;

UPDATE  `wp_postmeta` SET `meta_value` = '4810M-8' WHERE `post_id` = 682 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 683, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Fils de bougies V8 7.4l .8.2l thunderbolt Mercruiser' WHERE `ID` = 683;

UPDATE  `wp_postmeta` SET `meta_value` = '4827M-8' WHERE `post_id` = 683 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 685, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Fils de bougies 4 cylindres hei Mercruiser et Volvo Penta' WHERE `ID` = 685;

UPDATE  `wp_postmeta` SET `meta_value` = '4809M' WHERE `post_id` = 685 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 687, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Fils de bougies V6 hei delco 1994 &gt; 2000 male 90° Mercruiser. Volvo Penta' WHERE `ID` = 687;

UPDATE  `wp_postmeta` SET `meta_value` = '4806M-6' WHERE `post_id` = 687 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 688, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Fils de bougies delco hei V8 Mercruiser Volvo Penta male 90° 1992 &gt; 2005' WHERE `ID` = 688;

UPDATE  `wp_postmeta` SET `meta_value` = '4806M-8' WHERE `post_id` = 688 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 689, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Fils de bougies hei delco pour 7.4l/8.2l Mercruiser .Volvo Penta male 90°' WHERE `ID` = 689;

UPDATE  `wp_postmeta` SET `meta_value` = '4816M-8' WHERE `post_id` = 689 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 690, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Fils de bougies V6 hei delco tete de delco plate Mercruiser mpi/Volvo Penta gxi' WHERE `ID` = 690;

UPDATE  `wp_postmeta` SET `meta_value` = '3146-6' WHERE `post_id` = 690 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 691, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Fils de bougies V8 hei delco Mercruiser mpi Volvo Penta gxi' WHERE `ID` = 691;

UPDATE  `wp_postmeta` SET `meta_value` = '3146-8' WHERE `post_id` = 691 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 692, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Fils de bougies V8 Ford fi injection Volvo Penta' WHERE `ID` = 692;

UPDATE  `wp_postmeta` SET `meta_value` = '210' WHERE `post_id` = 692 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 693, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Fils de bougies V8 Omc . Volvo Penta . pcm tetede delco a vis prestolite' WHERE `ID` = 693;

UPDATE  `wp_postmeta` SET `meta_value` = '4821M-8' WHERE `post_id` = 693 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 694, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Fils de bougies V6 Volvo Penta tete de delco a vis prestolite' WHERE `ID` = 694;

UPDATE  `wp_postmeta` SET `meta_value` = '4821M-6' WHERE `post_id` = 694 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 845, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Distributeur d''allumage delco tous V8 gm pour Mercruiser et Volvo Penta' WHERE `ID` = 845;

UPDATE  `wp_postmeta` SET `meta_value` = 'DST1830' WHERE `post_id` = 845 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 846, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Distributeur d''allumage V6 4.3l gm pour Mercruiser et Volvo Penta' WHERE `ID` = 846;

UPDATE  `wp_postmeta` SET `meta_value` = 'DST1635' WHERE `post_id` = 846 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 847, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Distributeur d''allumage type delco tous V8 pour Mercruiser et Volvo Penta' WHERE `ID` = 847;

UPDATE  `wp_postmeta` SET `meta_value` = 'DST1829' WHERE `post_id` = 847 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 848, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Distributeur d''allumage type delco V6 pour Mercruiser ou Volvo Penta' WHERE `ID` = 848;

UPDATE  `wp_postmeta` SET `meta_value` = 'DST 841839' WHERE `post_id` = 848 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 849, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Bobine d''allumage hei pour Volvo Penta et Mercruiser' WHERE `ID` = 849;

UPDATE  `wp_postmeta` SET `meta_value` = 'DR38M' WHERE `post_id` = 849 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 850, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Cordon d''allumage hei bobine pour Mercruiser et Volvo Penta' WHERE `ID` = 850;

UPDATE  `wp_postmeta` SET `meta_value` = 'DST1831' WHERE `post_id` = 850 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 851, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Bobine d''allumage hei pour Volvo Penta et Mercruiser' WHERE `ID` = 851;

UPDATE  `wp_postmeta` SET `meta_value` = 'FD478' WHERE `post_id` = 851 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 852, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit tete de delco. type delco. 4 cylindres pour Volvo Penta ou Mercruiser' WHERE `ID` = 852;

UPDATE  `wp_postmeta` SET `meta_value` = 'DC1014' WHERE `post_id` = 852 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 853, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit tete de delco V6 4.3l type delco pour Mercruiser et Volvo Penta' WHERE `ID` = 853;

UPDATE  `wp_postmeta` SET `meta_value` = 'missing value' WHERE `post_id` = 853 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 854, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit tete de delco type delco tous V8 pour Mercruiser et Volvo Penta' WHERE `ID` = 854;

UPDATE  `wp_postmeta` SET `meta_value` = 'DR1016' WHERE `post_id` = 854 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 855, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit tete de delco type delco V6 4.3l gxi mpi pour Mercruiser et Volvo Penta' WHERE `ID` = 855;

UPDATE  `wp_postmeta` SET `meta_value` = 'DC2009' WHERE `post_id` = 855 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 856, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit tete de delco type delco tous V8 gxi mpi pour Mercruiser et Volvo Penta' WHERE `ID` = 856;

UPDATE  `wp_postmeta` SET `meta_value` = 'DC1001' WHERE `post_id` = 856 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 857, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit tete de delco prestolite a vis V6 4.3l pour Volvo Penta et Omc' WHERE `ID` = 857;

UPDATE  `wp_postmeta` SET `meta_value` = 'AL487' WHERE `post_id` = 857 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 858, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit tete de delco prestolite a vis tous V8 pour Volvo Penta et Omc' WHERE `ID` = 858;

UPDATE  `wp_postmeta` SET `meta_value` = 'AL488' WHERE `post_id` = 858 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 859, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit tete de delco base Ford fi 5.0l.5.8l pour Volvo Penta' WHERE `ID` = 859;

UPDATE  `wp_postmeta` SET `meta_value` = '3D1117' WHERE `post_id` = 859 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 860, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit tete de delco model tunderbolt V6 pour Mercruiser' WHERE `ID` = 860;

UPDATE  `wp_postmeta` SET `meta_value` = 'AL488' WHERE `post_id` = 860 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 861, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit tete de delco tunderbolt V8 pour Mercruiser' WHERE `ID` = 861;

UPDATE  `wp_postmeta` SET `meta_value` = 'AL488-1' WHERE `post_id` = 861 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 880, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages
' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'bougies-fils-de-bougie-tetes-de-delco-distributeurs-cablages
' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Cablage moteur femele pour Mercruiser' WHERE `ID` = 880;

UPDATE  `wp_postmeta` SET `meta_value` = '620-08813' WHERE `post_id` = 880 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 634, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'brasseurs-deau-et-pompes-eau-de-mer' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'brasseurs-deau-et-pompes-eau-de-mer' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit turbine pour Mercruiser alpha one' WHERE `ID` = 634;

UPDATE  `wp_postmeta` SET `meta_value` = '9610201K' WHERE `post_id` = 634 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 642, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'brasseurs-deau-et-pompes-eau-de-mer' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'brasseurs-deau-et-pompes-eau-de-mer' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit turbine pour alpha one gen ii' WHERE `ID` = 642;

UPDATE  `wp_postmeta` SET `meta_value` = '9611601K' WHERE `post_id` = 642 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 715, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'carters-de-distribution' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'carters-de-distribution' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carter de distribution tole V6 . V8 gm 1980 &gt; 1991' WHERE `ID` = 715;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM655512' WHERE `post_id` = 715 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 717, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'carters-de-distribution' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'carters-de-distribution' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carter de distribution plastique avec entree capteur V6 vortec Mercruiser . Volvo Penta' WHERE `ID` = 717;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM35502' WHERE `post_id` = 717 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 718, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'carters-de-distribution' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'carters-de-distribution' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carter de distribution plastique avec entree capteur V8 Mercruiser . Volvo Penta' WHERE `ID` = 718;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM635505' WHERE `post_id` = 718 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 719, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'carters-de-distribution' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'carters-de-distribution' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carter de distribution tole 7.4l 454 cid Mercruiser. Volvo Penta' WHERE `ID` = 719;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM63551' WHERE `post_id` = 719 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 721, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'carters-de-distribution' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'carters-de-distribution' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carter de distribution 4 cyl tole 3.ol gm Mercruiser . Volvo Penta' WHERE `ID` = 721;

UPDATE  `wp_postmeta` SET `meta_value` = '635501' WHERE `post_id` = 721 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 695, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'carters-dhuile' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'carters-dhuile' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carter d''huile pour V6 4.3l tole 1988 &gt; 1992 pour Volvo Penta et Mercruiser' WHERE `ID` = 695;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM264106' WHERE `post_id` = 695 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 696, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'carters-dhuile' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'carters-dhuile' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carter d''huile V6 4.3l vortec en aluminium Mercruiser et Volvo Penta' WHERE `ID` = 696;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM12559523' WHERE `post_id` = 696 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 697, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'carters-dhuile' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'carters-dhuile' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carter dhuile V8 5.0l base Ford Volvo Penta .Omc .pcm' WHERE `ID` = 697;

UPDATE  `wp_postmeta` SET `meta_value` = 'FO264002' WHERE `post_id` = 697 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 698, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'carters-dhuile' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'carters-dhuile' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carter d''huile d''huile V8 5.0l 5.7l 1988 &gt; 2000 Mercruiser . Volvo Penta .cobra . pcm' WHERE `ID` = 698;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM264132' WHERE `post_id` = 698 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 700, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'carters-dhuile' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'carters-dhuile' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carter d''huile V8 tole 5.ol 5.7l 1980 &gt; 1986 Mercruiser Volvo Penta Omc' WHERE `ID` = 700;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM264103' WHERE `post_id` = 700 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 701, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'carters-dhuile' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'carters-dhuile' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carter d''huile V8 7.4l 454 cid tole Mercruiser . Volvo Penta' WHERE `ID` = 701;

UPDATE  `wp_postmeta` SET `meta_value` = 'GMP41A' WHERE `post_id` = 701 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 881, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'collecteurs-dechappement' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'collecteurs-dechappement' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Coude d''echappement 4 cylindres lx Mercruiser' WHERE `ID` = 881;

UPDATE  `wp_postmeta` SET `meta_value` = '1972' WHERE `post_id` = 881 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 883, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'collecteurs-dechappement' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'collecteurs-dechappement' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Collecteur 4 cylindres lx pour Mercruiser' WHERE `ID` = 883;

UPDATE  `wp_postmeta` SET `meta_value` = '1956' WHERE `post_id` = 883 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 884, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'collecteurs-dechappement' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'collecteurs-dechappement' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Coude d''echappement V6. V8 pour Mercruiser' WHERE `ID` = 884;

UPDATE  `wp_postmeta` SET `meta_value` = '1976' WHERE `post_id` = 884 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 885, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'collecteurs-dechappement' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'collecteurs-dechappement' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Collecteur d''echappement V8 5.0l 5.7l 1988 &gt; 1998 pour Mercruiser' WHERE `ID` = 885;

UPDATE  `wp_postmeta` SET `meta_value` = '1953' WHERE `post_id` = 885 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 886, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'collecteurs-dechappement' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'collecteurs-dechappement' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Collecteur d''echappement V6 4.3l 1988 &gt; 1998 pour Mercruiser ' WHERE `ID` = 886;

UPDATE  `wp_postmeta` SET `meta_value` = '1952' WHERE `post_id` = 886 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 888, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'collecteurs-dechappement' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'collecteurs-dechappement' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Collecteur d''echappement V8 7.4l 8.2l pour Mercruiser' WHERE `ID` = 888;

UPDATE  `wp_postmeta` SET `meta_value` = '1957' WHERE `post_id` = 888 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 889, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'collecteurs-dechappement' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'collecteurs-dechappement' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Coude d''echappement V6.v8 pour Volvo Penta gs gi gxi' WHERE `ID` = 889;

UPDATE  `wp_postmeta` SET `meta_value` = '1909' WHERE `post_id` = 889 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 890, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'collecteurs-dechappement' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'collecteurs-dechappement' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Collecteur d''echappement V8 5.0l 5.7l pour Volvo Penta gs gi gxi' WHERE `ID` = 890;

UPDATE  `wp_postmeta` SET `meta_value` = '1986' WHERE `post_id` = 890 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 891, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'collecteurs-dechappement' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'collecteurs-dechappement' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Collecteur d''echappement V6 pour Volvo Penta gs gi gxi' WHERE `ID` = 891;

UPDATE  `wp_postmeta` SET `meta_value` = '3857656' WHERE `post_id` = 891 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 892, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'collecteurs-dechappement' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'collecteurs-dechappement' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Collecteur d''echappement V8 base Ford carburateur pour Volvo Penta et Omc' WHERE `ID` = 892;

UPDATE  `wp_postmeta` SET `meta_value` = '3852347' WHERE `post_id` = 892 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 893, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'collecteurs-dechappement' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'collecteurs-dechappement' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Coude d''echappement inox V6 V8 pour Mercruiser' WHERE `ID` = 893;

UPDATE  `wp_postmeta` SET `meta_value` = '32-420' WHERE `post_id` = 893 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 740, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'culasses' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'culasses' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Culasse V6 4.3l 1987 &gt; 1991 pour Mercruiser . Volvo Penta et Omc' WHERE `ID` = 740;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM43' WHERE `post_id` = 740 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 741, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'culasses' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'culasses' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Culasse V8 5.7l 350 cid 1987 &gt; 1992 pour Mercruiser . VolvoPenta et Omc' WHERE `ID` = 741;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM57' WHERE `post_id` = 741 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 742, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'culasses' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'culasses' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Culasse vortec 5.7l 350 cid 1992 &gt; 2006 pour Mercruiser et Volvo Penta' WHERE `ID` = 742;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM57V' WHERE `post_id` = 742 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 743, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'culasses' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'culasses' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Culasse 4 cylindres 3.0l gm 140 hp pour Mercruiser et Volvo Penta' WHERE `ID` = 743;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM181' WHERE `post_id` = 743 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 744, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'culasses' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'culasses' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Culasse V6 4.3l vortec 1991 &gt; 1995 pour Mercruiser et Volvo Penta' WHERE `ID` = 744;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM43V' WHERE `post_id` = 744 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 745, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'culasses' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'culasses' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Culasse V6 4.3l vortec pour Mercruiser et Volvo Penta' WHERE `ID` = 745;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM43V1' WHERE `post_id` = 745 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 579, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'demarreur-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'demarreur-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Demarreur neuf pour Mercruiser 4 cylindre 3.0 l' WHERE `ID` = 579;

UPDATE  `wp_postmeta` SET `meta_value` = 'SDR0059' WHERE `post_id` = 579 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 580, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'demarreur-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'demarreur-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Demarreur neuf pour Mercruiser V6 -v8' WHERE `ID` = 580;

UPDATE  `wp_postmeta` SET `meta_value` = 'SDR003131L' WHERE `post_id` = 580 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 581, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'demarreur-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'demarreur-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Demarreur neuf pour Mercruiser V8 5.ol 5.8l 1975 &gt; 1988' WHERE `ID` = 581;

UPDATE  `wp_postmeta` SET `meta_value` = 'SFD0026' WHERE `post_id` = 581 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 572, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'demarreur-volvo-penta' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'demarreur-volvo-penta' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Demarreur neuf pour Volvo Penta V6 -v8' WHERE `ID` = 572;

UPDATE  `wp_postmeta` SET `meta_value` = 'SDR0031L' WHERE `post_id` = 572 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 573, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'demarreur-volvo-penta' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'demarreur-volvo-penta' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Demarreur neuf pour Volvo Penta 3.0l 4 cylindres' WHERE `ID` = 573;

UPDATE  `wp_postmeta` SET `meta_value` = 'SDR0059' WHERE `post_id` = 573 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 575, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'demarreur-volvo-penta' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'demarreur-volvo-penta' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Demarreur neuf pour Volvo Penta diesel 31 - 41 - 42' WHERE `ID` = 575;

UPDATE  `wp_postmeta` SET `meta_value` = 'SPR007' WHERE `post_id` = 575 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 576, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'demarreur-volvo-penta' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'demarreur-volvo-penta' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Demarreur neuf pour Volvo Penta V8 5.0l - 5.8l base Ford' WHERE `ID` = 576;

UPDATE  `wp_postmeta` SET `meta_value` = 'SFD0010' WHERE `post_id` = 576 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 577, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'demarreur-volvo-penta' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'demarreur-volvo-penta' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Demarreur neuf pour Volvo Penta V8 Ford 1 generation' WHERE `ID` = 577;

UPDATE  `wp_postmeta` SET `meta_value` = 'SFD0026' WHERE `post_id` = 577 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 578, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'demarreur-volvo-penta' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'demarreur-volvo-penta' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Demarreur neuf pour Volvo Penta 4 cyl 2.0l et 2.5l 1972 &gt; 1990' WHERE `ID` = 578;

UPDATE  `wp_postmeta` SET `meta_value` = 'missing value' WHERE `post_id` = 578 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 583, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'demarreur-volvo-penta' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'demarreur-volvo-penta' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Demarreur neuf pour pcm indmar V8 base Ford' WHERE `ID` = 583;

UPDATE  `wp_postmeta` SET `meta_value` = 'SFD 0026' WHERE `post_id` = 583 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 548, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-johnson' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-johnson' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase neuve pour johnson ocean pro V4 V6 ' WHERE `ID` = 548;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI 306/25:1' WHERE `post_id` = 548 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 549, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-johnson' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-johnson' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase neuve pour johnson V6 ocean pro' WHERE `ID` = 549;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI306/1.83' WHERE `post_id` = 549 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 550, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-johnson' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-johnson' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase neuve pour V6 johnson ocean pro contre rotation' WHERE `ID` = 550;

UPDATE  `wp_postmeta` SET `meta_value` = 'missing value' WHERE `post_id` = 550 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 552, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-johnson' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-johnson' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase neuve pour johnson V4 1995 &gt; 2006' WHERE `ID` = 552;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI305/2.1' WHERE `post_id` = 552 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 494, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase 4 cylindres neuve pour alpha one 73 &gt; 91 Mercruiser' WHERE `ID` = 494;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI106194' WHERE `post_id` = 494 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 497, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase V8 neuve 73 &gt; 91 alpha one pour Mercruiser' WHERE `ID` = 497;

UPDATE  `wp_postmeta` SET `meta_value` = 'se106147' WHERE `post_id` = 497 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 498, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase V6 neuve alpha one 73 &gt; 91 pour Mercruiser' WHERE `ID` = 498;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI106181' WHERE `post_id` = 498 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 499, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase 6 cylindres en ligne neuve 73 &gt; 91 pour Mercruiser' WHERE `ID` = 499;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI106162' WHERE `post_id` = 499 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 500, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase 4 cylindres 91 &gt; 2110 neuve pour modele alpha one gen ii Mercruiser' WHERE `ID` = 500;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI116194' WHERE `post_id` = 500 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 501, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase neuve V6 91 &gt; 2010 modele pour alpha one gen ii Mercruiser' WHERE `ID` = 501;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI116162' WHERE `post_id` = 501 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 502, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase V8 neuve 91 &gt; 2010 pour alpha one gen ii Mercruiser' WHERE `ID` = 502;

UPDATE  `wp_postmeta` SET `meta_value` = 'SIE116147' WHERE `post_id` = 502 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 503, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pied embase bravo one neuve pour Mercruiser' WHERE `ID` = 503;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI121' WHERE `post_id` = 503 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 644, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit soufflet embase alpha one' WHERE `ID` = 644;

UPDATE  `wp_postmeta` SET `meta_value` = '9A108' WHERE `post_id` = 644 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 645, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit soufflet alpha one gen ii' WHERE `ID` = 645;

UPDATE  `wp_postmeta` SET `meta_value` = '9A116' WHERE `post_id` = 645 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 646, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint papier d''application embase alpha one' WHERE `ID` = 646;

UPDATE  `wp_postmeta` SET `meta_value` = '9510601' WHERE `post_id` = 646 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 647, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Roulement de bol pour Mercruiser . Volvo Penta . Omc cobra' WHERE `ID` = 647;

UPDATE  `wp_postmeta` SET `meta_value` = '9A10801' WHERE `post_id` = 647 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 649, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe de trim complet pour Mercruiser et Mercury' WHERE `ID` = 649;

UPDATE  `wp_postmeta` SET `meta_value` = '9C10827' WHERE `post_id` = 649 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 651, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Cardan Mercruiser alpha one 4 cylindres (arbre court)' WHERE `ID` = 651;

UPDATE  `wp_postmeta` SET `meta_value` = '9212106' WHERE `post_id` = 651 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 652, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Cardan Mercruiser alpha one et gen ii tous V6/v8 81481a2' WHERE `ID` = 652;

UPDATE  `wp_postmeta` SET `meta_value` = '9210206' WHERE `post_id` = 652 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 654, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint spy cardan exterieur Mercruiser alpha one et gen ii' WHERE `ID` = 654;

UPDATE  `wp_postmeta` SET `meta_value` = '9410201' WHERE `post_id` = 654 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 655, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Verin alpha one tribord 98704a3' WHERE `ID` = 655;

UPDATE  `wp_postmeta` SET `meta_value` = '9810401' WHERE `post_id` = 655 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 656, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Verin pour Mercruiser alpha one babord 98703a4' WHERE `ID` = 656;

UPDATE  `wp_postmeta` SET `meta_value` = '9B10402' WHERE `post_id` = 656 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 657, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Verin tribord pour Mercruiser alpha gen ii 14034a3' WHERE `ID` = 657;

UPDATE  `wp_postmeta` SET `meta_value` = '9B10601' WHERE `post_id` = 657 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 658, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Verin babord pour Mercruiser gii 14035a3' WHERE `ID` = 658;

UPDATE  `wp_postmeta` SET `meta_value` = '14035A3' WHERE `post_id` = 658 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 659, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Verin tribord pour Mercruiser gen ii montage clips 815954a7' WHERE `ID` = 659;

UPDATE  `wp_postmeta` SET `meta_value` = '9B11601' WHERE `post_id` = 659 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 660, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Verin babord pour Mercruiser alpha one gen ii a clips 815935a7' WHERE `ID` = 660;

UPDATE  `wp_postmeta` SET `meta_value` = '9B11602' WHERE `post_id` = 660 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 661, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Verin tribord pour Mercruiser bravo 98704a26' WHERE `ID` = 661;

UPDATE  `wp_postmeta` SET `meta_value` = '9B12101' WHERE `post_id` = 661 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 662, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Verin babord pour Mercruiser bravo 98703a26' WHERE `ID` = 662;

UPDATE  `wp_postmeta` SET `meta_value` = '9B12102' WHERE `post_id` = 662 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 663, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit flexible de trim pour Mercruiser mr' WHERE `ID` = 663;

UPDATE  `wp_postmeta` SET `meta_value` = '9B102' WHERE `post_id` = 663 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 664, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit flexible de trim alpha one' WHERE `ID` = 664;

UPDATE  `wp_postmeta` SET `meta_value` = '9B104' WHERE `post_id` = 664 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 665, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Flexibles de trim pour Mercruiser alpha gen ii ( a clip) et bravo' WHERE `ID` = 665;

UPDATE  `wp_postmeta` SET `meta_value` = '9B106' WHERE `post_id` = 665 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 666, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Ecrou de serrage diabolo alpha one i et ii' WHERE `ID` = 666;

UPDATE  `wp_postmeta` SET `meta_value` = '97110203' WHERE `post_id` = 666 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 667, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercruiser' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercruiser' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Ecrou de serrage cardan alpha one i et ii' WHERE `ID` = 667;

UPDATE  `wp_postmeta` SET `meta_value` = '9710204' WHERE `post_id` = 667 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 526, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercury' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercury' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase neuve pour Mercury 2 temps et 4 temps 70hp &gt; 125 hp' WHERE `ID` = 526;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEIS205' WHERE `post_id` = 526 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 540, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercury' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercury' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase Mercury 2.0l 2.4l 2.5l' WHERE `ID` = 540;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEIS206' WHERE `post_id` = 540 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 541, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercury' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercury' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase neuve pour Mercury 3.0l' WHERE `ID` = 541;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEIS216' WHERE `post_id` = 541 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 542, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercury' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercury' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase neuve pour Mercury 3.0l contre rotation' WHERE `ID` = 542;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI218' WHERE `post_id` = 542 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 553, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-mercury' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-mercury' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase neuve pour Mercury verado' WHERE `ID` = 553;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI207' WHERE `post_id` = 553 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 650, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-volvo-penta' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-volvo-penta' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Cardan Volvo sx et cobra 3852385' WHERE `ID` = 650;

UPDATE  `wp_postmeta` SET `meta_value` = '9210906' WHERE `post_id` = 650 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 653, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-volvo-penta' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-volvo-penta' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint spy cardan exterieur pour Volvo sx' WHERE `ID` = 653;

UPDATE  `wp_postmeta` SET `meta_value` = '9411509' WHERE `post_id` = 653 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 511, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-yamaha' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-yamaha' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase neuve pour Yamaha V4 115 hp 130 hp' WHERE `ID` = 511;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI405' WHERE `post_id` = 511 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 520, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-yamaha' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-yamaha' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase V6 pour Yamaha' WHERE `ID` = 520;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI 416' WHERE `post_id` = 520 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 521, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-yamaha' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-yamaha' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase neuve pour Yamaha V6 contre rotation' WHERE `ID` = 521;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI418CR' WHERE `post_id` = 521 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 522, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-yamaha' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-yamaha' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase neuve pour Yamaha V6 bloc moteur 76°' WHERE `ID` = 522;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI421' WHERE `post_id` = 522 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 524, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'embase-yamaha' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'embase-yamaha' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Embase pour Yamaha V6 bloc moteur 76° contre rotation' WHERE `ID` = 524;

UPDATE  `wp_postmeta` SET `meta_value` = 'SEI428CR' WHERE `post_id` = 524 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 678, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'filtres-a-essence' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'filtres-a-essence' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Filtre a essence court Mercruiser Volvo Penta' WHERE `ID` = 678;

UPDATE  `wp_postmeta` SET `meta_value` = 'PS3808' WHERE `post_id` = 678 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 679, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'filtres-a-essence' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'filtres-a-essence' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Filtre a essence Omc carburateur' WHERE `ID` = 679;

UPDATE  `wp_postmeta` SET `meta_value` = 'PS3645' WHERE `post_id` = 679 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 680, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'filtres-a-essence' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'filtres-a-essence' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Filtre a essence Omc . Volvo Penta fi. gxi. gi. gs petit modele vo -3862228' WHERE `ID` = 680;

UPDATE  `wp_postmeta` SET `meta_value` = 'MAL9-37804' WHERE `post_id` = 680 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 675, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'filtres-a-huile' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'filtres-a-huile' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Filtre a huile court 4 cylindres et V6 .v8 deporte Mercruiser et Volvo Penta' WHERE `ID` = 675;

UPDATE  `wp_postmeta` SET `meta_value` = 'PF454' WHERE `post_id` = 675 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 676, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'filtres-a-huile' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'filtres-a-huile' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Filtre a huile long V8 tous modeles carburateur Mercruiser. Volvo. Penta .Omc' WHERE `ID` = 676;

UPDATE  `wp_postmeta` SET `meta_value` = 'ML1011' WHERE `post_id` = 676 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 677, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'filtres-a-huile' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'filtres-a-huile' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Filtre a huile pour V6 4.3l 1987 &gt; 1995 Mercruiser Volvo Penta Omc' WHERE `ID` = 677;

UPDATE  `wp_postmeta` SET `meta_value` = 'ML1007' WHERE `post_id` = 677 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 776, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'injecteurs-carburateurs-couvres-filtres-kit-injection' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'injecteurs-carburateurs-couvres-filtres-kit-injection' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Injecteur mono point' WHERE `ID` = 776;

UPDATE  `wp_postmeta` SET `meta_value` = 'TJ17' WHERE `post_id` = 776 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 783, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'injecteurs-carburateurs-couvres-filtres-kit-injection' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'injecteurs-carburateurs-couvres-filtres-kit-injection' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Injecteur pour Volvo Penta 3855182' WHERE `ID` = 783;

UPDATE  `wp_postmeta` SET `meta_value` = 'TJ11' WHERE `post_id` = 783 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 816, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'injecteurs-carburateurs-couvres-filtres-kit-injection' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'injecteurs-carburateurs-couvres-filtres-kit-injection' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Injecteur V6 4.3l vortec. V8 5.0l 5.7l vortec pour Mercruiser et Volvo Penta' WHERE `ID` = 816;

UPDATE  `wp_postmeta` SET `meta_value` = 'FJ459' WHERE `post_id` = 816 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 817, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'injecteurs-carburateurs-couvres-filtres-kit-injection' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'injecteurs-carburateurs-couvres-filtres-kit-injection' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Injecteur V8 496 8.1l pour Mercruiser' WHERE `ID` = 817;

UPDATE  `wp_postmeta` SET `meta_value` = '51136' WHERE `post_id` = 817 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 818, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'injecteurs-carburateurs-couvres-filtres-kit-injection' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'injecteurs-carburateurs-couvres-filtres-kit-injection' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Injecteur V8 302w 5.ol base Ford pour Volvo Penta' WHERE `ID` = 818;

UPDATE  `wp_postmeta` SET `meta_value` = 'MF18' WHERE `post_id` = 818 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 862, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'injecteurs-carburateurs-couvres-filtres-kit-injection' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'injecteurs-carburateurs-couvres-filtres-kit-injection' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carburateur remanufacture V8 por Volvo Penta' WHERE `ID` = 862;

UPDATE  `wp_postmeta` SET `meta_value` = '33011' WHERE `post_id` = 862 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 863, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'injecteurs-carburateurs-couvres-filtres-kit-injection' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'injecteurs-carburateurs-couvres-filtres-kit-injection' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carburateur V8 5.7l et 7.4l starter electrique pour Mercruiser' WHERE `ID` = 863;

UPDATE  `wp_postmeta` SET `meta_value` = '33005' WHERE `post_id` = 863 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 864, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'injecteurs-carburateurs-couvres-filtres-kit-injection' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'injecteurs-carburateurs-couvres-filtres-kit-injection' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carburateur V6 4.3l starter electrique pour Mercruiser' WHERE `ID` = 864;

UPDATE  `wp_postmeta` SET `meta_value` = '33001' WHERE `post_id` = 864 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 865, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'injecteurs-carburateurs-couvres-filtres-kit-injection' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'injecteurs-carburateurs-couvres-filtres-kit-injection' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carburateur carter V6 lx pour Mercruiser' WHERE `ID` = 865;

UPDATE  `wp_postmeta` SET `meta_value` = '9909' WHERE `post_id` = 865 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 866, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'injecteurs-carburateurs-couvres-filtres-kit-injection' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'injecteurs-carburateurs-couvres-filtres-kit-injection' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carburateu holley V8 base Ford pour Omc .pcm' WHERE `ID` = 866;

UPDATE  `wp_postmeta` SET `meta_value` = '80219-1' WHERE `post_id` = 866 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 871, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'injecteurs-carburateurs-couvres-filtres-kit-injection' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'injecteurs-carburateurs-couvres-filtres-kit-injection' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carburateur V6 4.3l Mercruiser' WHERE `ID` = 871;

UPDATE  `wp_postmeta` SET `meta_value` = '33001' WHERE `post_id` = 871 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 872, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'injecteurs-carburateurs-couvres-filtres-kit-injection' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'injecteurs-carburateurs-couvres-filtres-kit-injection' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carburateur carter pour V6 4.3l pour Mercruiser 1989 &gt; 1992' WHERE `ID` = 872;

UPDATE  `wp_postmeta` SET `meta_value` = '9909' WHERE `post_id` = 872 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 873, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'injecteurs-carburateurs-couvres-filtres-kit-injection' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'injecteurs-carburateurs-couvres-filtres-kit-injection' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Carburateur holley V6 4.3l et V8 5.8l Ford pour Volvo Penta Omc et pcm' WHERE `ID` = 873;

UPDATE  `wp_postmeta` SET `meta_value` = '80319-1' WHERE `post_id` = 873 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 874, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'injecteurs-carburateurs-couvres-filtres-kit-injection' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'injecteurs-carburateurs-couvres-filtres-kit-injection' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Couvre filtre a air vortec pour carburateur' WHERE `ID` = 874;

UPDATE  `wp_postmeta` SET `meta_value` = '432120' WHERE `post_id` = 874 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 796, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-carter-dhuile' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-carter-dhuile' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint de carte d''huile V6 4.3l vortec carter en aluminium pour Mercruiser et Volvo Penta' WHERE `ID` = 796;

UPDATE  `wp_postmeta` SET `meta_value` = 'PG3129' WHERE `post_id` = 796 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 797, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-carter-dhuile' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-carter-dhuile' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint de carter d''huile V6 4.3l carter tole 1987 &gt; 1991 pour Mercruiser et Volvo Penta' WHERE `ID` = 797;

UPDATE  `wp_postmeta` SET `meta_value` = '16528' WHERE `post_id` = 797 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 798, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-carter-dhuile' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-carter-dhuile' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint de carter d''huile V8 5.0l 5.7l et vortec 1987 &gt; ' WHERE `ID` = 798;

UPDATE  `wp_postmeta` SET `meta_value` = 'OS34500R' WHERE `post_id` = 798 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 799, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-carter-dhuile' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-carter-dhuile' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint de carter d''huile V8 base Ford 302w pour Volvo Penta Omc' WHERE `ID` = 799;

UPDATE  `wp_postmeta` SET `meta_value` = 'FP810' WHERE `post_id` = 799 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 800, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-carter-dhuile' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-carter-dhuile' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint de carter d''huile V8 7.4l 454 vortec pour Mercruiser et Volvo Penta' WHERE `ID` = 800;

UPDATE  `wp_postmeta` SET `meta_value` = 'OS3340R' WHERE `post_id` = 800 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 791, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-collecteurs-echappements' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-collecteurs-echappements' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joints de collecteur d''echappement V6 tous 4.3l gm pour Mercruiser Volvo pent Omc' WHERE `ID` = 791;

UPDATE  `wp_postmeta` SET `meta_value` = '1402' WHERE `post_id` = 791 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 792, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-collecteurs-echappements' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-collecteurs-echappements' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Its de collecteur d''echappement pour tous V8 5.0l 5.7l gm pour Mercruiser et Volvo Penta' WHERE `ID` = 792;

UPDATE  `wp_postmeta` SET `meta_value` = 'HED 27500' WHERE `post_id` = 792 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 793, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-collecteurs-echappements' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-collecteurs-echappements' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joints de collecteur d''echappement V8 7.4 8.2 8.1l pour Mercruiser et Volvo pe' WHERE `ID` = 793;

UPDATE  `wp_postmeta` SET `meta_value` = 'EG3111' WHERE `post_id` = 793 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 794, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-collecteurs-echappements' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-collecteurs-echappements' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint d''echappement 470 pour Mercruiser' WHERE `ID` = 794;

UPDATE  `wp_postmeta` SET `meta_value` = 'FP1419' WHERE `post_id` = 794 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 795, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-collecteurs-echappements' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-collecteurs-echappements' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joinst d''echappement V8 302w 351w base Ford pour Volvo Penta pcm' WHERE `ID` = 795;

UPDATE  `wp_postmeta` SET `meta_value` = 'HED27340' WHERE `post_id` = 795 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 722, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-dadmission' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-dadmission' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint d''admission V6 4.3l 1986 &gt; 1991 Mercruiser . Volvo Penta .Omc' WHERE `ID` = 722;

UPDATE  `wp_postmeta` SET `meta_value` = 'missing value' WHERE `post_id` = 722 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 724, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-dadmission' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-dadmission' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint d''admission V8 5.ol 5.7l gm 1986 &gt; 1991 Mercruiser . Volvo Penta . Omc' WHERE `ID` = 724;

UPDATE  `wp_postmeta` SET `meta_value` = '111120SEC' WHERE `post_id` = 724 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 725, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-dadmission' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-dadmission' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint d''admission V8 7.4l 454 gm admission carre Mercruiser hp' WHERE `ID` = 725;

UPDATE  `wp_postmeta` SET `meta_value` = '95008' WHERE `post_id` = 725 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 726, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-dadmission' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-dadmission' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint admission plastique V6 vortec Mercruiser . Volvo Penta' WHERE `ID` = 726;

UPDATE  `wp_postmeta` SET `meta_value` = 'MS5958171' WHERE `post_id` = 726 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 727, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-dadmission' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-dadmission' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint d''admission V8 base Ford 5.ol 5.8l Volvo Penta Omc' WHERE `ID` = 727;

UPDATE  `wp_postmeta` SET `meta_value` = 'MS90361' WHERE `post_id` = 727 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 728, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-dadmission' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-dadmission' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joit d''admission 4 cyl 3.7l base Ford 470 cid Mercruiser' WHERE `ID` = 728;

UPDATE  `wp_postmeta` SET `meta_value` = '95109SG' WHERE `post_id` = 728 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 729, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-dadmission' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-dadmission' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint d''admission plastique V8 vortec Mercruiser . Volvo Penta' WHERE `ID` = 729;

UPDATE  `wp_postmeta` SET `meta_value` = 'missing value' WHERE `post_id` = 729 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 730, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-dadmission' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-dadmission' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint d''admission V8 7.4 l admission oval Mercruiser Volvo Penta' WHERE `ID` = 730;

UPDATE  `wp_postmeta` SET `meta_value` = 'missing value' WHERE `post_id` = 730 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 731, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-dadmission' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-dadmission' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint d''admission V8 6.0l vortec Volvo Penta . Mercruiser' WHERE `ID` = 731;

UPDATE  `wp_postmeta` SET `meta_value` = '15479A' WHERE `post_id` = 731 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 732, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-de-culasses' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-de-culasses' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint de culasse V6 262 cid 4.3l et vortec Mercruiser et Volvo Penta Omc' WHERE `ID` = 732;

UPDATE  `wp_postmeta` SET `meta_value` = '287070' WHERE `post_id` = 732 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 733, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-de-culasses' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-de-culasses' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint de culasse V8 5.0 .5.7l gm et vortec Mercruiser et Volvo Penta' WHERE `ID` = 733;

UPDATE  `wp_postmeta` SET `meta_value` = '287082' WHERE `post_id` = 733 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 734, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-de-culasses' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-de-culasses' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint de culasse V8 base Ford Volvo Penta Omc' WHERE `ID` = 734;

UPDATE  `wp_postmeta` SET `meta_value` = '3428' WHERE `post_id` = 734 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 735, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-de-culasses' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-de-culasses' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint de culasse 4 cylindres 2.5l 3.0l gm Mercruiser .Volvo Penta' WHERE `ID` = 735;

UPDATE  `wp_postmeta` SET `meta_value` = '1179VC' WHERE `post_id` = 735 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 736, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'joints-de-culasses' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'joints-de-culasses' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Joint de culasse 4 cylindres 470 Mercruiser' WHERE `ID` = 736;

UPDATE  `wp_postmeta` SET `meta_value` = '3536VC' WHERE `post_id` = 736 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 626, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur neuf gm vortec 8.1l 375 hp Mercruiser/Volvo Penta' WHERE `ID` = 626;

UPDATE  `wp_postmeta` SET `meta_value` = '81496STD' WHERE `post_id` = 626 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 621, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur neuf 7.4 l 454 cid 1987 &gt; 2002 330/350 hp' WHERE `ID` = 621;

UPDATE  `wp_postmeta` SET `meta_value` = '743301G6' WHERE `post_id` = 621 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 622, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur neuf 7.4 l 454 392hp 1987 &gt; 2002' WHERE `ID` = 622;

UPDATE  `wp_postmeta` SET `meta_value` = '744392' WHERE `post_id` = 622 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 489, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Vortec 3.0l 4 cylindres pour Mercruiser et Volvo Penta et Omc' WHERE `ID` = 489;

UPDATE  `wp_postmeta` SET `meta_value` = '301501P' WHERE `post_id` = 489 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 493, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'V8 6.0l vortec pour Mercruiser Volvo Penta indmar' WHERE `ID` = 493;

UPDATE  `wp_postmeta` SET `meta_value` = '60350V' WHERE `post_id` = 493 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 625, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur neuf gm 8.2 l 450 hp Mercruiser hp' WHERE `ID` = 625;

UPDATE  `wp_postmeta` SET `meta_value` = '82450' WHERE `post_id` = 625 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 624, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur neuf 7.4 l 454 hp 424 hp' WHERE `ID` = 624;

UPDATE  `wp_postmeta` SET `meta_value` = '74425G6WRCVH' WHERE `post_id` = 624 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 602, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur neuf V8 vortec 5.0l 305 cid 1998 &gt; 2004' WHERE `ID` = 602;

UPDATE  `wp_postmeta` SET `meta_value` = '50250V' WHERE `post_id` = 602 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 603, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur neuf V8 vortec 5.7l 350 cid 1998 &gt; 2004' WHERE `ID` = 603;

UPDATE  `wp_postmeta` SET `meta_value` = '57350V' WHERE `post_id` = 603 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 604, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur neuf V8 6.2v vortec ' WHERE `ID` = 604;

UPDATE  `wp_postmeta` SET `meta_value` = 'missing value' WHERE `post_id` = 604 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 606, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur neuf V8 350 cid 260 hp 1967 &gt; 1986' WHERE `ID` = 606;

UPDATE  `wp_postmeta` SET `meta_value` = 'missing value' WHERE `post_id` = 606 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 608, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur neuf V8 6.2 l stroker 340 hp vortec' WHERE `ID` = 608;

UPDATE  `wp_postmeta` SET `meta_value` = 'missing value' WHERE `post_id` = 608 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 629, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur complet 357 Mercruiser usa carburateur 325 hp' WHERE `ID` = 629;

UPDATE  `wp_postmeta` SET `meta_value` = '357MAG' WHERE `post_id` = 629 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 627, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur neuf 8.1l vortec 420 hp pour Mercruiser hp' WHERE `ID` = 627;

UPDATE  `wp_postmeta` SET `meta_value` = '81496HO' WHERE `post_id` = 627 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 628, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur neuf gm 8.1l 496 vortec 8100+  525 hp' WHERE `ID` = 628;

UPDATE  `wp_postmeta` SET `meta_value` = '81496525' WHERE `post_id` = 628 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 630, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-neufs' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-neufs' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur complet neuf vortec V8 330 hp pour Volvo Penta et Mercruiser' WHERE `ID` = 630;

UPDATE  `wp_postmeta` SET `meta_value` = '350VMPI' WHERE `post_id` = 630 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 77, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-remanufactures-usa' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-remanufactures-usa' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur V6 4.3l - 90 &gt; 93 pour Mercruiser et Volvo Penta' WHERE `ID` = 77;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM 262-1' WHERE `post_id` = 77 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 214, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-remanufactures-usa' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-remanufactures-usa' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur V6 4.3l 92 &gt; modèle vortec pour Mercruiser et Volvo Penta' WHERE `ID` = 214;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM262V' WHERE `post_id` = 214 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 215, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-remanufactures-usa' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-remanufactures-usa' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'V8 5.7l 92 &gt; 350 cid gm vortec pour Mercruiser et Volvo Penta' WHERE `ID` = 215;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM350 VORTEC' WHERE `post_id` = 215 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 467, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-remanufactures-usa' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-remanufactures-usa' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur V6 4.3l année 1986 &gt; 1990 remplacement Mercruiser ou Volvo Penta' WHERE `ID` = 467;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM262' WHERE `post_id` = 467 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 479, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-remanufactures-usa' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-remanufactures-usa' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur V8 5.7l 1987 &gt; 1992 pour Mercruiser ou Volvo Penta' WHERE `ID` = 479;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM350' WHERE `post_id` = 479 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 481, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-remanufactures-usa' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-remanufactures-usa' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'V8 7.4l 454 cid 83 &gt; 95 pour Mercruiser et Volvo Penta et crusader' WHERE `ID` = 481;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM454' WHERE `post_id` = 481 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 483, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-remanufactures-usa' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-remanufactures-usa' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'V8 7.4 l 454 cid 83 &gt; 95 pour Mercruiser hight perf...' WHERE `ID` = 483;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM454HP' WHERE `post_id` = 483 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 485, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-remanufactures-usa' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-remanufactures-usa' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'V8 318 cid 5.2 l chryler ' WHERE `ID` = 485;

UPDATE  `wp_postmeta` SET `meta_value` = 'CH318' WHERE `post_id` = 485 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 486, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-remanufactures-usa' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-remanufactures-usa' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'V8 5.9l 360 cid chrysler' WHERE `ID` = 486;

UPDATE  `wp_postmeta` SET `meta_value` = 'CH360' WHERE `post_id` = 486 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 487, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-remanufactures-usa' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-remanufactures-usa' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'V8 Ford 5.0l 302 cid pour Mercruiser Volvo Penta crusader' WHERE `ID` = 487;

UPDATE  `wp_postmeta` SET `meta_value` = 'F302' WHERE `post_id` = 487 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 488, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-remanufactures-usa' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-remanufactures-usa' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'V8 351 cid 5.8l base Ford pour Omc pcm' WHERE `ID` = 488;

UPDATE  `wp_postmeta` SET `meta_value` = 'F351' WHERE `post_id` = 488 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 490, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-remanufactures-usa' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-remanufactures-usa' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'V6 vortec 4.3l 262 cid pour Mercruiser et Volvo Penta' WHERE `ID` = 490;

UPDATE  `wp_postmeta` SET `meta_value` = '432352V' WHERE `post_id` = 490 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 607, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'moteurs-remanufactures-usa' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'moteurs-remanufactures-usa' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Moteur 5.7l 350 cid 270 hp pre-vortec 1987 &gt; 1991' WHERE `ID` = 607;

UPDATE  `wp_postmeta` SET `meta_value` = 'missing value' WHERE `post_id` = 607 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 894, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pieces-honda' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pieces-honda' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Honda bobine d''allumage adaptable serie n°30520pwc003' WHERE `ID` = 894;

UPDATE  `wp_postmeta` SET `meta_value` = 'UF581' WHERE `post_id` = 894 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 896, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pieces-honda' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pieces-honda' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Honda capteur de villebrequin adaptable serie n°3750pna003' WHERE `ID` = 896;

UPDATE  `wp_postmeta` SET `meta_value` = 'PC376' WHERE `post_id` = 896 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 898, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pieces-honda' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pieces-honda' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Honda capteur d''arbre a came adaptable serie n°37510pwa003' WHERE `ID` = 898;

UPDATE  `wp_postmeta` SET `meta_value` = 'PC610' WHERE `post_id` = 898 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 899, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pieces-honda' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pieces-honda' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Honda capteur de detonation adaptable serie n°30530p8fa01' WHERE `ID` = 899;

UPDATE  `wp_postmeta` SET `meta_value` = 'KS102' WHERE `post_id` = 899 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 900, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pieces-honda' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pieces-honda' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Honda sensor adaptable serie n°30530pna003' WHERE `ID` = 900;

UPDATE  `wp_postmeta` SET `meta_value` = 'KS195' WHERE `post_id` = 900 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 710, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pipes-dadmission' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pipes-dadmission' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pipe admission V6 vortec 4 corps carburateur Mercruiser Volvo Penta' WHERE `ID` = 710;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM4304V' WHERE `post_id` = 710 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 711, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pipes-dadmission' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pipes-dadmission' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pipe admission V8 vortec 5.0l .5.7 l carburateur 4 corps 1993 &gt; 2000 Mercruiser VolvoPenta' WHERE `ID` = 711;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM5700V' WHERE `post_id` = 711 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 712, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pipes-dadmission' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pipes-dadmission' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pipe admission V8 base fi 5.0l Volvo Penta original' WHERE `ID` = 712;

UPDATE  `wp_postmeta` SET `meta_value` = 'FO302FI' WHERE `post_id` = 712 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 713, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pipes-dadmission' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pipes-dadmission' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pipe admission 7.4l 454 admission oval 330hp carburateur Mercruiser Volvo Penta' WHERE `ID` = 713;

UPDATE  `wp_postmeta` SET `meta_value` = 'GM74100' WHERE `post_id` = 713 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 714, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pipes-dadmission' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pipes-dadmission' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Kit boulons de fixation pipe admission V6 . V8 vortec mercfruiser et Volvo Penta' WHERE `ID` = 714;

UPDATE  `wp_postmeta` SET `meta_value` = 'missing value' WHERE `post_id` = 714 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 801, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pochettes-de-joints-completes' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pochettes-de-joints-completes' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pochette de joints completes V6 4.3l 1987 &gt; 1991 no vortec pour Mercruiser et Volvo Penta' WHERE `ID` = 801;

UPDATE  `wp_postmeta` SET `meta_value` = 'KS2647' WHERE `post_id` = 801 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 802, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pochettes-de-joints-completes' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pochettes-de-joints-completes' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pochette de joints completes V6 4.3l vortec 1991 &gt;  pour Mercruiser et Volvo Penta' WHERE `ID` = 802;

UPDATE  `wp_postmeta` SET `meta_value` = '953491VR' WHERE `post_id` = 802 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 803, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pochettes-de-joints-completes' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pochettes-de-joints-completes' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pochette de joint 4 cylindres 3.0l 140 hp pour Mercruiser et Volvo Penta' WHERE `ID` = 803;

UPDATE  `wp_postmeta` SET `meta_value` = 'FS1179VX' WHERE `post_id` = 803 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 804, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pochettes-de-joints-completes' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pochettes-de-joints-completes' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pochette de joints complete V8 7.4l 454 330 hp pour Mercruiser et Volvo Penta' WHERE `ID` = 804;

UPDATE  `wp_postmeta` SET `meta_value` = 'HS4878' WHERE `post_id` = 804 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 805, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pochettes-de-joints-completes' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pochettes-de-joints-completes' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pochette de joints 7.4l 454 cid admission carre 392hp pour Mercruiser' WHERE `ID` = 805;

UPDATE  `wp_postmeta` SET `meta_value` = '953491VR' WHERE `post_id` = 805 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 806, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pochettes-de-joints-completes' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pochettes-de-joints-completes' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pochette de joints de joints V8 5.0l 5.7l 350 cid pompe a essence mecanique por Mercruiser et Volvo Penta' WHERE `ID` = 806;

UPDATE  `wp_postmeta` SET `meta_value` = 'KS2666' WHERE `post_id` = 806 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 807, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pochettes-de-joints-completes' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pochettes-de-joints-completes' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pochette de joints V8 5.0l 5.7l vortec joint admission en plastique pour Mercruiser et Volvo Penta' WHERE `ID` = 807;

UPDATE  `wp_postmeta` SET `meta_value` = '953488' WHERE `post_id` = 807 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 808, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pochettes-de-joints-completes' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pochettes-de-joints-completes' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pochette de joint V8 base Ford 302w pour Volvo Penta Omc' WHERE `ID` = 808;

UPDATE  `wp_postmeta` SET `meta_value` = 'HS9280' WHERE `post_id` = 808 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 746, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-essence' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-essence' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe a essence electrique basse pression V8 efi V6 V8 carburateur Mercruiser et Volvo Penta' WHERE `ID` = 746;

UPDATE  `wp_postmeta` SET `meta_value` = '7329-8263' WHERE `post_id` = 746 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 747, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-essence' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-essence' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe a essence electrique basse bression V8 7.4l carburateur' WHERE `ID` = 747;

UPDATE  `wp_postmeta` SET `meta_value` = 'E8264' WHERE `post_id` = 747 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 748, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-essence' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-essence' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe a essence electrique haute bression V8 base Ford fi. V6 V8 efi Mercruiser et Volvo Penta' WHERE `ID` = 748;

UPDATE  `wp_postmeta` SET `meta_value` = 'E8266' WHERE `post_id` = 748 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 749, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-essence' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-essence' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe a essence electrique raccord filete haute pression mpi 861155a3' WHERE `ID` = 749;

UPDATE  `wp_postmeta` SET `meta_value` = 'E11003' WHERE `post_id` = 749 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 750, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-essence' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-essence' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe a essence electrique haute pression raccord non filete 861156a1' WHERE `ID` = 750;

UPDATE  `wp_postmeta` SET `meta_value` = 'E11004' WHERE `post_id` = 750 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 751, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-essence' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-essence' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe a essence electrique 12 volts basse pression remplacement pompe a essence mecanique Mercruiser et Volvo Penta' WHERE `ID` = 751;

UPDATE  `wp_postmeta` SET `meta_value` = 'P4389' WHERE `post_id` = 751 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 752, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-essence' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-essence' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe a essence mecanique V8 5.0l 5.7l 0mc . Mercruiser' WHERE `ID` = 752;

UPDATE  `wp_postmeta` SET `meta_value` = '6973' WHERE `post_id` = 752 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 753, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-essence' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-essence' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe a essence mecanique V8 Omc base Ford ' WHERE `ID` = 753;

UPDATE  `wp_postmeta` SET `meta_value` = '6974' WHERE `post_id` = 753 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 754, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-essence' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-essence' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe a essence mecanique V6 4.3l Volvo Mercruiser' WHERE `ID` = 754;

UPDATE  `wp_postmeta` SET `meta_value` = '60201' WHERE `post_id` = 754 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 755, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-essence' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-essence' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe a essence 4 cylindres 3.0l Volvo Penta Mercruiser' WHERE `ID` = 755;

UPDATE  `wp_postmeta` SET `meta_value` = '60337' WHERE `post_id` = 755 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 813, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-essence' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-essence' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe a essence 454 cid 7.4l (marine haute pression)pour Mercruiser et Volvo Penta' WHERE `ID` = 813;

UPDATE  `wp_postmeta` SET `meta_value` = 'SU122177' WHERE `post_id` = 813 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 815, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-essence' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-essence' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe a essence (haute pression) V8 351 base Ford pour Volvo Penta Omc pcm' WHERE `ID` = 815;

UPDATE  `wp_postmeta` SET `meta_value` = 'SU122169' WHERE `post_id` = 815 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 811, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-huile-pompes-direction-assistee' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-huile-pompes-direction-assistee' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe a huile (marine haute pression) V6 4.3l V8 5.0l 5.7l pour Mercruiser Volvo Penta et Omc' WHERE `ID` = 811;

UPDATE  `wp_postmeta` SET `meta_value` = 'SU 121155' WHERE `post_id` = 811 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 812, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-huile-pompes-direction-assistee' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-huile-pompes-direction-assistee' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe a huile (marine haute pression) V6 vortec 4.3l pour Mercruiser et Volvo Penta ' WHERE `ID` = 812;

UPDATE  `wp_postmeta` SET `meta_value` = 'SU121156' WHERE `post_id` = 812 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 814, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-huile-pompes-direction-assistee' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-huile-pompes-direction-assistee' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe a huile V8 302w base Ford pour Volvo Penta' WHERE `ID` = 814;

UPDATE  `wp_postmeta` SET `meta_value` = 'SU122168' WHERE `post_id` = 814 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 825, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'pompes-a-huile-pompes-direction-assistee' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'pompes-a-huile-pompes-direction-assistee' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Pompe de direction assiste 4cyl V6 et V81986 &gt; 1996 pour Mercruiser et Volvo Penta' WHERE `ID` = 825;

UPDATE  `wp_postmeta` SET `meta_value` = 'missing value' WHERE `post_id` = 825 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 819, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'poussoirs-hydroliques' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'poussoirs-hydroliques' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Poussoir hydraulique 4cyl 6 cyl V8 non roller pour Mercruiser et Volvo Penta Omc pcm' WHERE `ID` = 819;

UPDATE  `wp_postmeta` SET `meta_value` = 'LIF3101' WHERE `post_id` = 819 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 820, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'poussoirs-hydroliques' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'poussoirs-hydroliques' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Poussoir hydrolique a roller V6 et V8 5.0l 5.7l pour Mercruiser et Volvo Penta' WHERE `ID` = 820;

UPDATE  `wp_postmeta` SET `meta_value` = 'LI3104' WHERE `post_id` = 820 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 821, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'poussoirs-hydroliques' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'poussoirs-hydroliques' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Poussoir hydrolique V8 base Ford 302w 351w pour Volvo Penta Mercruiser' WHERE `ID` = 821;

UPDATE  `wp_postmeta` SET `meta_value` = 'LIF4137' WHERE `post_id` = 821 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 822, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'poussoirs-hydroliques' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'poussoirs-hydroliques' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Poussoir hydrolique a roller V8 base Ford 302w 351w pour Volvo Penta pcm' WHERE `ID` = 822;

UPDATE  `wp_postmeta` SET `meta_value` = 'LIF 213742' WHERE `post_id` = 822 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 823, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'poussoirs-hydroliques' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'poussoirs-hydroliques' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Poussoir hydrolique V8 7.4l 454 cid pour Mercruiser et Volvo Penta' WHERE `ID` = 823;

UPDATE  `wp_postmeta` SET `meta_value` = 'LIF 3302' WHERE `post_id` = 823 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 824, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'poussoirs-hydroliques' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'poussoirs-hydroliques' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Tige de culbuteur pour poussoir a roller V6 vortec V8 5.0l 5.7l' WHERE `ID` = 824;

UPDATE  `wp_postmeta` SET `meta_value` = 'missing value' WHERE `post_id` = 824 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 840, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'soupapes' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'soupapes' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Soupape admission pour V6 4.3l. V8 5.7l non vortec Volvo Penta et Mercruiser et Omc' WHERE `ID` = 840;

UPDATE  `wp_postmeta` SET `meta_value` = 'V1926' WHERE `post_id` = 840 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 842, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'soupapes' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'soupapes' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Soupape echappement V6 4.3l. V8 5.7l non vortec pour Mercruiser Volvo Penta' WHERE `ID` = 842;

UPDATE  `wp_postmeta` SET `meta_value` = 'V1904' WHERE `post_id` = 842 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 843, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'soupapes' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'soupapes' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Soupape echappement V8 7.4l 330 hp pour Mercruiser et Volvo Penta' WHERE `ID` = 843;

UPDATE  `wp_postmeta` SET `meta_value` = 'V1989X' WHERE `post_id` = 843 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 844, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'soupapes' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'soupapes' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Soupape admission 7.4l 330 hp pour Mercruiser et Volvo Penta' WHERE `ID` = 844;

UPDATE  `wp_postmeta` SET `meta_value` = 'V1912' WHERE `post_id` = 844 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 702, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'volants-moteur' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'volants-moteur' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Volant moteur V6 .v8 gm 5.0l 5.7l 4.3l 1988 &gt; 2010 et vortec Mercruiser et Volvo Penta . Omc' WHERE `ID` = 702;

UPDATE  `wp_postmeta` SET `meta_value` = '506525' WHERE `post_id` = 702 AND `meta_key` = '_wpsc_sku';

INSERT IGNORE INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) 
SELECT 703, tt.`term_taxonomy_id`, 0
FROM `wp_terms` t, `wp_term_taxonomy` tt 
WHERE `slug` =  'volants-moteur' AND t.`term_id` = tt.`term_id`;

UPDATE `wp_term_taxonomy` t
INNER JOIN (SELECT count(1) as `countTerms`,  tt.`term_taxonomy_id` FROM `wp_term_taxonomy` tt 
LEFT JOIN `wp_term_relationships` t ON t.`term_taxonomy_id` = tt.`term_taxonomy_id`
LEFT JOIN `wp_terms` ts ON ts.`term_id` = tt.`term_id`
WHERE ts.`slug` = 'volants-moteur' ) AS TMP
SET `count`= TMP.`countTerms`
WHERE TMP.`term_taxonomy_id` = t.`term_taxonomy_id`;

UPDATE `wp_posts` SET `post_title` = 'Volant moteur tole 454 cid 7.4l tous modeles Mercruiser . Volvo Penta' WHERE `ID` = 703;

UPDATE  `wp_postmeta` SET `meta_value` = '506528' WHERE `post_id` = 703 AND `meta_key` = '_wpsc_sku';
