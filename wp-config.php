<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'Nautic');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+[zp_MlTJbL&:oO):ET;xJWXiGjtaY]y$7=xfpv]{S}vI+! t#j_#!a;UZpu%ZX+');
define('SECURE_AUTH_KEY',  '2A+C=V~UciAP@LDPOQNjT4Sw5R+D+1a209jP?,RTv:}S=I3;S,Q+Sm`}rz}g3$YK');
define('LOGGED_IN_KEY',    't #!q%2D&9f!J7QKUEUkyAn/Ht$YDu=`+RWv|b-6i_w$<X$nFwG-NA;A4p=Q{/6S');
define('NONCE_KEY',        'o$3ZmB)%L$zz+az1wMff{W.O_u>EbPgC^pv4MLsLFNF RKSY8J& rC6lU|MJx/w0');
define('AUTH_SALT',        '}$xBXf/e2,^&HhcAr-Bt5-{pTK= w|B~+3g_.o|UnSOmD{-GtacAE;gHsy3?9Ohf');
define('SECURE_AUTH_SALT', '3M  G7Ng&|/CM!-bq@DSEYwXLk0@jX(;D>SZSY(JeKdE+WTYzFp$F|2?<3}0sWWX');
define('LOGGED_IN_SALT',   'aX0psC0A`Mxpg*IYCvb0DlH_)dMlhrv/QA}dGd:R.m17)B-i8P-yq<Mq&1*j_*Q)');
define('NONCE_SALT',       'rvnv@i2fz$/UxhVoi[.xl6Ex;2yDOm&#Z=Ki62&HAsGtO6$tsqbrN+uHKf!N_|MZ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
// Turns WordPress debugging on
define('WP_DEBUG', true);

// Tells WordPress to log everything to the /wp-content/debug.log file
define('WP_DEBUG_LOG', true);

// Doesn't force the PHP 'display_errors' variable to be on
define('WP_DEBUG_DISPLAY', false);

// Hides errors from being displayed on-screen
@ini_set('display_errors', 0);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
